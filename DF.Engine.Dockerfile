#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/runtime:6.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["App/MultiCryptoFin.Engine.Algo/MultiCryptoFin.Engine.Algo.csproj", "App/MultiCryptoFin.Engine.Algo/"]
COPY ["Shared/MultiCryptoFin.Models/MultiCryptoFin.Models.csproj", "Shared/MultiCryptoFin.Models/"]
COPY ["Shared/MultiCryptoFin.Shared/MultiCryptoFin.Shared.csproj", "Shared/MultiCryptoFin.Shared/"]
RUN dotnet restore "App/MultiCryptoFin.Engine.Algo/MultiCryptoFin.Engine.Algo.csproj"
COPY . .
RUN dotnet build "App/MultiCryptoFin.Engine.Algo/MultiCryptoFin.Engine.Algo.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "App/MultiCryptoFin.Engine.Algo/MultiCryptoFin.Engine.Algo.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "MultiCryptoFin.Engine.Algo.dll"]