#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["App/MultiCryptoFin.SignalR.Algo/MultiCryptoFin.SignalR.Algo.csproj", "App/MultiCryptoFin.SignalR.Algo/"]
COPY ["Shared/MultiCryptoFin.Models/MultiCryptoFin.Models.csproj", "Shared/MultiCryptoFin.Models/"]
COPY ["Shared/MultiCryptoFin.Shared/MultiCryptoFin.Shared.csproj", "Shared/MultiCryptoFin.Shared/"]
RUN dotnet restore "App/MultiCryptoFin.SignalR.Algo/MultiCryptoFin.SignalR.Algo.csproj"
COPY . .
RUN dotnet build "App/MultiCryptoFin.SignalR.Algo/MultiCryptoFin.SignalR.Algo.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "App/MultiCryptoFin.SignalR.Algo/MultiCryptoFin.SignalR.Algo.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "MultiCryptoFin.SignalR.Algo.dll"]