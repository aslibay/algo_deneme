using Deneme;
using System.Text;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR.Client;
using FinTech.Definitions.Extensions;
using MessagePack;
using MultiCryptoFin.Shared.Commons;
using MultiCryptoFin.Shared.Services;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddSingleton<IAlgoTestManager, AlgoTestManager>();

    })
    .Build();

await host.RunAsync();

AlgoTestManager _algoTestManager = new AlgoTestManager();

_algoTestManager.SendByQueue("AlgoStart", "");
List<object> sendList = new List<object>(){"BINANCE","BYBIT","BTC/USDT","6A3C6434-13A9-EC11-AC1F-000C29330757","9DE08355-B5A9-EC11-AC1F-000C29330757",0.01,0.01,0.01,0.5,-0.5,0,0.1,3,true};