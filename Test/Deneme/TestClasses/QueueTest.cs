using FinTech.Definitions.Queue;
using FinTech.Definitions.Queue.Interfaces;

namespace Deneme;

public class QueueTest
{
    private readonly IQueueManager<string> _queue;
    public QueueTest()
    {
        _queue = new QueueManager<string>("");      
        _queue.OnEventTriggered += queue_OnEventTriggered;          
    }    

    public void queue_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<string> e)
    {
        Console.WriteLine(e.Item);
    }

    public void Add(string request)
    {
        _queue.Add(request);
    }
}
