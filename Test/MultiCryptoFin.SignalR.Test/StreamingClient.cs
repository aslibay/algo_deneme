﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using MultiCryptoFin.SignalR.Algo.Test.Entities;
using Newtonsoft.Json;
using MessagePack;

namespace MultiCryptoFin.SignalR.Algo.Test;
public class StreamingClient
{
    public static async Task ExecuteAsync(string uri, bool IsStreaming = false)
    {
        await using var connection = new HubConnectionBuilder()
            .AddMessagePackProtocol(options =>
            {
                options.SerializerOptions.WithResolver(MessagePack.Resolvers.StandardResolver.Instance);
            })
            .WithUrl(uri)
            .Build();


        Console.WriteLine(string.Format("{0, -35} => Connecting...", uri));

        connection.On<string>("Connected", (message) =>
        {
            Console.WriteLine(string.Format("{0, -35} => Connected. ConnectionId = {1}", uri, message));
        });
        connection.On<string>("AlgoStart", (message) =>
        {
            Console.WriteLine(message);
        });
        await connection.StartAsync();

        if (IsStreaming)
        {
            await foreach (var iter in connection.StreamAsync<byte[]>("Streaming"))
            {
                DataModel dt = MessagePack.MessagePackSerializer.Deserialize<DataModel>(iter);
                Console.WriteLine(string.Format("{0, -35} => {1}", uri, JsonConvert.SerializeObject(dt)));
            }
        }
        else
        {


            List<object> orderBinanceBybit = new List<object>() { "BINANCE", "BYBIT", "BTC/USDT", "f2904143-e4a9-ec11-ac1f-000c29330757", "f2904143-e4a9-ec11-ac1f-000c29330757", 1, 2, 0.01, 0.5, -0.5, 0, 0.1, 3, true };
            List<object> orderHuobiFtx = new List<object>() { "HUOBI", "FTX", "BTC/USDT", "f2904143-e4a9-ec11-ac1f-000c29330757", "f2904143-e4a9-ec11-ac1f-000c29330757", 1, 2, 0.01, 0.5, -0.5, 0, 0.1, 3, true };
            List<object> orderWooOkex = new List<object>() { "WOO", "OKEX", "BTC/USDT", "f2904143-e4a9-ec11-ac1f-000c29330757", "f2904143-e4a9-ec11-ac1f-000c29330757", 1, 2, 0.01, 0.5, -0.5, 0, 0.1, 3, true };
            List<List<object>> orderList = new List<List<object>>() { orderBinanceBybit, orderHuobiFtx, orderWooOkex };
            try
            {
                byte[] v1 = MessagePackSerializer.Serialize<List<object>>(orderBinanceBybit);
                byte[] v2 = MessagePackSerializer.Serialize<List<object>>(orderHuobiFtx);
                byte[] v3 = MessagePackSerializer.Serialize<List<object>>(orderWooOkex);
                await connection.SendAsync("AlgoStart", v1).ConfigureAwait(false);
                await connection.SendAsync("AlgoStart", v2).ConfigureAwait(false);
                await connection.SendAsync("AlgoStart", v3).ConfigureAwait(false);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }


            Console.ReadLine();

            // foreach (List<object> iter in orderList)
            // {
            //     connection.SendAsync("AlgoStart", MessagePackSerializer.Serialize<List<object>>(iter)).ConfigureAwait(false);

            // }
            // while (true)
            // {
            //     Task.Delay(TimeSpan.FromHours(1));
            // }
        }
    }
}
