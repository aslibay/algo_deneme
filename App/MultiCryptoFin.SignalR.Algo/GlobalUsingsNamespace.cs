﻿global using Newtonsoft.Json;

global using FinTech.Definitions.Queue;
global using FinTech.Definitions.Queue.Interfaces;
global using FinTech.LoggerService.Interfaces;
global using FinTech.StackExchangeRedisService.Interfaces;
global using FinTech.StackExchangeRedisService;

global using MultiCryptoFin.Shared.Constants;

