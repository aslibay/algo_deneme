﻿/*
 
    https://www.telerik.com/blogs/bring-your-apps-life-signalr-dotnet-6
    
    CLI:
        * dotnet new console -o ProjectName
        * cd ProjectName
        * dotnet add package Microsoft.AspNetCore.SignalR.Client
 
 */

using MultiCryptoFin.Shared.Commons;
using MultiCryptoFin.Shared.Services;
using MultiCryptoFin.SignalR.Algo.Helpers;
using MultiCryptoFin.SignalR.Algo.Hubs;
using MultiCryptoFin.SignalR.Algo.Services.RedisService.Interfaces;
using MultiCryptoFin.SignalR.Algo.Services.RedisService;
using MultiCryptoFin.SignalR.Algo.Services.SwarmpitService.Interfaces;

FinTech.Definitions.Commons.InfoManager.Write(FinTech.Definitions.Commons.InfoManager.InfoModel.FinTech2, ConsoleColor.Blue);
FinTech.Definitions.Commons.InfoManager.Write(FinTech.Definitions.Commons.InfoManager.InfoModel.Socket2, ConsoleColor.White);
FinTech.Definitions.Commons.InfoManager.Write(FinTech.Definitions.Commons.InfoManager.InfoModel.Server2, ConsoleColor.Yellow);

#region BUILDER

var builder = WebApplication.CreateBuilder(args);

Definitions.SignalRAppSetting = builder.Configuration.Get<MultiCryptoFin.Shared.AppSettingsConfiguration.SignalR.AppSetting>();
#region ENVIRONMENT VARIABLE SETTINGS
IConfigurationRoot EnvConfiguration = new ConfigurationBuilder()
    .AddEnvironmentVariables("MultiCryptoFin.SignalR_")
    .Build();

EnvironmentVariableService.SetEnvironmentVariable(EnvConfiguration.GetChildren());
EnvironmentVariableService.CheckInEnvironment(Definitions.SignalRAppSetting, EnvConfiguration.GetChildren());
#endregion

List<string> appSetting = FinTech.Definitions.Commons.Definitions.CheckInPropertyValueIsNullOrEmpty(Definitions.SignalRAppSetting, "SignalRConfiguration.CorsAllowOrigins", "RedisSettings.Password", "LogSettings.Root");
if (appSetting.Count == 0)
{
    builder.Services.FintechAddLoggerSettings(Definitions.SignalRAppSetting.LogSettings);
    builder.Services.FintechAddRedisSettings(Definitions.SignalRAppSetting.RedisSettings);
    builder.Services.AddSingleton<IRedisManager, RedisManager>();
    builder.Services.AddSingleton<ISwarmpitManager, SwarmpitManager>();
    builder.Services.AddSignalR(options =>
    {
        options.EnableDetailedErrors = false;
        options.MaximumReceiveMessageSize = long.MaxValue;
        options.MaximumParallelInvocationsPerClient = 100;
    })
        .AddMessagePackProtocol(options =>
        {
            options.SerializerOptions.WithResolver(MessagePack.Resolvers.StandardResolver.Instance);
        });
    builder.Services.FintechAddSignalRCors(Definitions.SignalRAppSetting.SignalRConfiguration);


    #region APP

    var app = builder.Build();


    app.MapHub<HubAlgo>("/algo", opt =>
    {
        opt.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
        opt.ApplicationMaxBufferSize = long.MaxValue;
    });
    // app.MapHub<HubMain>("/main", opt =>
    // {
    //     opt.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
    //     opt.ApplicationMaxBufferSize = long.MaxValue;
    // });
    // app.MapHub<HubOpenOrders>("/openorders", opt =>
    // {
    //     opt.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
    //     opt.ApplicationMaxBufferSize = long.MaxValue;
    // });
    // app.MapHub<HubTransactions>("/transactions", opt =>
    // {
    //     opt.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
    //     opt.ApplicationMaxBufferSize = long.MaxValue;
    // });
    // app.MapHub<HubPositions>("/positions", opt =>
    // {
    //     opt.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
    //     opt.ApplicationMaxBufferSize = long.MaxValue;
    // });
    // app.MapHub<HubPrices>("/prices", opt =>
    // {
    //     opt.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
    //     opt.ApplicationMaxBufferSize = long.MaxValue;
    // });

    if (Definitions.SignalRAppSetting.Port != 0)
    {
        Console.WriteLine($"Running on port {Definitions.SignalRAppSetting.Port}.");
    }
    Console.WriteLine("Version: " + Definitions.SignalRVersion);

    if (Definitions.SignalRAppSetting.Port != 0)
    {
        app.Run($"http://*:{Definitions.SignalRAppSetting.Port}");
    }
    else
    {
        app.Run();
    }

    #endregion
}
else
{
    Console.WriteLine($"appsettings.json dosyası eksik parametre: {JsonConvert.SerializeObject(appSetting)}");
    FinTech.LoggerService.LoggerManager.Write(FinTech.LoggerService.Commons.EnumTypes.Log.Warning, $"appsettings.json dosyası eksik parametre :{JsonConvert.SerializeObject(appSetting)}");
}

#endregion
