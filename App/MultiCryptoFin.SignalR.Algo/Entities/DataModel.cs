﻿using MessagePack;

namespace MultiCryptoFin.SignalR.Algo.Entities;

[MessagePackObject]
public class DataModel
{
    [Key(0)]
    public string Symbol { get; set; }
    [Key(1)]
    public decimal Ask { get; set; }
    [Key(2)]
    public decimal AskSize { get; set; }
    [Key(3)]
    public decimal Bid { get; set; }
    [Key(4)]
    public decimal BidSize { get; set; }

}
