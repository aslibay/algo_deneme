using MultiCryptoFin.Models.Algo;
namespace MultiCryptoFin.SignalR.Algo.Services.SwarmpitService.Interfaces;
public interface ISwarmpitManager : IDisposable
{
    Task CreateService(string stackName, string serviceName, List<string>? environment = null, string? port= null, string? placement= null);


}
