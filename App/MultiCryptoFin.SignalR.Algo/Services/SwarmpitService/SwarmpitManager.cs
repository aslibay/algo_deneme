using MultiCryptoFin.SignalR.Algo.Services.SwarmpitService.Interfaces;
using MultiCryptoFin.Models.Algo;
using System.Net.Http.Headers;
using MultiCryptoFin.Shared.AppSettingsConfiguration.SignalR;
using System.Text;

namespace MultiCryptoFin.SignalR.Algo.Services.RedisService;
class SwarmpitManager : ISwarmpitManager
{
    private readonly string _swarmpitUrl = MultiCryptoFin.Shared.Commons.Definitions.SignalRAppSetting.SwarmpitUrl;
    const string _loginRoute = "/login";
    const string _stackRoute = "/api/stacks";
    const string _loginAuthenticationString = "ZmludGVjaDo5OVNhbG1hbjk5";
    public SwarmpitManager(){
        Console.WriteLine("constructor çalıştıdfksşlkfşslfkşskfş");
    }
    
    public void Dispose()
    {

    }
    public async Task<string> GetToken()
    {
        try{
        using (HttpClient _client = new HttpClient())
        {
            using (HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_swarmpitUrl}{_loginRoute}"))
            {
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", _loginAuthenticationString);
                using (HttpResponseMessage httpResponseMessage = await _client.SendAsync(requestMessage))
                {
                    string response = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (httpResponseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        dynamic tokenResult = JsonConvert.DeserializeObject(response);
                        return tokenResult.token;
                    }
                    else
                    {
                        return response;
                    }
                }
            }
        }}
        catch(Exception exc){
            return exc.Message;
        }
    }

    public async Task CreateService(string stackName, string serviceName, List<string>? environment = null, string? port= null, string? placement= null)
    {
        string token = await GetToken();
        token = token.Split(" ")[1];
        Console.WriteLine("token=> "+token);


        using (HttpClient _client = new HttpClient())
        {
            using (HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_swarmpitUrl}{_stackRoute}/{stackName}"))
            {
                var data = new {
                    name = stackName,
                    spec=new {
                        compose=$"version: '3.3'\nservices:\n  {serviceName}:\n    image: registry.fintechyazilim.com/multicryptofin/log_explorer:latest\n    environment:\n      FinTech.FileExplorer.Blazor_Setting_UrlBase: /\n    ports:\n     - {port}\n    volumes:\n     - /home/:/traefik-confs\n    networks:\n     - default\n    logging:\n      driver: json-file\n    deploy:\n      placement:\n        constraints:\n         - node.labels.grup == order\nnetworks:\n  default:\n    driver: overlay\n"
                    }
                    
                    
                };
                string _data = JsonConvert.SerializeObject(data);

                requestMessage.Content = new StringContent(_data, Encoding.UTF8, "application/json");
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                using (HttpResponseMessage httpResponseMessage = await _client.SendAsync(requestMessage))
                {
                    string response = await httpResponseMessage.Content.ReadAsStringAsync();
                        Console.WriteLine("status code => "+httpResponseMessage.StatusCode);

                    if (httpResponseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        dynamic tokenResult = JsonConvert.DeserializeObject(response);
                        Console.WriteLine("tokenResult => "+tokenResult);
                        // return tokenResult.token;
                    }
                    else
                    {
                        Console.WriteLine(response);
                        // return response;
                    }
                }
            }
        }
    }
}