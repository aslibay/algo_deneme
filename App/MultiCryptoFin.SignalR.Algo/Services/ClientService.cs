﻿using System.Collections.Concurrent;
using FinTech.LoggerService.Interfaces;
using Microsoft.AspNetCore.SignalR;

namespace MultiCryptoFin.SignalR.Algo.Services
{
    public class ClientService
    {
        private readonly string Name;
        private static readonly object Lock_RemoveGroup = new object();

        private readonly ConcurrentDictionary<string, List<string>> Channel2ConnectionIds = new ConcurrentDictionary<string, List<string>>();
        private readonly ConcurrentDictionary<string, List<string>> ConnectionId2Channels = new ConcurrentDictionary<string, List<string>>();
        private readonly ConcurrentDictionary<string, HubCallerContext> ConnectionId2HubCaller = new ConcurrentDictionary<string, HubCallerContext>();
        //private readonly ConcurrentDictionary<string, bool> ConnectionIds = new ConcurrentDictionary<string, bool>();

        private readonly ConcurrentDictionary<string, string> ConnectionId2Token = new ConcurrentDictionary<string, string>();
        private static ConcurrentDictionary<string, DateTime> Token2Time = new ConcurrentDictionary<string, DateTime>();
        private readonly ConcurrentDictionary<string, List<string>> Token2ConnectionIds = new ConcurrentDictionary<string, List<string>>();

        private readonly ILoggerManager LogManager;

        public ClientService(string Name,
            ILoggerManager LogManager)
        {
            this.Name = Name;
            this.LogManager = LogManager;
        }

        #region Sisteme bağlı client listesine 'ConnectionId' client bilgisi eleniyor.
        public void AddClient(HubCallerContext hubCallerContext, string AccessToken = null)
        {
            //ConnectionIds.TryAdd(ConnectionId, true);
            ConnectionId2HubCaller.TryAdd(hubCallerContext.ConnectionId, hubCallerContext);
            if (string.IsNullOrWhiteSpace(AccessToken) == false)
            {
                ConnectionId2Token.TryAdd(hubCallerContext.ConnectionId, AccessToken);
                Token2Time.AddOrUpdate(AccessToken, DateTime.Now, (k, v) =>
                {
                    v = DateTime.Now;
                    return v;
                });
                Token2ConnectionIds.AddOrUpdate(AccessToken, new List<string>() { hubCallerContext.ConnectionId },
                    (k, v) =>
                    {
                        v.Add(hubCallerContext.ConnectionId);
                        return v;
                    });
            }
        }
        #endregion

        #region Sisteme bağlı client listesinden 'ConnectionId' client bilgisi çıkarılıyor.
        public void RemoveClient(string ConnectionId)
        {
            //ConnectionIds.TryRemove(ConnectionId, out bool outConnectionIds);

            if (ConnectionId2Channels.TryRemove(ConnectionId, out var outChannels))
            {
                foreach (var iterChannel in outChannels)
                {
                    UnsubscribeChannel(iterChannel, ConnectionId);
                }
            }
            if (ConnectionId2HubCaller.TryRemove(ConnectionId, out var outValueContext))
            {
                if (outValueContext != null) GC.SuppressFinalize(outValueContext);
            }
            if (ConnectionId2Token.TryRemove(ConnectionId, out var outValueToken))
            {
                if (Token2ConnectionIds.TryGetValue(outValueToken, out var outConnectiondIds))
                {
                    outConnectiondIds.Remove(ConnectionId);
                    if (outConnectiondIds.Count == 0)
                    {
                        if (Token2ConnectionIds.TryRemove(outValueToken, out var outValueRemove))
                        {
                            if (outValueRemove != null) GC.SuppressFinalize(outValueRemove);
                        }
                        if (Token2Time.TryRemove(outValueToken, out var outValueTime))
                        {

                        }
                    }
                    if (outConnectiondIds != null) GC.SuppressFinalize(outConnectiondIds);
                }
            }
        }
        #endregion

        public void SubscribeChannel(string Channel, string ConnectionId)
        {
            #region 'Channel' kanalını dinleyen client listesine 'ConnectionId' client bilgisi ekleniyor.
            Channel2ConnectionIds.AddOrUpdate(Channel, new List<string>() { ConnectionId },
            (k, v) =>
            {
                if (v.Contains(ConnectionId) == false)
                {
                    v.Add(ConnectionId);
                }
                return v;
            });
            #endregion

            #region 'ConnectionId' client bilgisinin dinlemiş olduğu kanal listesine 'Channel' bilgisi ekleniyor.
            ConnectionId2Channels.AddOrUpdate(ConnectionId, new List<string>() { Channel },
               (k, v) =>
               {
                   if (v.Contains(Channel) == false)
                   {
                       v.Add(Channel);
                   }
                   return v;
               });
            #endregion
        }

        #region 'Channel' kanalını dinleyen client listesinden 'ConnectionId' client bilgisi çıkarılıyor.
        public void UnsubscribeChannel(string Channel, string ConnectionId)
        {
            lock (Lock_RemoveGroup)
            {
                if (Channel2ConnectionIds.ContainsKey(Channel))
                {
                    Channel2ConnectionIds[Channel].Remove(ConnectionId);
                }
            }
        }
        #endregion

        #region 'Channel' kanalını dinleyen client listesini getirir.
        public List<string> GetConnectionIdByChannel(string Channel)
        {
            if (Channel2ConnectionIds.ContainsKey(Channel))
            {
                return Channel2ConnectionIds[Channel].ToList();
            }
            return new List<string>();
        }
        public bool ClientIsSubscribe(string Channel, string ConnectionId)
        {
            if (ConnectionId2Channels.TryGetValue(ConnectionId, out var outValue))
            {
                return outValue.Contains(Channel);
            }
            return false;
        }
        #endregion

        #region 'ConnectionId' client bilgisi sisteme bağlı mı.
        public bool ClientIsConnected(string ConnectionId)
        {
            return ConnectionId2HubCaller.ContainsKey(ConnectionId);
        }
        #endregion

        #region Sisteme bağlı clinet listesini verir.
        public List<string> GetConnectionIds()
        {
            return ConnectionId2HubCaller.Keys.ToList();
        }
        #endregion

        #region AccessToken bilgisi ile bağlı client listesini verir.
        public List<string>? GetConnectionIds(string AccessToken)
        {
            if (Token2ConnectionIds.TryGetValue(AccessToken, out var outValueIds))
            {
                return outValueIds;
            }
            return null;
        }
        #endregion

        #region Get HubCallerContext from ConnectionId
        public HubCallerContext? GetHubCallerContext(string ConnectionId)
        {
            if (ConnectionId2HubCaller.TryGetValue(ConnectionId, out var outValueHubCaller))
            {
                return outValueHubCaller;
            }
            return null;
        }
        #endregion

        #region ConnectionId'ye ait kanal listesini getirir.
        public List<string> GetChannelsByConnectionId(string ConnectionId)
        {
            if (ConnectionId2Channels.ContainsKey(ConnectionId))
            {
                return ConnectionId2Channels[ConnectionId].ToList();
            }
            return new List<string>();
        }
        #endregion

        #region Channel listesini getirir
        public Dictionary<string, List<string>> GetSubscribers()
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            foreach (var iter in Channel2ConnectionIds.ToList())
            {
                result.TryAdd(iter.Key, iter.Value);
            }
            return result;
        }
        #endregion
    }
}
