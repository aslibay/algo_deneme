﻿using System.Collections.Concurrent;
using System.Diagnostics;
using MultiCryptoFin.Models.Algo;
using System.Text;
using FinTech.Definitions.Extensions;
using MessagePack;
using MultiCryptoFin.Shared.Entities.SignalR;
using MultiCryptoFin.SignalR.Algo.Services.RedisService.Interfaces;
using MultiCryptoFin.SignalR.Algo.Services.SwarmpitService.Interfaces;


namespace MultiCryptoFin.SignalR.Algo.Services
{
    public class AlgoSenderService
    {
        private readonly ILoggerManager _loggerManager;
        private readonly IRedisManager _redisManager;
        private readonly ISwarmpitManager _swarmpitManager;
        private readonly IQueueManager<byte[]> OnMessageAlgoCreateRequest;
        private readonly IQueueManager<AlgoSignalRModel> OnMessageAlgoCreateRequestInitialize;

        private ConcurrentDictionary<Guid?, string> EngineId2ConnectionId = new ConcurrentDictionary<Guid?, string>();
        private ConcurrentDictionary<string, Guid> ConnectionId2EngineId = new ConcurrentDictionary<string, Guid>();
        private ConcurrentDictionary<Guid?, AlgoSignalRModel> EngineId2Algo = new ConcurrentDictionary<Guid?, AlgoSignalRModel>();
        private ConcurrentDictionary<Guid?, int> EngineId2Process = new ConcurrentDictionary<Guid?, int>();

        string connectionId;
        public AlgoSenderService(ILoggerManager loggerManager, IRedisManager redisManager, ISwarmpitManager swarmpitManager)
        {
            _loggerManager = loggerManager;
            _redisManager = redisManager;
            _swarmpitManager = swarmpitManager;

            OnMessageAlgoCreateRequestInitialize = new QueueManager<AlgoSignalRModel>("OnMessageAlgoCreateRequestInitialize");
            OnMessageAlgoCreateRequestInitialize.OnEventTriggered += OnMessageAlgoCreateRequestInitialize_OnEventTriggered;

            OnMessageAlgoCreateRequest = new QueueManager<byte[]>("OnMessageAlgoCreateRequest");
            OnMessageAlgoCreateRequest.OnEventTriggered += OnMessageAlgoCreateRequest_OnEventTriggered;
        }

        public void OnMessageAlgoCreateRequestInitialize_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<AlgoSignalRModel> e)
        {
            try
            {
                EngineId2Process.TryAdd(e.Item.Id, CreateProcess(command: "MultiCryptoFin.Engine.Algo.dll", filePath: "/media/sena/Data/Fintech/code/algo_deneme/App/MultiCryptoFin.Engine.Algo/bin/Debug/net6.0", parameters: new List<string>() { e.Item.Id.ToString() }));

                Console.WriteLine("*************************** redisten okunanalar ayağa kalkıyor ****************************");


            }
            catch (Exception exc)
            {
                Console.WriteLine("exception => " + JsonConvert.SerializeObject(exc));
                GC.SuppressFinalize(exc);
            }
            finally { }
        }
        public void OnMessageAlgoCreateRequest_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<byte[]> e)
        {
            try
            {
                List<object> b = MessagePackSerializer.Deserialize<List<object>>(e.Item);
                SignalRMessageModel<AlgoSignalRModel> requestModel = new SignalRMessageModel<AlgoSignalRModel>()
                {
                    Channel = ConstantSignalRChannels.AlgoStart,
                    ConnectionId = connectionId,
                    UserId = Guid.Empty,
                    Message = new AlgoSignalRModel(b)
                };

                AddEngineId2Algo(requestModel.Message.Id, requestModel.Message);
                EngineId2Process.TryAdd(requestModel.Message.Id, CreateProcess(command: "MultiCryptoFin.Engine.Algo.dll", filePath: "/media/sena/Data/Fintech/code/algo_deneme/App/MultiCryptoFin.Engine.Algo/bin/Debug/net6.0", parameters: new List<string>() { requestModel.Message.Id.ToString() }));
                // EngineId2Process.TryAdd(requestModel.Message.Id, CreateProcess(serviceName:"ALGO"+requestModel.Message.FirstExchange+ requestModel.Message.SecondExchange, command: "MultiCryptoFin.Engine.Algo.dll", filePath: "/media/sena/Data/Fintech/code/algo_deneme/App/MultiCryptoFin.Engine.Algo/bin/Debug/net6.0", isDocker: true, parameters: new List<string>() { requestModel.Message.Id.ToString() }));

                requestModel.Dispose();


            }
            catch (Exception exc)
            {
                Console.WriteLine("exception => " + JsonConvert.SerializeObject(exc));
                _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
                GC.SuppressFinalize(exc);
            }
            finally { }
        }


        public int CreateProcess(string? serviceName = null, string? command = null, List<string>? parameters = null, string? filePath = null, bool? isDocker = false)
        {
            string arguments = "";
            if (parameters != null)
            {
                foreach (string item in parameters)
                {
                    arguments = arguments + " " + item;
                }
            }
            if (isDocker == false)
            {

                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = "dotnet", //"docker",
                    Arguments = command + " " + arguments,//"run -it worker_denemee",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                };

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = startInfo;
                proc.StartInfo.WorkingDirectory = filePath;
                proc.Start();
                return proc.Id;
            }
            else
            {
                _swarmpitManager.CreateService("Algo", serviceName, port: "9015:80");
                // ProcessStartInfo startInfo = new ProcessStartInfo()
                // {
                //     FileName = "docker",
                //     Arguments = "run -it worker_denemee",
                //     UseShellExecute = false,
                //     RedirectStandardOutput = true,
                //     CreateNoWindow = true
                // };

                // System.Diagnostics.Process proc = new System.Diagnostics.Process();
                // proc.StartInfo = startInfo;
                // proc.Start();
                // return proc.Id;
                return 1;

            }
        }
        public void AddAlgoCreate(byte[] request, string connectionId)
        {
            this.connectionId = connectionId;
            OnMessageAlgoCreateRequest.Add(request);

        }
        public void AddAlgoCreate(List<AlgoSignalRModel> request)
        {
            foreach (var item in request)
            {
                OnMessageAlgoCreateRequestInitialize.Add(item);
            }

        }
        public void AddEngineId(string connectionId, string engineId)
        {
            ConnectionId2EngineId.TryAdd(connectionId, new Guid(engineId));
            EngineId2ConnectionId.TryAdd(ConnectionId2EngineId[connectionId], connectionId);
        }
        public void RemoveEngine(string connectionId)
        {
            if (ConnectionId2EngineId.TryRemove(connectionId, out Guid outEngineId))
            {
                EngineId2ConnectionId.TryRemove(outEngineId, out string outConnectionId);
                EngineId2Algo.TryRemove(outEngineId, out AlgoSignalRModel outAlgo);
                _redisManager.RemoveAlgoString(outEngineId.ToString());
                KillProcess(outEngineId);
            }

        }
        public bool RemoveEngine(Guid? engineId)
        {
            if (EngineId2ConnectionId.TryRemove(engineId, out string outConnectionId))
            {
                ConnectionId2EngineId.TryRemove(outConnectionId, out Guid outEngineId);
                EngineId2Algo.TryRemove(outEngineId, out AlgoSignalRModel outAlgo);
                _redisManager.RemoveAlgoString(outEngineId.ToString());
                return KillProcess(engineId);
            }
            else
            {
                Console.WriteLine("Silinmeye çalışılan engine id bulunamadı!");
                return false;
            }

        }
        public bool KillProcess(Guid? Id)
        {
            EngineId2Process.TryGetValue(Id, out int procId);
            try
            {
                Process process = Process.GetProcessById(procId);
                Console.WriteLine("silinen engine => " + procId);
                process.Kill();
                return true;


            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
                return false;

            }


        }
        public List<AlgoSignalRModel> GetAlgoList()
        {

            return EngineId2Algo.Values.ToList();
        }
        public void ReplaceAlgo(Guid? Id, AlgoSignalRModel value)
        {
            _redisManager.ReplaceAlgoString(Id.ToString(), JsonConvert.SerializeObject(value));

            EngineId2Algo.AddOrUpdate(Id,
                (k) =>
                {
                    return value;
                },
                (k, v) =>
                {
                    v = value;
                    return v;
                });
        }
        public string GetEngineId2ConnectionId(Guid engineId)
        {
            if (EngineId2ConnectionId.TryGetValue(engineId, out string value))
            {
                return value;
            }
            return "";
        }

        public ConcurrentDictionary<string, Guid> GetConnectionId2EngineId()
        {
            return ConnectionId2EngineId;
        }
        // public ConcurrentDictionary<Guid, AlgoSignalRModel> GetEngineId2Algo(){
        //     return EngineId2Algo;
        // }
        public void AddEngineId2Algo(Guid? EngineId, AlgoSignalRModel Message)
        {
            _redisManager.AddAlgoString(EngineId.ToString(), JsonConvert.SerializeObject(Message));
            // Console.WriteLine("redise göndderilen veri => " + JsonConvert.SerializeObject(Message));
            // _redisManager.AddAlgoHash(JsonConvert.SerializeObject(EngineId), Message);
            EngineId2Algo.TryAdd(EngineId, Message);
        }
        public void AddEngineId2Algo(List<AlgoSignalRModel> Message)
        {
            foreach (var item in Message)
            {
                EngineId2Algo.TryAdd(item.Id, item);

            }
        }
        public AlgoSignalRModel GetEngineId2Algo(Guid? Id)
        {
            if (EngineId2Algo.TryGetValue(Id, out AlgoSignalRModel value))
            {
                return value;
            }
            return new AlgoSignalRModel();
        }
        public Guid? GetConnectionId2EngineId(string connectionId)
        {
            if (ConnectionId2EngineId.TryGetValue(connectionId, out Guid value))
            {
                return value;
            }
            return Guid.Empty;
        }
        public string GetEngineId2ConnectionId(Guid? id)
        {
            if (EngineId2ConnectionId.TryGetValue(id, out String value))
            {
                return value;
            }
            return "";
        }
    }

}
