using MultiCryptoFin.Models.Algo;
namespace MultiCryptoFin.SignalR.Algo.Services.RedisService.Interfaces;
public interface IRedisManager : IDisposable
{
    void AddAlgoString(string key, string value);
    void RemoveAlgoString(string key);
    void ReplaceAlgoString(string key, string value);
    AlgoSignalRModel GetAlgoString(string key);
    List<AlgoSignalRModel> GetAlgoStringList();
    void AddAlgoHash(string key, AlgoSignalRModel value);
    void RemoveAlgoHash(string key);
    void ReplaceAlgoHash(string key, AlgoSignalRModel value);

    void AddValue(string key, string value);

}
