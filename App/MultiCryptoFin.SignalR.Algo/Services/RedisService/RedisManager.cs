using MultiCryptoFin.SignalR.Algo.Services.RedisService.Interfaces;
using MultiCryptoFin.Models.Algo;


namespace MultiCryptoFin.SignalR.Algo.Services.RedisService;
public class RedisManager : IRedisManager
{
    public readonly IStackExchangeRedisManager _redisManager;
    public RedisManager(IStackExchangeRedisManager redisManager)
    {
        _redisManager = redisManager;
    }


    public void AddAlgoString(string key, string value)
    {
        // Console.WriteLine("buraya geldi-------------" + key);
        _redisManager.StringSet($"AlgoString:{key}", value);
    }
    public void RemoveAlgoString(string key)
    {
        // _redisManager.StringSet($"AlgoString:{key}",JsonConvert.SerializeObject(value));
        _redisManager.KeyDelete($"AlgoString:{key}");

    }
    public void ReplaceAlgoString(string key, string value)
    {
        _redisManager.StringSet($"AlgoString:{key}", value);

    }
    public AlgoSignalRModel GetAlgoString(string key)
    {
        string response = _redisManager.Get($"AlgoString:{key}");
        // Console.WriteLine("redisten gelen veriiiiiiiiiii=============>>>>>>>>>>>>>><" + response);
        AlgoSignalRModel _response = JsonConvert.DeserializeObject<AlgoSignalRModel>(response);
        return _response;
    }

    public List<AlgoSignalRModel> GetAlgoStringList()
    {
        List<AlgoSignalRModel> responseList = new List<AlgoSignalRModel>();
        List<string> response = _redisManager.GetKeys("AlgoString", "*");
        Console.WriteLine(JsonConvert.SerializeObject(response));

        foreach (string item in response)
        {
            string id = item.Split("AlgoString:")[1];
            // Console.WriteLine("redisten gelen id =>>     <>>>>>>>>>>>>>>>>>>>>>>>"+id);
            responseList.Add(GetAlgoString(id.ToString()));

        }
        // Console.WriteLine("redisten GELEN VERİ => " + JsonConvert.SerializeObject(responseList));
        return responseList;
    }
    public void AddAlgoHash(string key, AlgoSignalRModel value)
    {
        _redisManager.HashMSet<AlgoSignalRModel>($"AlgoHash:{key}", value);

    }
    public void RemoveAlgoHash(string key)
    {
        // _redisManager.HashMSet<AlgoSignalRModel>($"AlgoString:{key}",value);
        _redisManager.KeyDelete($"AlgoHash:{key}");
    }
    public void ReplaceAlgoHash(string key, AlgoSignalRModel value)
    {
        _redisManager.HashMSet<AlgoSignalRModel>($"AlgoHash:{key}", value);

    }

    public void AddValue(string key, string value)
    {
        _redisManager.SetAdd(key, value);

    }

    public void Dispose()
    {

    }
}

