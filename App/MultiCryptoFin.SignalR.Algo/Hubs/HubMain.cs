﻿using System.Text;
using FinTech.Definitions.Extensions;
using MessagePack;
using Microsoft.AspNetCore.SignalR;
using MultiCryptoFin.Models.Orders.UI;
using MultiCryptoFin.Shared.Entities.SignalR;
using MultiCryptoFin.SignalR.Algo.Services;

namespace MultiCryptoFin.SignalR.Algo.Hubs;

public class HubMain : Hub
{
    private readonly ILoggerManager LoggerManager;
    private static ClientService clientService;
    private static string EngineConnectionId;

    public HubMain(ILoggerManager LoggerManager)
    {
        this.LoggerManager = LoggerManager;

        clientService = new ClientService("HubMain", LoggerManager);
    }
    public override Task OnConnectedAsync()
    {
        string EngineType = GetEngineType();
        if (EngineType == "1")
        {
            EngineConnectionId = Context.ConnectionId;
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Connected|Engine|{Context.ConnectionId}");
        }
        else
        {
            clientService.AddClient(Context);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Connected|Client|{Context.ConnectionId}");
            SendToEngineConnectedClient();
            SendToClientsEngineIsOnlineState(Context.ConnectionId);
        }   
        Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Connected, Context.ConnectionId);
        return base.OnConnectedAsync();
    }
    public override Task OnDisconnectedAsync(Exception? exception)
    {
        if(EngineConnectionId != Context.ConnectionId)
        {
            clientService.RemoveClient(Context.ConnectionId);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Disonnected|Client|{Context.ConnectionId}");
        }
        else
        {            
            EngineConnectionId = null;
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Disonnected|Engine|{Context.ConnectionId}");
            SendToClientsEngineIsOnlineState();
        }
        return base.OnDisconnectedAsync(exception);
    }

    [HubMethodName(ConstantHubMethods.NewOrder)]
    public async Task NewOrder(byte[] Request)
    {
        LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.Request, JsonConvert.SerializeObject(new { HubMethods = ConstantHubMethods.NewOrder, Request = MessagePackSerializer.Deserialize<List<object>>(Request) })); LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.Request, $"NewOrder|{JsonConvert.SerializeObject(new { Request = Encoding.UTF8.GetString(Request) })}");
        try
        { 
            if (string.IsNullOrWhiteSpace(EngineConnectionId))
            {
                await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, "Engine bağlı değil!").ConfigureAwait(false);
            }
            else
            {
                SignalRMessageModel<NewOrderUIRequestReceiveDto> requestModel = new SignalRMessageModel<NewOrderUIRequestReceiveDto>()
                {
                    Channel = ConstantSignalRChannels.NewOrder,
                    ConnectionId = Context.ConnectionId,
                    UserId = Guid.Empty,
                    Message = MessagePackSerializer.Deserialize<NewOrderUIRequestReceiveDto>(Request)
                };
                await Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.NewOrder, requestModel).ConfigureAwait(false);
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.NewOrder, $"{requestModel.ToJsonSerialize()}");
                requestModel.Dispose();
            }
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally { }         
    }
    [HubMethodName(ConstantHubMethods.ReplaceOrder)]
    public async Task ReplaceOrder(byte[] Request)
    {
        LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.Request, JsonConvert.SerializeObject(new { HubMethods = ConstantHubMethods.ReplaceOrder, Request = MessagePackSerializer.Deserialize<List<object>>(Request) }));
        try
        {
            if (string.IsNullOrWhiteSpace(EngineConnectionId))
            {
                await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, "Engine bağlı değil!").ConfigureAwait(false);
            }
            else
            {
                SignalRMessageModel<ReplaceOrderUIRequestReceiveDto> requestModel = new SignalRMessageModel<ReplaceOrderUIRequestReceiveDto>()
                {
                    Channel = ConstantSignalRChannels.ReplaceOrder,
                    ConnectionId = Context.ConnectionId,
                    UserId = Guid.Empty,
                    Message = MessagePackSerializer.Deserialize<ReplaceOrderUIRequestReceiveDto>(Request)
                };
                await Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.ReplaceOrder, requestModel).ConfigureAwait(false);
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.ReplaceOrder, $"{requestModel.ToJsonSerialize()}");
                requestModel.Dispose();
            }
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally { }
    }
    [HubMethodName(ConstantHubMethods.CancelOrder)]
    public async Task CancelOrder(byte[] Request)
    {
        LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.Request, JsonConvert.SerializeObject(new { HubMethods = ConstantHubMethods.CancelOrder, Request = MessagePackSerializer.Deserialize<List<object>>(Request) }));
        try
        {
            if (string.IsNullOrWhiteSpace(EngineConnectionId))
            {
                await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, "Engine bağlı değil!").ConfigureAwait(false);
            }
            else
            {
                SignalRMessageModel<CancelOrderUIRequestReceiveDto> requestModel = new SignalRMessageModel<CancelOrderUIRequestReceiveDto>()
                {
                    Channel = ConstantSignalRChannels.CancelOrder,
                    ConnectionId = Context.ConnectionId,
                    UserId = Guid.Empty,
                    Message = MessagePackSerializer.Deserialize<CancelOrderUIRequestReceiveDto>(Request)
                };
                await Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.CancelOrder, requestModel).ConfigureAwait(false);
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.CancelOrder, $"{requestModel.ToJsonSerialize()}");
                requestModel.Dispose();
            }
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally { }
    }
    [HubMethodName(ConstantHubMethods.SendEngineIsOnline)]
    public Task SendEngineIsOnline(string ConnectionId)
    {
        return SendToClientsEngineIsOnlineState(ConnectionId);
    }

    #region PRIVATE METHODS
    private Task SendToEngineConnectedClient()
    {
        if (string.IsNullOrWhiteSpace(EngineConnectionId))
        {
            return Task.CompletedTask;
        }
        SignalRMessageModel<string> requestModel = new SignalRMessageModel<string>()
        {
            Channel = ConstantSignalRChannels.Unsubscribed,
            ConnectionId = Context.ConnectionId,
            UserId = Guid.Empty,
            Message = ConstantCommon.PriceHub
        };
        LoggerManager.AddInfoFileName(ConstantLoggerFileName.ClientConnected, $"{requestModel.ToJsonSerialize()}");
        requestModel.Dispose();
        return Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.ClientConnected, requestModel);
    }
    private Task SendToClientsEngineIsOnlineState(string ConnectionId = null)
    {
        if (string.IsNullOrWhiteSpace(ConnectionId))
        {
            return Clients.AllExcept(EngineConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, string.IsNullOrWhiteSpace(EngineConnectionId));
        }
        else
        {
            return Clients.Client(ConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, !string.IsNullOrWhiteSpace(EngineConnectionId));
        }
    }

    private string GetEngineType()
    {
        string headerValue = "";
        HttpRequest httpRequest = null;
        try
        {
            httpRequest = Context.GetHttpContext().Request;
            headerValue = httpRequest.Headers["EngineType"];
            if (string.IsNullOrWhiteSpace(headerValue) == true)
            {
                headerValue = httpRequest.Query["EngineType"];
            }
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally
        {
            if (httpRequest != null) GC.SuppressFinalize(httpRequest);
        }
        return headerValue;
    }
    private string? GetAccessTokenByRequest()
    {
        string accessToken = null;
        HttpRequest httpRequest = null;
        try
        {
            httpRequest = Context.GetHttpContext().Request;
            accessToken = httpRequest.Headers["Authorization"];
            LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromHeaders|Authorization|{accessToken}");
            if (string.IsNullOrEmpty(accessToken))
            {
                accessToken = httpRequest.Query["access_token"];
                LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromQuery|access_token|{accessToken}");
                if (string.IsNullOrWhiteSpace(accessToken))
                {
                    accessToken = (string)httpRequest.RouteValues[ConstantCommon.access_token];
                    LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromRoute|access_token|{accessToken}");
                }
            }
            if (string.IsNullOrWhiteSpace(accessToken) == false)
            {
                if (accessToken.StartsWith(ConstantCommon.AuthorizationType))
                {
                    accessToken = accessToken[ConstantCommon.AuthorizationType.Length..];
                }
                accessToken = accessToken.Trim();
                accessToken = accessToken.Replace("\"", "");
            }
            httpRequest = null;
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally
        {
            if (httpRequest != null) GC.SuppressFinalize(httpRequest);
        }
        return accessToken;
    }
    //public async Task<TokenCheckInfo> GetTokenCheckInfo(string accessToken = null)
    //{
    //    if (string.IsNullOrWhiteSpace(accessToken)) accessToken = GetAccessTokenByRequest();
    //    if (string.IsNullOrWhiteSpace(accessToken) == false)
    //    {
    //        try
    //        {
    //            return await TokenCheckManager.CheckIn(accessToken).ConfigureAwait(false);
    //        }
    //        catch (Exception exc)
    //        {
    //            LoggerQueueManager.AddErrorFileName(LoggerFileNameConstants.Error, exc.ExtractToString());
    //            GC.SuppressFinalize(exc);
    //        }
    //    }
    //    return null;
    //}
    //private async Task<Guid> GetUserId()
    //{
    //    string AccessToken = GetAccessTokenByRequest();
    //    using (TokenCheckInfo tokenCheckInfo = await GetTokenCheckInfo(AccessToken).ConfigureAwait(false))
    //    {
    //        if (tokenCheckInfo is null)
    //        {
    //            await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //            Thread.Sleep(5);
    //            Context.Abort();
    //            //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //        }
    //        else
    //        {
    //            using (FTP.Shared.Entities.Account.UserGetDBResult DBUserInfo = tokenCheckInfo.DBUserInfo)
    //            {
    //                if (DBUserInfo is null)
    //                {
    //                    await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //                    Thread.Sleep(5);
    //                    Context.Abort();
    //                    //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //                }
    //                else
    //                {
    //                    return DBUserInfo.UserID;
    //                }
    //            }
    //        }
    //    }
    //    return Guid.Empty;
    //}
    #endregion
}
