﻿using Microsoft.AspNetCore.SignalR;

namespace MultiCryptoFin.SignalR.Algo.Hubs;

public class HubPositions : Hub
{
    public HubPositions()
    {

    }
    public override Task OnConnectedAsync()
    {
        Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Connected, Context.ConnectionId);
        return base.OnConnectedAsync();
    }
    public override Task OnDisconnectedAsync(Exception? exception)
    {
        return base.OnDisconnectedAsync(exception);
    }
}
