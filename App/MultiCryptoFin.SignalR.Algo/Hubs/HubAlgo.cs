﻿using System.Text;
using FinTech.Definitions.Extensions;
using MessagePack;
using Microsoft.AspNetCore.SignalR;
using MultiCryptoFin.Models.Orders.UI;
using MultiCryptoFin.Shared.Entities.SignalR;
using MultiCryptoFin.SignalR.Algo.Services;
using MultiCryptoFin.Models.Algo;
using System.Collections.Concurrent;
using MultiCryptoFin.SignalR.Algo.Services.RedisService.Interfaces;
using MultiCryptoFin.SignalR.Algo.Services.SwarmpitService.Interfaces;

namespace MultiCryptoFin.SignalR.Algo.Hubs;

public class HubAlgo : Hub
{
    private readonly ILoggerManager LoggerManager;
    private readonly IRedisManager _redisManager;
    private readonly ISwarmpitManager _swarmpitManager;
    private static ClientService clientService;
    private static string EngineConnectionId;
    private static AlgoSenderService AlgoSender;
    public string? EngineId;
    public string? EngineType;
    public AlgoSignalRModel _algoPrice;
    private static bool isInitialize;
    public HubAlgo(ILoggerManager LoggerManager, IRedisManager redisManager, ISwarmpitManager swarmpitManager)
    {
        _redisManager = redisManager;
        _swarmpitManager = swarmpitManager;
        this.LoggerManager = LoggerManager;
        if (isInitialize == false)
        {
            clientService = new ClientService("HubAlgo", LoggerManager);
            isInitialize = true;
            AlgoSender = new AlgoSenderService(LoggerManager, redisManager, swarmpitManager);
            AlgoSender.AddEngineId2Algo(_redisManager.GetAlgoStringList());
            AlgoSender.AddAlgoCreate(_redisManager.GetAlgoStringList());
        }

    }
    public override Task OnConnectedAsync()
    {
        Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Connected, Context.ConnectionId);
        EngineId = GetEngineType();
        if (string.IsNullOrWhiteSpace(EngineId) == false)
        {
            AlgoSender.AddEngineId(Context.ConnectionId, EngineId);


            // Console.WriteLine($"EngineID : {EngineId}, ConnectionId: {Context.ConnectionId}");
            // Console.WriteLine("algoModel =>" + JsonConvert.SerializeObject(AlgoSender.GetEngineId2Algo(new Guid(EngineId))));
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Connected|Engine|{Context.ConnectionId}");
        }
        else
        {
            Console.WriteLine($"UI ConnectionId: {Context.ConnectionId}");
            Console.WriteLine(JsonConvert.SerializeObject(Context.ConnectionId));
            clientService.AddClient(Context);
            Console.WriteLine(JsonConvert.SerializeObject(clientService.GetConnectionIds()));
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Connected|Client|{Context.ConnectionId}");
            SendToEngineConnectedClient();
            SendToClientsEngineIsOnlineState(Context.ConnectionId);
            // Console.WriteLine("melihe giden algolistttttt => " + JsonConvert.SerializeObject(AlgoSender.GetAlgoList()));
            List<AlgoSignalRModel> sended = new List<AlgoSignalRModel>();
            Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.AlgoList, AlgoSender.GetAlgoList());

        }
        return base.OnConnectedAsync();
    }


    public override Task OnDisconnectedAsync(Exception? exception)
    {
        if (AlgoSender.GetConnectionId2EngineId().ContainsKey(Context.ConnectionId) == false)
        {
            Console.WriteLine($"Closed Client ConnectionId: {Context.ConnectionId}");
            clientService.RemoveClient(Context.ConnectionId);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Disonnected|Client|{Context.ConnectionId}");
        }
        else
        {
            Console.WriteLine($"Closed Engine ConnectionId: {Context.ConnectionId}");

            // AlgoSender.RemoveEngine(Context.ConnectionId);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubMain, $"Disonnected|Engine|{Context.ConnectionId}");
            SendToClientsEngineIsOnlineState();
        }
        return base.OnDisconnectedAsync(exception);
    }


    [HubMethodName(ConstantHubMethods.GetAlgo)]
    public async Task GetAlgo(string ConnectionId)
    {
        // Console.WriteLine("sena---------------------------");

        try
        {
            if (AlgoSender.GetConnectionId2EngineId(Context.ConnectionId) != Guid.Empty)
            {
                Guid? engineId = AlgoSender.GetConnectionId2EngineId(Context.ConnectionId);
                AlgoSignalRModel algoSignalRModel = AlgoSender.GetEngineId2Algo(engineId);
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "Gönderilen algo =>  " + JsonConvert.SerializeObject(algoSignalRModel));

                // Console.WriteLine(JsonConvert.SerializeObject("Gönderilen algo => " + JsonConvert.SerializeObject(algoSignalRModel)));
                // SignalRMessageModel<object> sena = new SignalRMessageModel<object>();
                await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.AlgoCreate, algoSignalRModel).ConfigureAwait(false);
            }
            else
            {
                await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.AlgoCreate, "Böyle bir engine kayıtlı değil!").ConfigureAwait(false);

            }

        }
        catch (Exception exc)
        {
            Console.WriteLine("exception => " + JsonConvert.SerializeObject(exc));
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally { }
    }


    [HubMethodName(ConstantHubMethods.AlgoCreate)]
    public async Task AlgoCreate(byte[] Request)
    {
        AlgoSender.AddAlgoCreate(Request, Context.ConnectionId);

    }


    [HubMethodName(ConstantHubMethods.AlgoStart)]
    public async Task AlgoStart(Guid Request)
    {
        Console.WriteLine("algo start engine id => " + JsonConvert.SerializeObject(Request));
        string connectionId = AlgoSender.GetEngineId2ConnectionId(Request);
        Console.WriteLine("algo start connectionId=> " + JsonConvert.SerializeObject(connectionId));
        await Clients.Client(connectionId).SendAsync(ConstantSignalRChannels.AlgoStart, true).ConfigureAwait(false);
        AlgoSignalRModel bufferStart = AlgoSender.GetEngineId2Algo(Request);
        bufferStart.IsOn = true;
        AlgoSender.ReplaceAlgo(Request, bufferStart);
        // AlgoSender.AddAlgoStart(Id);

    }


    [HubMethodName(ConstantHubMethods.AlgoStop)]
    public async Task AlgoStop(Guid Request)
    {
        string connectionId = AlgoSender.GetEngineId2ConnectionId(Request);
        await Clients.Client(connectionId).SendAsync(ConstantSignalRChannels.AlgoStop, false).ConfigureAwait(false);
        Console.WriteLine("algostop=> " + Request);
        AlgoSignalRModel bufferStop = AlgoSender.GetEngineId2Algo(Request);
        bufferStop.IsOn = false;
        AlgoSender.ReplaceAlgo(Request, bufferStop);

    }


    [HubMethodName(ConstantHubMethods.AlgoReplace)]
    public async Task AlgoReplace(byte[] Request)
    {
        LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.Request, JsonConvert.SerializeObject(new { HubMethods = ConstantHubMethods.ReplaceOrder, Request = MessagePackSerializer.Deserialize<List<object>>(Request) }));
        try
        {
            List<object> _request = MessagePackSerializer.Deserialize<List<object>>(Request);
            AlgoSignalRModel _replacedData = new AlgoSignalRModel(_request);
            string connectionId = AlgoSender.GetEngineId2ConnectionId(_replacedData.Id);
            AlgoSender.ReplaceAlgo(_replacedData.Id, _replacedData);
            await Clients.Client(connectionId).SendAsync(ConstantSignalRChannels.AlgoReplace, _replacedData).ConfigureAwait(false);
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally { }
    }


    [HubMethodName(ConstantHubMethods.AlgoDelete)]
    public async Task AlgoDelete(Guid Request)
    {
        var clientList = clientService.GetConnectionIds();
        Console.WriteLine("clients => " + JsonConvert.SerializeObject(clientList));
        LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.AlgoDelete, JsonConvert.SerializeObject(Request));
        try
        {
            bool engineIsDeleted = AlgoSender.RemoveEngine(Request);
            Console.WriteLine("engineIsDeleted => " + engineIsDeleted);
            if (engineIsDeleted == true)
            {
                List<object> response = new List<object>() { Request, true };
                await Clients.Clients(clientList).SendAsync(ConstantSignalRChannels.AlgoDeleteResponse, response).ConfigureAwait(false);
            }
            else
            {
                List<object> response = new List<object>() { Request, false };
                await Clients.Clients(clientList).SendAsync(ConstantSignalRChannels.AlgoDeleteResponse, response).ConfigureAwait(false);


            }
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally { }
    }


    [HubMethodName(ConstantHubMethods.PreAlgoList)]
    public async Task PreAlgoList(string Request)
    {
        Console.WriteLine("PreAlgoList melihten geldi");
        var clientList = clientService.GetConnectionIds();
        List<AlgoSignalRModel> algoList = AlgoSender.GetAlgoList();
        List<List<object>> finalArray = new List<List<object>>();
        foreach (var item in algoList)
        {
            finalArray.Add(new List<object>(){ item.FirstExcApiId, item.SecondExcApiId});
            
        }
        
        Console.WriteLine("PreAlgoList => "+JsonConvert.SerializeObject(finalArray.GroupBy(x=> x[0].ToString()+x[1].ToString()).Select(g => g.First()).Distinct().ToList()));

        await Clients.Clients(clientList).SendAsync(ConstantSignalRChannels.PreAlgoList, finalArray.GroupBy(x=> x[0].ToString()+x[1].ToString()).Select(g => g.First()).Distinct().ToList()).ConfigureAwait(false);

    }


    [HubMethodName(ConstantHubMethods.AlgoList)]
    public async Task AlgoList(string Request)
    {
        var clientList = clientService.GetConnectionIds();

        await Clients.Clients(clientList).SendAsync(ConstantSignalRChannels.AlgoList, AlgoSender.GetAlgoList()).ConfigureAwait(false);
    }


    [HubMethodName(ConstantHubMethods.SendAlgoPrice)]
    public async Task SendAlgoPrice(AlgoPriceSender Request)
    {
        var clientList = clientService.GetConnectionIds();
        // Console.WriteLine("melihe giden veri => " + JsonConvert.SerializeObject(clientList));

        // Console.WriteLine("melihe giden veri => " + JsonConvert.SerializeObject(Request));
        await Clients.Clients(clientList).SendAsync(ConstantSignalRChannels.AlgoPrice, Request).ConfigureAwait(false);

    }


    [HubMethodName(ConstantHubMethods.SendEngineIsOnline)]
    public Task SendEngineIsOnline(string ConnectionId)
    {
        return SendToClientsEngineIsOnlineState(ConnectionId);
    }



    #region PRIVATE METHODS
    private Task SendToEngineConnectedClient()
    {
        if (string.IsNullOrWhiteSpace(EngineConnectionId))
        {
            return Task.CompletedTask;
        }
        SignalRMessageModel<string> requestModel = new SignalRMessageModel<string>()
        {
            Channel = ConstantSignalRChannels.Unsubscribed,
            ConnectionId = Context.ConnectionId,
            UserId = Guid.Empty,
            Message = ConstantCommon.PriceHub
        };
        LoggerManager.AddInfoFileName(ConstantLoggerFileName.ClientConnected, $"{requestModel.ToJsonSerialize()}");
        requestModel.Dispose();
        return Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.ClientConnected, requestModel);
    }
    private Task SendToClientsEngineIsOnlineState(string ConnectionId = null)
    {
        if (string.IsNullOrWhiteSpace(ConnectionId))
        {
            return Clients.AllExcept(EngineConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, string.IsNullOrWhiteSpace(EngineConnectionId));
        }
        else
        {
            return Clients.Client(ConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, !string.IsNullOrWhiteSpace(EngineConnectionId));
        }
    }

    private string GetEngineType()
    {
        string engineId = "";
        HttpContext? httpContext = Context.GetHttpContext();
        if (httpContext != null)
        {
            try
            {
                ;
                if (string.IsNullOrWhiteSpace(httpContext.Request.Headers["EngineId"]) == false)
                {
                    engineId = httpContext.Request.Headers["EngineId"];
                }
                else if (string.IsNullOrWhiteSpace(httpContext.Request.Query["EngineId"]) == false)
                {
                    engineId = httpContext.Request.Query["EngineId"];
                }
            }
            catch (Exception exc)
            {
                LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
                GC.SuppressFinalize(exc);
            }
            finally
            {
                if (httpContext != null) GC.SuppressFinalize(httpContext);
            }
        }
        return engineId;
    }
    private string? GetAccessTokenByRequest()
    {
        string accessToken = null;
        HttpContext? httpContext = Context.GetHttpContext();
        if (httpContext != null)
        {
            try
            {
                accessToken = httpContext.Request.Headers["Authorization"];
                LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromHeaders|Authorization|{accessToken}");
                if (string.IsNullOrEmpty(accessToken))
                {
                    accessToken = httpContext.Request.Query["access_token"];
                    LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromQuery|access_token|{accessToken}");
                    if (string.IsNullOrWhiteSpace(accessToken))
                    {
                        accessToken = (string)httpContext.Request.RouteValues[ConstantCommon.access_token];
                        LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromRoute|access_token|{accessToken}");
                    }
                }
                if (string.IsNullOrWhiteSpace(accessToken) == false)
                {
                    if (accessToken.StartsWith(ConstantCommon.AuthorizationType))
                    {
                        accessToken = accessToken[ConstantCommon.AuthorizationType.Length..];
                    }
                    accessToken = accessToken.Trim();
                    accessToken = accessToken.Replace("\"", "");
                }
            }
            catch (Exception exc)
            {
                LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
                GC.SuppressFinalize(exc);
            }
            finally
            {
                if (httpContext != null) GC.SuppressFinalize(httpContext);
            }
        }
        return accessToken;
    }
    //public async Task<TokenCheckInfo> GetTokenCheckInfo(string accessToken = null)
    //{
    //    if (string.IsNullOrWhiteSpace(accessToken)) accessToken = GetAccessTokenByRequest();
    //    if (string.IsNullOrWhiteSpace(accessToken) == false)
    //    {
    //        try
    //        {
    //            return await TokenCheckManager.CheckIn(accessToken).ConfigureAwait(false);
    //        }
    //        catch (Exception exc)
    //        {
    //            LoggerQueueManager.AddErrorFileName(LoggerFileNameConstants.Error, exc.ExtractToString());
    //            GC.SuppressFinalize(exc);
    //        }
    //    }
    //    return null;
    //}
    //private async Task<Guid> GetUserId()
    //{
    //    string AccessToken = GetAccessTokenByRequest();
    //    using (TokenCheckInfo tokenCheckInfo = await GetTokenCheckInfo(AccessToken).ConfigureAwait(false))
    //    {
    //        if (tokenCheckInfo is null)
    //        {
    //            await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //            Thread.Sleep(5);
    //            Context.Abort();
    //            //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //        }
    //        else
    //        {
    //            using (FTP.Shared.Entities.Account.UserGetDBResult DBUserInfo = tokenCheckInfo.DBUserInfo)
    //            {
    //                if (DBUserInfo is null)
    //                {
    //                    await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //                    Thread.Sleep(5);
    //                    Context.Abort();
    //                    //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //                }
    //                else
    //                {
    //                    return DBUserInfo.UserID;
    //                }
    //            }
    //        }
    //    }
    //    return Guid.Empty;
    //}
    #endregion
}
