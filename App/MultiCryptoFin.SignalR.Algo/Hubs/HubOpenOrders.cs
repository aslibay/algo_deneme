﻿using FinTech.Definitions.Extensions;
using Microsoft.AspNetCore.SignalR;
using MultiCryptoFin.Models.Orders.Base;
using MultiCryptoFin.Shared.Entities.SignalR;
using MultiCryptoFin.SignalR.Algo.Services;

namespace MultiCryptoFin.SignalR.Algo.Hubs;

public class HubOpenOrders : Hub
{
    private readonly ILoggerManager LoggerManager;
    private static bool IsInitiazlied { get; set; }
    private static ClientService clientService;
    private static string EngineConnectionId;

    public HubOpenOrders(ILoggerManager LoggerManager)
    {
        this.LoggerManager = LoggerManager;

        if (IsInitiazlied == false)
        {
            IsInitiazlied = true;
            clientService = new ClientService("HubOpenOrders", LoggerManager);
        }
    }

    public override Task OnConnectedAsync()
    {
        string EngineType = GetEngineType();
        if (EngineType == "1")
        {
            EngineConnectionId = Context.ConnectionId;
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubOpenOrders, $"Connected|Engine|{Context.ConnectionId}");
        }
        else
        {
            clientService.AddClient(Context);
            clientService.SubscribeChannel(ConstantSignalRChannels.OpenOrders, Context.ConnectionId);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubOpenOrders, $"Connected|Client|{Context.ConnectionId}");
            SendToEngineConnectedClient();
            SendToClientsEngineIsOnlineState(Context.ConnectionId);
        }
        Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Connected, Context.ConnectionId);
        return base.OnConnectedAsync();
    }
    public override Task OnDisconnectedAsync(Exception? exception)
    {
        if (EngineConnectionId != Context.ConnectionId)
        {
            clientService.UnsubscribeChannel(ConstantSignalRChannels.OpenOrders, Context.ConnectionId);
            clientService.RemoveClient(Context.ConnectionId);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubOpenOrders, $"Disonnected|Client|{Context.ConnectionId}");
        }
        else
        {
            EngineConnectionId = null;
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubOpenOrders, $"Disonnected|Engine|{Context.ConnectionId}");
            SendToClientsEngineIsOnlineState();
        }
        return base.OnDisconnectedAsync(exception);
    }

    #region SOCKET ENDPOINTS
    [HubMethodName(ConstantHubMethods.SendGroup)]
    public Task SendGroup(OrderDto Message)
    {
        var _clients = clientService.GetConnectionIdByChannel(ConstantSignalRChannels.OpenOrders);
        if (_clients.Count > 0)
        {
            LoggerManager.AddInfoFileNameDebugMode(ConstantSignalRChannels.OpenOrders, $"SendGroup|{JsonConvert.SerializeObject(Message)}");
            return Clients.Clients(_clients).SendAsync(ConstantSignalRChannels.OpenOrders, Message);
        }
        return Task.CompletedTask;
    }
    [HubMethodName(ConstantHubMethods.SendGroupInitialize)]
    public Task SendGroupInitialize(List<OrderDto> Message)
    {
        var _clients = clientService.GetConnectionIdByChannel(ConstantSignalRChannels.OpenOrders);
        if (_clients.Count > 0)
        {
            return Clients.Clients(_clients).SendAsync(ConstantSignalRChannels.OpenOrdersInitialize, Message);
        }
        return Task.CompletedTask;
    }
    [HubMethodName(ConstantHubMethods.SendClient)]
    public Task SendClient(string ConnectionId, List<OrderDto> Message)
    {
        if (clientService.ClientIsConnected(ConnectionId))
        {
            return Clients.Client(ConnectionId).SendAsync(ConstantSignalRChannels.OpenOrders, Message);
        }
        return Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, $"{ConnectionId} id'li client sisteme bağlı değil!");
    }
    [HubMethodName(ConstantHubMethods.SendClientInitialize)]
    public Task SendClientInitialize(string ConnectionId, List<OrderDto> Message)
    {
        if (clientService.ClientIsConnected(ConnectionId))
        {
            return Clients.Client(ConnectionId).SendAsync(ConstantSignalRChannels.OpenOrdersInitialize, Message);
        }
        return Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, $"{ConnectionId} id'li client sisteme bağlı değil!");
    }
    [HubMethodName(ConstantHubMethods.SendGroupWarning)]
    public Task SendGroupWarning(OrderDto Message)
    {
        var _clients = clientService.GetConnectionIdByChannel(ConstantSignalRChannels.OpenOrders);
        if (_clients.Count > 0)
        {
            LoggerManager.AddInfoFileNameDebugMode(ConstantSignalRChannels.Warning, $"SendGroupWarning|{JsonConvert.SerializeObject(Message)}");
            return Clients.Clients(_clients).SendAsync(ConstantSignalRChannels.Warning, Message);
        }
        return Task.CompletedTask;
    }
    [HubMethodName(ConstantHubMethods.SendEngineIsOnline)]
    public Task SendEngineIsOnline(string ConnectionId)
    {
        return SendToClientsEngineIsOnlineState(ConnectionId);
    }
    #endregion
    #region PRIVATE METHODS
    private Task SendToEngineConnectedClient()
    {
        if (string.IsNullOrWhiteSpace(EngineConnectionId))
        {
            return Task.CompletedTask;
        }
        SignalRMessageModel<string> requestModel = new SignalRMessageModel<string>()
        {
            Channel = ConstantSignalRChannels.Unsubscribed,
            ConnectionId = Context.ConnectionId,
            UserId = Guid.Empty,
            Message = ConstantCommon.PriceHub
        };
        LoggerManager.AddInfoFileName(ConstantLoggerFileName.ClientConnected, $"{requestModel.ToJsonSerialize()}");
        requestModel.Dispose();
        return Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.ClientConnected, requestModel);
    }
    private Task SendToClientsEngineIsOnlineState(string ConnectionId = null)
    {
        if (string.IsNullOrWhiteSpace(ConnectionId))
        {
            return Clients.AllExcept(EngineConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, string.IsNullOrWhiteSpace(EngineConnectionId));
        }
        else
        {
            return Clients.Client(ConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, !string.IsNullOrWhiteSpace(EngineConnectionId));
        }
    }
    private string GetEngineType()
    {
        string headerValue = "";
        HttpRequest httpRequest = null;
        try
        {
            httpRequest = Context.GetHttpContext().Request;
            headerValue = httpRequest.Headers["EngineType"];
            if (string.IsNullOrWhiteSpace(headerValue) == true)
            {
                headerValue = httpRequest.Query["EngineType"];
            }
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally
        {
            if (httpRequest != null) GC.SuppressFinalize(httpRequest);
        }
        return headerValue;
    }
    private string? GetAccessTokenByRequest()
    {
        string accessToken = null;
        HttpRequest httpRequest = null;
        try
        {
            httpRequest = Context.GetHttpContext().Request;
            accessToken = httpRequest.Headers["Authorization"];
            LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromHeaders|Authorization|{accessToken}");
            if (string.IsNullOrEmpty(accessToken))
            {
                accessToken = httpRequest.Query["access_token"];
                LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromQuery|access_token|{accessToken}");
                if (string.IsNullOrWhiteSpace(accessToken))
                {
                    accessToken = (string)httpRequest.RouteValues[ConstantCommon.access_token];
                    LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromRoute|access_token|{accessToken}");
                }
            }
            if (string.IsNullOrWhiteSpace(accessToken) == false)
            {
                if (accessToken.StartsWith(ConstantCommon.AuthorizationType))
                {
                    accessToken = accessToken[ConstantCommon.AuthorizationType.Length..];
                }
                accessToken = accessToken.Trim();
                accessToken = accessToken.Replace("\"", "");
            }
            httpRequest = null;
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally
        {
            if (httpRequest != null) GC.SuppressFinalize(httpRequest);
        }
        return accessToken;
    }
    //public async Task<TokenCheckInfo> GetTokenCheckInfo(string accessToken = null)
    //{
    //    if (string.IsNullOrWhiteSpace(accessToken)) accessToken = GetAccessTokenByRequest();
    //    if (string.IsNullOrWhiteSpace(accessToken) == false)
    //    {
    //        try
    //        {
    //            return await TokenCheckManager.CheckIn(accessToken).ConfigureAwait(false);
    //        }
    //        catch (Exception exc)
    //        {
    //            LoggerQueueManager.AddErrorFileName(LoggerFileNameConstants.Error, exc.ExtractToString());
    //            GC.SuppressFinalize(exc);
    //        }
    //    }
    //    return null;
    //}
    //private async Task<Guid> GetUserId()
    //{
    //    string AccessToken = GetAccessTokenByRequest();
    //    using (TokenCheckInfo tokenCheckInfo = await GetTokenCheckInfo(AccessToken).ConfigureAwait(false))
    //    {
    //        if (tokenCheckInfo is null)
    //        {
    //            await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //            Thread.Sleep(5);
    //            Context.Abort();
    //            //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //        }
    //        else
    //        {
    //            using (FTP.Shared.Entities.Account.UserGetDBResult DBUserInfo = tokenCheckInfo.DBUserInfo)
    //            {
    //                if (DBUserInfo is null)
    //                {
    //                    await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //                    Thread.Sleep(5);
    //                    Context.Abort();
    //                    //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //                }
    //                else
    //                {
    //                    return DBUserInfo.UserID;
    //                }
    //            }
    //        }
    //    }
    //    return Guid.Empty;
    //}
    #endregion
}
