﻿using FinTech.Definitions.Extensions;
using Microsoft.AspNetCore.SignalR;
using MultiCryptoFin.Models.Price;
using MultiCryptoFin.Models.Subscribe;
using MultiCryptoFin.Shared.Entities.SignalR;
using MultiCryptoFin.SignalR.Algo.Services;

namespace MultiCryptoFin.SignalR.Algo.Hubs;

public class HubPrices : Hub
{
    private readonly ILoggerManager LoggerManager;
    private static bool IsInitiazlied { get; set; }
    private static ClientService clientService;
    private static string EngineConnectionId;
    public HubPrices(ILoggerManager LoggerManager)
    {
        this.LoggerManager = LoggerManager;

        if (IsInitiazlied == false)
        {
            IsInitiazlied = true;
            clientService = new ClientService("HubPrices", LoggerManager);
        }
    }
    public override Task OnConnectedAsync()
    {
        string EngineType = GetEngineType();
        if (EngineType == "1")
        {
            EngineConnectionId = Context.ConnectionId;
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubPrices, $"Connected|Engine|{Context.ConnectionId}");
            // Engine bağlanınca tüme clientlara bildirmesi için engine içinde connect olunca o tüm clientlara bildiriyor!
        }
        else
        {
            clientService.AddClient(Context);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubPrices, $"Connected|Client|{Context.ConnectionId}");
            SendToEngineConnectedClient();
            SendToClientsEngineIsOnlineState(Context.ConnectionId);
        }
        Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Connected, Context.ConnectionId);
        return base.OnConnectedAsync();
    }
    public override Task OnDisconnectedAsync(Exception? exception)
    {
        if (EngineConnectionId != Context.ConnectionId)
        {
            #region UI her sembol değişiminde sayfasını yeniden yüklediği için connection 'lar yeniden açılıyor bundan dolayı connection kapatılınca client' a bağlı subscribe' ler için unsubscribe mesajlarını engine gönderiliyor!
            List<string> channels = clientService.GetChannelsByConnectionId(Context.ConnectionId);
            if (string.IsNullOrWhiteSpace(EngineConnectionId) == false)
            {
                SubscriberSignalRModel message = new SubscriberSignalRModel("", false);
                SignalRMessageModel<SubscriberSignalRModel> requestModel = new SignalRMessageModel<SubscriberSignalRModel>()
                {
                    Channel = ConstantSignalRChannels.Unsubscribed,
                    ConnectionId = Context.ConnectionId,
                    UserId = Guid.Empty,
                    Message = message
                };
                foreach (string channel in channels)
                {
                    message.Symbol = channel;
                    requestModel.Message = message;
                    Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.Unsubscribed, requestModel).ConfigureAwait(false);
                    LoggerManager.AddInfoFileName(ConstantLoggerFileName.Subscribed, $"{requestModel.ToJsonSerialize()}");
                }
                requestModel.Dispose();
                message.Dispose();
            }
            #endregion
            clientService.RemoveClient(Context.ConnectionId);
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubPrices, $"Disonnected|Client|{Context.ConnectionId}");
        }
        else
        {
            SendToClientsEngineIsOnlineState();
            EngineConnectionId = null;
            LoggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.HubPrices, $"Disonnected|Engine|{Context.ConnectionId}");
        }
        return base.OnDisconnectedAsync(exception);
    }
    #region SOCKET ENDPOINTS
    [HubMethodName(ConstantHubMethods.SendGroup)]
    public Task SendGroup(string Symbol, List<PriceDto> Message)
    {
        List<string> clients = clientService.GetConnectionIdByChannel(Symbol);
        if (clients.Count > 0)
        {
            Console.WriteLine("sendgrup => "+JsonConvert.SerializeObject(Message) );     

            Clients.Clients(clients).SendAsync(ConstantSignalRChannels.Prices, Message);
        }
        LoggerManager.AddInfoFileNameDebugMode(ConstantSignalRChannels.Prices, $"SendGroup|{clients.Count}|{JsonConvert.SerializeObject(Message)}");
        return Task.CompletedTask;
    }

    [HubMethodName(ConstantHubMethods.SendClient)]
    public Task SendClient(string ConnectionId, string Symbol, List<PriceDto> Message)
    {
        if (clientService.ClientIsConnected(ConnectionId))
        {
            if (clientService.ClientIsSubscribe(Symbol, ConnectionId))
            {
                return Clients.Client(ConnectionId).SendAsync(ConstantSignalRChannels.Prices, Message);
            }
        }
        return Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, $"{ConnectionId} id'li client sisteme bağlı değil!");
    }


    [HubMethodName(ConstantHubMethods.Subscribe)]
    public async Task Subscribe(string Symbol)
    {
        if (string.IsNullOrWhiteSpace(EngineConnectionId))
        {
            await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, "Engine bağlı değil!").ConfigureAwait(false);
        }
        else
        {
            SignalRMessageModel<SubscriberSignalRModel> requestModel = new SignalRMessageModel<SubscriberSignalRModel>()
            {
                Channel = ConstantSignalRChannels.Subscribed,
                ConnectionId = Context.ConnectionId,
                UserId = Guid.Empty,
                Message = new SubscriberSignalRModel(Symbol, true)  

            };
            
            Console.WriteLine("subscribe symbol => "+ JsonConvert.SerializeObject(requestModel))  ; 
            bool ClientIsSubscribe = clientService.ClientIsSubscribe(Symbol, Context.ConnectionId);
            if (ClientIsSubscribe == false)
            {
                clientService.SubscribeChannel(Symbol, Context.ConnectionId);
                await Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.Subscribed, requestModel).ConfigureAwait(false);
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.Subscribed, $"{requestModel.ToJsonSerialize()}");
                await Clients.Clients(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Subscribed, true);
            }
            else
            {
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.Subscribed, JsonConvert.SerializeObject(new { ClientIsSubscribe, Request = requestModel }));
            }
            requestModel.Dispose();
        }
    }
    [HubMethodName(ConstantHubMethods.Unsubscribe)]
    public async Task Unsubscribe(string Symbol)
    {        
        if (string.IsNullOrWhiteSpace(EngineConnectionId))
        {
            await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, "Engine bağlı değil!").ConfigureAwait(false);
        }
        else
        {
            SignalRMessageModel<SubscriberSignalRModel> requestModel = new SignalRMessageModel<SubscriberSignalRModel>()
            {
                Channel = ConstantSignalRChannels.Unsubscribed,
                ConnectionId = Context.ConnectionId,
                UserId = Guid.Empty,
                Message = new SubscriberSignalRModel(Symbol, false)
            };
            bool ClientIsSubscribe = clientService.ClientIsSubscribe(Symbol, Context.ConnectionId);
            if (ClientIsSubscribe)
            {
                clientService.UnsubscribeChannel(Symbol, Context.ConnectionId);
                await Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.Unsubscribed, requestModel).ConfigureAwait(false);
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.Unsubscribed, $"{requestModel.ToJsonSerialize()}");
                await Clients.Clients(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Unsubscribed, true);
            }
            else
            {
                LoggerManager.AddInfoFileName(ConstantLoggerFileName.Unsubscribed, JsonConvert.SerializeObject(new { ClientIsSubscribe, Request = requestModel}));
            }
            requestModel.Dispose();
        }        
    }
    [HubMethodName(ConstantHubMethods.GetSubscribers)]
    public async Task GetSubscribers()
    {
        if (string.IsNullOrWhiteSpace(EngineConnectionId))
        {
            await Clients.Client(Context.ConnectionId).SendAsync(ConstantSignalRChannels.Warning, "Engine bağlı değil!").ConfigureAwait(false);
        }
        else
        {
            Dictionary<string, List<string>> values = clientService.GetSubscribers();
            await Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.Subscribers, values);
            LoggerManager.AddInfoFileName(ConstantLoggerFileName.Subscribers, JsonConvert.SerializeObject(values));
            GC.SuppressFinalize(values);
        }
    }
    [HubMethodName(ConstantHubMethods.SendEngineIsOnline)]
    public Task SendEngineIsOnline(string ConnectionId)
    {
        return SendToClientsEngineIsOnlineState(ConnectionId);
    }
    #endregion

    #region PRIVATE METHODS
    private Task SendToEngineConnectedClient()
    {
        if (string.IsNullOrWhiteSpace(EngineConnectionId))
        {
            return Task.CompletedTask;
        }
        SignalRMessageModel<string> requestModel = new SignalRMessageModel<string>()
        {
            Channel = ConstantSignalRChannels.Unsubscribed,
            ConnectionId = Context.ConnectionId,
            UserId = Guid.Empty,
            Message = ConstantCommon.PriceHub
        };
        LoggerManager.AddInfoFileName(ConstantLoggerFileName.ClientConnected, $"{requestModel.ToJsonSerialize()}");
        requestModel.Dispose();
        return Clients.Client(EngineConnectionId).SendAsync(ConstantSignalRChannels.ClientConnected, requestModel);
    }
    private Task SendToClientsEngineIsOnlineState(string ConnectionId = null)
    {
        if (string.IsNullOrWhiteSpace(ConnectionId))
        {
            return Clients.AllExcept(EngineConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, string.IsNullOrWhiteSpace(EngineConnectionId));
        }
        else
        {
            return Clients.Client(ConnectionId).SendAsync(ConstantSignalRChannels.EngineIsOnline, !string.IsNullOrWhiteSpace(EngineConnectionId));
        }
    }
    private string GetEngineType()
    {
        string headerValue = "";
        HttpRequest httpRequest = null;
        try
        {
            httpRequest = Context.GetHttpContext().Request;
            headerValue = httpRequest.Headers["EngineType"];
            if (string.IsNullOrWhiteSpace(headerValue) == true)
            {
                headerValue = httpRequest.Query["EngineType"];
            }
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally
        {
            if (httpRequest != null) GC.SuppressFinalize(httpRequest);
        }
        return headerValue;
    }
    private string? GetAccessTokenByRequest()
    {
        string accessToken = null;
        HttpRequest httpRequest = null;
        try
        {
            httpRequest = Context.GetHttpContext().Request;
            accessToken = httpRequest.Headers["Authorization"];
            LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromHeaders|Authorization|{accessToken}");
            if (string.IsNullOrEmpty(accessToken))
            {
                accessToken = httpRequest.Query["access_token"];
                LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromQuery|access_token|{accessToken}");
                if (string.IsNullOrWhiteSpace(accessToken))
                {
                    accessToken = (string)httpRequest.RouteValues[ConstantCommon.access_token];
                    LoggerManager.AddInfoDebugMode($"GetAccessTokenByRequest|FromRoute|access_token|{accessToken}");
                }
            }
            if (string.IsNullOrWhiteSpace(accessToken) == false)
            {
                if (accessToken.StartsWith(ConstantCommon.AuthorizationType))
                {
                    accessToken = accessToken[ConstantCommon.AuthorizationType.Length..];
                }
                accessToken = accessToken.Trim();
                accessToken = accessToken.Replace("\"", "");
            }
            httpRequest = null;
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            GC.SuppressFinalize(exc);
        }
        finally
        {
            if (httpRequest != null) GC.SuppressFinalize(httpRequest);
        }
        return accessToken;
    }
    //public async Task<TokenCheckInfo> GetTokenCheckInfo(string accessToken = null)
    //{
    //    if (string.IsNullOrWhiteSpace(accessToken)) accessToken = GetAccessTokenByRequest();
    //    if (string.IsNullOrWhiteSpace(accessToken) == false)
    //    {
    //        try
    //        {
    //            return await TokenCheckManager.CheckIn(accessToken).ConfigureAwait(false);
    //        }
    //        catch (Exception exc)
    //        {
    //            LoggerQueueManager.AddErrorFileName(LoggerFileNameConstants.Error, exc.ExtractToString());
    //            GC.SuppressFinalize(exc);
    //        }
    //    }
    //    return null;
    //}
    //private async Task<Guid> GetUserId()
    //{
    //    string AccessToken = GetAccessTokenByRequest();
    //    using (TokenCheckInfo tokenCheckInfo = await GetTokenCheckInfo(AccessToken).ConfigureAwait(false))
    //    {
    //        if (tokenCheckInfo is null)
    //        {
    //            await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //            Thread.Sleep(5);
    //            Context.Abort();
    //            //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //        }
    //        else
    //        {
    //            using (FTP.Shared.Entities.Account.UserGetDBResult DBUserInfo = tokenCheckInfo.DBUserInfo)
    //            {
    //                if (DBUserInfo is null)
    //                {
    //                    await Clients.Client(Context.ConnectionId).SendAsync(SignalRChannelConstants.Unauthorized, $"Geçersiz access token: {AccessToken}").ConfigureAwait(false);
    //                    Thread.Sleep(5);
    //                    Context.Abort();
    //                    //ConnectionId2HubCaller[Context.ConnectionId].Abort();
    //                }
    //                else
    //                {
    //                    return DBUserInfo.UserID;
    //                }
    //            }
    //        }
    //    }
    //    return Guid.Empty;
    //}
    #endregion
}
