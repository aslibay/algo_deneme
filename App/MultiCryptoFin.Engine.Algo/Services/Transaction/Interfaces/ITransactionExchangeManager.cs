﻿using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
public interface ITransactionExchangeManager: IDisposable
{
    void Add(TransactionsDto Request);
}
