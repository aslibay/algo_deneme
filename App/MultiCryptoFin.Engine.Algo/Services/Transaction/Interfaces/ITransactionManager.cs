﻿using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
public interface ITransactionManager : IDisposable
{
    void Add(TransactionsDto Request);
    List<TransactionsDto> GetTransactions();
}
