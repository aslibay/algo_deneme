﻿using MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Transaction.Interfaces;
using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Engine.Algo.Services.Transaction;
public class TransactionExchangeManager : ITransactionExchangeManager
{    
    private readonly IQueueManager<TransactionsDto> TransactionQueue;
    private readonly ITransactionSenderManager TransactionSenderManager;
    private readonly ILoggerManager LoggerManager;
    private readonly IStackExchangeRedisManager RedisPublisherManager;
    private readonly string ExchangeName;


    public TransactionExchangeManager(string ExchangeName,
        ILoggerManager LoggerManager,
        IStackExchangeRedisManager RedisPublisherManager,
        ITransactionSenderManager TransactionSenderManager)
    {
        this.ExchangeName = ExchangeName;
        this.LoggerManager = LoggerManager;
        this.RedisPublisherManager = RedisPublisherManager;
        this.TransactionSenderManager = TransactionSenderManager;

        TransactionQueue = new QueueManager<TransactionsDto>("TransactionQueue");
        TransactionQueue.OnEventTriggered += TransactionQueue_OnEventTriggered;
    }



    #region QueueManager Events

    #region TransactionQueue
    private void TransactionQueue_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<TransactionsDto> e)
    {
        TransactionSenderManager.Add(e.Item);
        e.Dispose();
    }
    #endregion

    #endregion

    public void Add(TransactionsDto request)
    {
        TransactionQueue.Add(request);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~TransactionExchangeManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
