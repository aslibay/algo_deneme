﻿using System.Collections.Concurrent;
using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Transaction.Interfaces;
using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Engine.Algo.Services.Transaction;
public class TransactionManager : ITransactionManager
{
    private readonly ConcurrentDictionary<string, ITransactionExchangeManager> ExchangeName2TransactionManager = new ConcurrentDictionary<string, ITransactionExchangeManager>();

    /*         
        {
          "BINANCE": [
            {}
          ],
          "BYBIT": [
            {}
          ],
          "...": [
            {}
          ]
        }
     */
    private readonly ConcurrentDictionary<string, List<TransactionsDto>> Exchange2Transactions = new ConcurrentDictionary<string, List<TransactionsDto>>();

    private readonly ILoggerManager LoggerManager;
    private readonly IStackExchangeRedisManager RedisPublisherManager;
    private readonly ITransactionSenderManager TransactionSenderManager;
    private readonly IQueueManager<TransactionsDto> TransactionQueue;

    public TransactionManager(ILoggerManager LoggerManager,
        IStackExchangeRedisManager RedisPublisherManager,
        ITransactionSenderManager TransactionSenderManager)
    {
        this.LoggerManager = LoggerManager;
        this.RedisPublisherManager = RedisPublisherManager;
        this.TransactionSenderManager = TransactionSenderManager;

        TransactionQueue = new QueueManager<TransactionsDto>("TransactionQueue");
        TransactionQueue.OnEventTriggered += TransactionQueue_OnEventTriggered;
    }

    #region QueueManager Events

    #region TransactionQueue
    private void TransactionQueue_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<TransactionsDto> e)
    {
        try
        {
            CheckInExchaneQueueWorker(e.Item.Exchange);
            ExchangeName2TransactionManager[e.Item.Exchange].Add(e.Item);
            Exchange2Transactions.AddOrUpdate(e.Item.Exchange,
                (k) =>
                {
                    return new List<TransactionsDto>() { e.Item };
                },
                (k, v) =>
                {
                    v.Add(e.Item);
                    return v;
                });
            e.Dispose();
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
        }
    }
    #endregion

    #endregion

    #region ChecInExchaneQueueWorker
    private void CheckInExchaneQueueWorker(string ExchangeName)
    {
        if (ExchangeName2TransactionManager.ContainsKey(ExchangeName) == false)
        {
            ExchangeName2TransactionManager.TryAdd(ExchangeName, new TransactionExchangeManager(ExchangeName, LoggerManager, RedisPublisherManager, TransactionSenderManager));
        }
    }
    #endregion

    public void Add(TransactionsDto Request)
    {
        TransactionQueue.Add(Request);
    }
    public List<TransactionsDto> GetTransactions()
    {
        List<TransactionsDto> result = new List<TransactionsDto>();
        foreach (var iter in Exchange2Transactions.Values.ToList())
        {
            result.AddRange(iter);
        }
        return result;
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~TransactionManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
