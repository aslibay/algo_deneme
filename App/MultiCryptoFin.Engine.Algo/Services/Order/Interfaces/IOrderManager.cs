﻿using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
using MultiCryptoFin.Models.Orders.UI;

namespace MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
public interface IOrderManager : IDisposable
{
    void AddNewOrder(NewOrderUIRequestReceiveDto request);
    void AddReplaceOrder(ReplaceOrderUIRequestReceiveDto request);
    void AddCancelOrder(CancelOrderUIRequestReceiveDto request);
    void AddResponse(OrderExchangeResponseReceiveDto Request);
}
