﻿using MultiCryptoFin.Models.Orders.Base;
using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;

namespace MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
public interface IOrderExchangeManager : IDisposable
{
    void AddRequest(RequestOrderDto Request);
    void AddResponse(OrderExchangeResponseReceiveDto request);
}
