﻿using System.Collections.Concurrent;
using MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.OpenOrderHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
using MultiCryptoFin.Models.Orders.Base;
using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
using MultiCryptoFin.Models.Orders.UI;

namespace MultiCryptoFin.Engine.Algo.Services.Order;
public class OrderManager : IOrderManager
{    
    private readonly ConcurrentDictionary<string, IOrderExchangeManager> ExchangeName2OrderManager = new ConcurrentDictionary<string, IOrderExchangeManager>();

    private readonly ILoggerManager _loggerManager;
    private readonly IStackExchangeRedisManager _redisPublisherManager;
    private readonly IOpenOrderHubManager _openOrderHubManager;
    private readonly IQueueManager<RequestOrderDto> _orderRequest;
    private readonly IQueueManager<OrderExchangeResponseReceiveDto> _orderResponse;
    private readonly ITransactionManager _transactionManager;

    public OrderManager(ILoggerManager loggerManager,
        IStackExchangeRedisManager redisPublisherManager,
        IOpenOrderHubManager openOrderHubManager, 
        ITransactionManager transactionManager)
    {
        _loggerManager = loggerManager;
        _redisPublisherManager = redisPublisherManager;
        _openOrderHubManager = openOrderHubManager;
        _transactionManager = transactionManager;

        _orderRequest = new QueueManager<RequestOrderDto>("OrderRequest");
        _orderRequest.OnEventTriggered += OrderRequest_OnEventTriggered;

        _orderResponse = new QueueManager<OrderExchangeResponseReceiveDto>("OrderResponse");
        _orderResponse.OnEventTriggered += OrderResponse_OnEventTriggered;
        
    }
    #region Queue Manager Events
    private void OrderRequest_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<RequestOrderDto> e)
    {
        CheckInExchaneQueueWorker(e.Item.Exchange);
        ExchangeName2OrderManager[e.Item.Exchange].AddRequest(e.Item);
    }
    private void OrderResponse_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<OrderExchangeResponseReceiveDto> e)
    {
        CheckInExchaneQueueWorker(e.Item.Exchange);
        ExchangeName2OrderManager[e.Item.Exchange].AddResponse(e.Item);
        e.Dispose();
    }
    #endregion

    #region ChecInExchaneQueueWorker
    private object ChecInExchaneQueueWorker_Locked = new object();
    private void CheckInExchaneQueueWorker(string ExchangeName)
    {
        lock(ChecInExchaneQueueWorker_Locked)
        {
            if (ExchangeName2OrderManager.ContainsKey(ExchangeName) == false)
            {
                ExchangeName2OrderManager.TryAdd(ExchangeName, new OrderExchangeManager(ExchangeName, _loggerManager, _redisPublisherManager, _openOrderHubManager, _transactionManager));
            }
        }
    } 
    #endregion

    public void AddNewOrder(NewOrderUIRequestReceiveDto request)
    {
        _orderRequest.Add(new RequestOrderDto(request));
    }
    public void AddReplaceOrder(ReplaceOrderUIRequestReceiveDto request)
    {
        _orderRequest.Add(new RequestOrderDto(request));
    }
    public void AddCancelOrder(CancelOrderUIRequestReceiveDto request)
    {
        _orderRequest.Add(new RequestOrderDto(request));
    }
    public void AddResponse(OrderExchangeResponseReceiveDto Request)
    {
        _orderResponse.Add(Request);
    }
    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~OrderManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    } 
    #endregion
}
