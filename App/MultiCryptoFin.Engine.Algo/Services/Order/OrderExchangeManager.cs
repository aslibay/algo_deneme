﻿using System.Collections.Concurrent;
using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.OpenOrderHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
using MultiCryptoFin.Models.Orders.Base;
using MultiCryptoFin.Models.Orders.Exchange.RequestSender;
using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;

namespace MultiCryptoFin.Engine.Algo.Services.Order;
public class OrderExchangeManager : IOrderExchangeManager
{
    #region New Order, Replace Order, Cancel Order istekleri atıldığında ilk olarak buraya eklenecek ham order listeme eklenmesin ki UI 'da bilgilendirme yapmayalım!
    private readonly ConcurrentDictionary<object, OrderDto> RequestOrderList = new ConcurrentDictionary<object, OrderDto>();
    #endregion
    #region Nihai borsadaki Order listesi
    private readonly ConcurrentDictionary<object, OrderDto> Orders = new ConcurrentDictionary<object, OrderDto>();
    #endregion    

    private readonly IQueueManager<RequestOrderDto> _orderRequest;
    private readonly IQueueManager<OrderExchangeResponseReceiveDto> _orderResponse;
    private readonly IOpenOrderHubManager _openOrderHubManager;
    private readonly ILoggerManager _loggerManager;
    private readonly IStackExchangeRedisManager _redisPublisherManager;
    private readonly ITransactionManager _transactionManager;
    private readonly string ExchangeName;


    public OrderExchangeManager(string ExchangeName,
        ILoggerManager LoggerManager,
        IStackExchangeRedisManager RedisPublisherManager,
        IOpenOrderHubManager OpenOrderHubManager, 
        ITransactionManager transactionManager)
    {
        this.ExchangeName = ExchangeName;
        _loggerManager = LoggerManager;
        _redisPublisherManager = RedisPublisherManager;
        _openOrderHubManager = OpenOrderHubManager;
        _transactionManager = transactionManager;

        _orderRequest = new QueueManager<RequestOrderDto>("OrderRequest");
        _orderRequest.OnEventTriggered += OrderRequest_OnEventTriggered;

        _orderResponse = new QueueManager<OrderExchangeResponseReceiveDto>("OrderResponse");
        _orderResponse.OnEventTriggered += OrderResponse_OnEventTriggered;        
    }

    #region QueueManager Events

    #region OrderRequest
    private NewOrderExchangeRequestSenderDto OrderRequest_NewOrder = new NewOrderExchangeRequestSenderDto();
    private ReplaceOrderExchangeRequestSenderDto OrderRequest_ReplaceOrder = new ReplaceOrderExchangeRequestSenderDto();
    private CancelOrderExchangeRequestSenderDto OrderRequest_CancelOrder = new CancelOrderExchangeRequestSenderDto();
    private void OrderRequest_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<RequestOrderDto> e)
    {
        try
        {
            if (e.Item.RequestType == Shared.Enums.EnumsRequestType.NewOrder)
            {
                RequestOrderList.TryAdd(e.Item.CustomOrderId, new OrderDto(e.Item));
                OrderRequest_NewOrder.Set(RequestOrderList[e.Item.CustomOrderId]);
                _loggerManager.AddInfoFileName("sena", JsonConvert.SerializeObject(e.Item.ApiId) );

                _redisPublisherManager.Publish($"{ConstantRedisChannels.Order}:{ConstantRedisChannels.Request}:{ExchangeName}:{ConstantRedisChannels.New}:{e.Item.ApiId}", OrderRequest_NewOrder.ToMessagePackSerialize(), StackExchange.Redis.CommandFlags.FireAndForget);
                OrderRequest_NewOrder.SetEmpty();
            }
            else if (e.Item.RequestType == Shared.Enums.EnumsRequestType.ReplaceOrder)
            {
                if (string.IsNullOrWhiteSpace(e.Item.OrderId) == false)
                {
                    if (Orders.TryGetValue(e.Item.OrderId, out OrderDto outOrder))
                    {
                        RequestOrderList.TryAdd(e.Item.CustomOrderId, new OrderDto(outOrder, e.Item));
                        OrderRequest_ReplaceOrder.Set(RequestOrderList[e.Item.CustomOrderId]);
                        _redisPublisherManager.Publish($"{ConstantRedisChannels.Order}:{ConstantRedisChannels.Request}:{ExchangeName}:{ConstantRedisChannels.Replace}", OrderRequest_ReplaceOrder.ToMessagePackSerialize(), StackExchange.Redis.CommandFlags.FireAndForget);
                        OrderRequest_ReplaceOrder.SetEmpty();
                        outOrder.Dispose();
                    }
                }
            }
            else if (e.Item.RequestType == Shared.Enums.EnumsRequestType.CancelOrder)
            {
                if (string.IsNullOrWhiteSpace(e.Item.OrderId) == false)
                {
                    if (Orders.TryGetValue(e.Item.OrderId, out OrderDto outOrder))
                    {
                        RequestOrderList.TryAdd(outOrder.OrderId, outOrder);
                        OrderRequest_CancelOrder.Set(outOrder);
                        _redisPublisherManager.Publish($"{ConstantRedisChannels.Order}:{ConstantRedisChannels.Request}:{ExchangeName}:{ConstantRedisChannels.Cancel}", OrderRequest_CancelOrder.ToMessagePackSerialize(), StackExchange.Redis.CommandFlags.FireAndForget);
                        OrderRequest_CancelOrder.SetEmpty();
                        outOrder.Dispose();
                    }
                }
            }
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
        }
        //Orders.TryAdd(e.Item.CustomOrderId, new OrderDto(e.Item));
    }
    #endregion

    #region OrderResponse
    private TransactionsDto OrderResponse_Execution = new TransactionsDto();
    private void OrderResponse_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<OrderExchangeResponseReceiveDto> e)
    {
        try
        {
            if (e.Item.MsgType == ConstantMsgTypes.MessageType_Accepted)
            {
                #region ACCEPTED
                if (e.Item.CustomOrderId != null && RequestOrderList.ContainsKey(e.Item.CustomOrderId))
                {
                    if (RequestOrderList.TryRemove(e.Item.CustomOrderId, out OrderDto outOrder1))
                    {
                        if (Orders.TryGetValue(e.Item.OrderId, out OrderDto outOrder2))
                        {
                            outOrder2.MsgType = ConstantMsgTypes.MessageType_Replaced;
                            outOrder2.CustomOrderId = e.Item.CustomOrderId;
                            _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, outOrder2);
                            outOrder2.Dispose();
                        }
                        else
                        {
                            outOrder1.MsgType = e.Item.MsgType;
                            outOrder1.OrderId = e.Item.OrderId;
                            Orders.TryAdd(outOrder1.OrderId, outOrder1);
                            _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, outOrder1);
                        }
                        outOrder1.Dispose();
                    }
                }
                else
                {
                    if (Orders.TryGetValue(e.Item.OrderId, out OrderDto outOrderDto))
                    {
                        if (outOrderDto.CustomOrderId == null)
                        {
                            outOrderDto.CustomOrderId = e.Item.CustomOrderId;
                            outOrderDto.MsgType = ConstantMsgTypes.MessageType_Replaced;
                            _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, Orders[e.Item.OrderId]);
                        }
                        outOrderDto.Dispose();
                    }
                    else
                    {
                        Orders.TryAdd(e.Item.OrderId, new OrderDto(e.Item));
                        _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, Orders[e.Item.OrderId]);
                    }                    
                }
                #endregion
            }
            else if (e.Item.MsgType == ConstantMsgTypes.MessageType_Canceled)
            {
                #region CANCELED
                if (RequestOrderList.TryRemove(e.Item.OrderId, out OrderDto outOrder))
                {
                    outOrder.MsgType = e.Item.MsgType;
                    if (Orders.TryRemove(outOrder.OrderId, out OrderDto removeOrder))
                    {
                        removeOrder.Dispose();
                    }
                    _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, outOrder);
                    outOrder.Dispose();
                }
                else
                {
                    if (Orders.TryRemove(e.Item.OrderId, out OrderDto removeOrder))
                    {
                        removeOrder.MsgType = e.Item.MsgType;
                        _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, removeOrder);
                        removeOrder.Dispose();
                    }
                }
                #endregion
            }
            else if (e.Item.MsgType == ConstantMsgTypes.MessageType_Rejected)
            {
                #region REJECTED
                if (RequestOrderList.TryRemove(e.Item.CustomOrderId, out OrderDto outOrder))
                {
                    outOrder.MsgType = e.Item.MsgType;
                    outOrder.ResponseCode = e.Item.ResponseCode;
                    outOrder.ResponseMessage = e.Item.ResponseMessage;
                    _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroupWarning, outOrder);
                    outOrder.Dispose();
                }
                #endregion
            }
            else if (e.Item.MsgType == ConstantMsgTypes.MessageType_Replaced)
            {
                #region REPLACED
                if (RequestOrderList.TryRemove(e.Item.CustomOrderId, out OrderDto outOrder))
                {
                    if (Orders.TryRemove(outOrder.PreviousOrderId, out OrderDto outOrderReplaced))
                    {
                        outOrderReplaced.MsgType = e.Item.MsgType;
                        outOrderReplaced.OrderId = e.Item.OrderId;
                        outOrderReplaced.CustomOrderId = e.Item.CustomOrderId;
                        outOrderReplaced.Price = outOrder.Price;
                        outOrderReplaced.Quantity = outOrder.Quantity;

                        Orders.TryAdd(outOrderReplaced.OrderId, outOrderReplaced);
                        _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, outOrderReplaced);
                        outOrderReplaced.Dispose();
                    }
                    outOrder.Dispose();
                }
                else 
                {
                    if (Orders.TryGetValue(e.Item.OrderId, out OrderDto outOrderReplaced))
                    {
                        outOrderReplaced.MsgType = e.Item.MsgType;
                        outOrderReplaced.OrderId = e.Item.OrderId;
                        outOrderReplaced.Price = outOrder.Price;
                        outOrderReplaced.Quantity = outOrder.Quantity;
                        _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, outOrderReplaced);
                        outOrderReplaced.Dispose();
                    }
                }
                #endregion
            }
            else if (e.Item.MsgType == ConstantMsgTypes.MessageType_Execution)
            {
                #region EXECUTION
                if (Orders.TryGetValue(e.Item.OrderId, out OrderDto outOrderExecution))
                {
                    outOrderExecution.MsgType = e.Item.MsgType;
                    outOrderExecution.ExecutedQuantity += e.Item.ExecutedQuantity;
                    if (outOrderExecution.LeaveQuantity == 0)
                    {
                        if (Orders.TryRemove(e.Item.OrderId, out OrderDto outOrderRemove))
                        {
                            outOrderRemove.Dispose();
                        }
                    }
                    _openOrderHubManager.SendByQueue(ConstantHubMethods.SendGroup, outOrderExecution);
                    ////// <<< EXECUTION MANAGER
                    OrderResponse_Execution.Set(e.Item, outOrderExecution);
                    _transactionManager.Add(OrderResponse_Execution);
                    OrderResponse_Execution.SetEmpty();
                    ///// >>>
                    outOrderExecution.Dispose();
                }
                #endregion
            }
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
        }
    }
    #endregion

    #endregion

    public void AddRequest(RequestOrderDto Request)
    {
        _orderRequest.Add(Request);
    }
    public void AddResponse(OrderExchangeResponseReceiveDto request)
    {
        _orderResponse.Add(request);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~OrderExchangeManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
