﻿using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
public interface IPriceExchangeManager: IDisposable
{
    void Add(PriceDto Request);
}
