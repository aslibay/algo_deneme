﻿using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
public interface IPriceQueueManager: IDisposable
{
    void Add(PriceDto Request);
}
