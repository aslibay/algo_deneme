﻿using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
public interface IPriceManager: IDisposable
{
    void Add(PriceDto Request);
}
