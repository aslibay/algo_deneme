﻿using System.Collections.Concurrent;
using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.PriceHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Price.Interfaces;
using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.Price;
public class PriceManager : IPriceManager
{
    private readonly ConcurrentDictionary<string, IPriceExchangeManager> ExchangeName2PriceManager = new ConcurrentDictionary<string, IPriceExchangeManager>();

    private readonly ILoggerManager LoggerManager;
    private readonly IStackExchangeRedisManager RedisPublisherManager;
    private readonly IQueueManager<PriceDto> PriceQueue;
    private readonly IPriceSenderManager PriceSenderManager;

    public PriceManager(ILoggerManager LoggerManager,
        IStackExchangeRedisManager RedisPublisherManager,
        IPriceSenderManager PriceSenderManager)
    {        
        this.LoggerManager = LoggerManager;
        this.RedisPublisherManager = RedisPublisherManager;
        this.PriceSenderManager = PriceSenderManager;

        PriceQueue = new QueueManager<PriceDto>("PriceQueue");
        PriceQueue.OnEventTriggered += PriceQueue_OnEventTriggered;
    }

    #region PriceQueue
    private void PriceQueue_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<PriceDto> e)
    {
        try
        {
            CheckInExchaneQueueWorker(e.Item.Exchange);
            ExchangeName2PriceManager[e.Item.Exchange].Add(e.Item);
            e.Dispose();
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
        }
    }
    #endregion

    #region ChecInExchaneQueueWorker
    private object ChecInExchaneQueueWorker_Locked = new object();
    private void CheckInExchaneQueueWorker(string ExchangeName)
    {
        lock (ChecInExchaneQueueWorker_Locked)
        {
            if (ExchangeName2PriceManager.ContainsKey(ExchangeName) == false)
            {
                ExchangeName2PriceManager.TryAdd(ExchangeName, new PriceExchangeManager(ExchangeName, LoggerManager, RedisPublisherManager, PriceSenderManager));
            }
        }
    }
    #endregion

    public void Add(PriceDto Request)
    {
        PriceQueue.Add(Request);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~PriceManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    } 
    #endregion
}
