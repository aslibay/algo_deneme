﻿using System.Collections.Concurrent;
using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.PriceHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Price.Interfaces;
using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.Price;
public class PriceExchangeManager : IPriceExchangeManager
{
    /// <summary>
    ///  Symbol => Queue
    /// </summary>
    //private readonly ConcurrentDictionary<string, IPriceQueueManager> Prices = new ConcurrentDictionary<string, IPriceQueueManager>();

    private readonly ConcurrentDictionary<string, PriceDto> PriceData = new ConcurrentDictionary<string, PriceDto>();

    private readonly string ExchangeName;
    private readonly IQueueManager<PriceDto> PriceQueue;
    private readonly ILoggerManager LoggerManager;
    private readonly IStackExchangeRedisManager RedisPublisherManager;   
    private readonly IPriceSenderManager PriceSenderManager;

    public PriceExchangeManager(string ExchangeName,
        ILoggerManager LoggerManager,
        IStackExchangeRedisManager RedisPublisherManager,
        IPriceSenderManager PriceSenderManager)
    {
        this.ExchangeName = ExchangeName;
        this.LoggerManager = LoggerManager;
        this.RedisPublisherManager = RedisPublisherManager;
        this.PriceSenderManager = PriceSenderManager;

        PriceQueue = new QueueManager<PriceDto>("PriceQueue");
        PriceQueue.OnEventTriggered += PriceQueue_OnEventTriggered;
    }

    #region QueueManager Events

    #region PriceQueue

    private void PriceQueue_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<PriceDto> e)
    {
        try
        {
            PriceData.AddOrUpdate(e.Item.Symbol, e.Item,
            (k, v) =>
            {
                v = e.Item;
                return v;
            });
            PriceSenderManager.Add(e.Item);
        }
        catch (Exception exc)
        {
            LoggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
        }
    }
    #endregion

    #endregion

    public void Add(PriceDto Request)
    {
        PriceQueue.Add(Request);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~TransactionExchangeManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
