﻿using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.AlgoHub.Interfaces;
using MultiCryptoFin.Shared.Commons;
using System;

namespace MultiCryptoFin.Engine.Algo.Services.Background.SignalRHubConnectionChecker;
public class HubConnectionService : BackgroundService
{
    private static bool IsInitialized { get; set; } = false;
    private readonly string ServiceFileName = "HubConnectionService";

    private readonly ILoggerManager LoggerQueueManager;
    private readonly IAlgoHubManager AlgoHubManager;
    private string AccessToken { get; set; }
    private readonly string SocketAddress;
    private readonly string HubAlgoAddress;

    /// <summary>
    /// SignalRClientServiceAlgo
    /// </summary>
    /// <param name="LoggerQueueManager"></param>
    public HubConnectionService(ILoggerManager LoggerQueueManager,
        IAlgoHubManager AlgoHubManager
        )
    {
        this.LoggerQueueManager = LoggerQueueManager;
        this.AlgoHubManager = AlgoHubManager;

        if (IsInitialized == false)
        {
            IsInitialized = true;


            this.AlgoHubManager.OnConnected += AlgoHubManager_OnConnected;
            this.AlgoHubManager.OnUnauthorized += AlgoHubManager_OnUnauthorized;
            this.AlgoHubManager.OnDisconnected += AlgoHubManager_OnDisconnected;

            SocketAddress = Definitions.EngineAppSetting.SocketSettings.Address;
            if (SocketAddress.EndsWith("/") == false)
            {
                SocketAddress = $"{SocketAddress}/";
            }
            HubAlgoAddress = $"{SocketAddress}algo";
        }
        LoggerQueueManager.AddInfoFileName(ServiceFileName, $"Yapıcı metod çalıştı");
    }


    #region AlgoHubManager
    private void AlgoHubManager_OnConnected(object? sender, FinTech.SignalRClientService.Commons.MyEventArgs<string> e)
    {
        LoggerQueueManager.AddInfoFileName(ServiceFileName, $"State|AlgoHubManager|Connected|{e.Item}");
        e.Dispose();
    }
    private void AlgoHubManager_OnDisconnected(object? sender, FinTech.SignalRClientService.Commons.MyEventArgs<string> e)
    {
        LoggerQueueManager.AddInfoFileName(ServiceFileName, $"State|AlgoHubManager|Disconnected|{e.Item}");
        AlgoHubCheckInToken().GetAwaiter().GetResult();
        e.Dispose();
    }
    private void AlgoHubManager_OnUnauthorized(object? sender, FinTech.SignalRClientService.Commons.MyEventArgs<string> e)
    {
        LoggerQueueManager.AddInfoFileName(ServiceFileName, $"State|AlgoHubManager|Unauthorized|{e.Item}");
        AlgoHubCheckInToken().GetAwaiter().GetResult();
        e.Dispose();
    }
    #endregion




    /// <summary>
    /// StartAsync
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public override Task StartAsync(CancellationToken cancellationToken)
    {
        LoggerQueueManager.AddInfoFileName(ServiceFileName, "Servis çalışıyor.");
        return base.StartAsync(cancellationToken);
    }
    /// <summary>
    /// StopAsync
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public override Task StopAsync(CancellationToken cancellationToken)
    {
        LoggerQueueManager.AddInfoFileName(ServiceFileName, "Servis durdu!");
        return base.StopAsync(cancellationToken);
    }
    /// <summary>
    /// ExecuteAsync
    /// </summary>
    /// <param name="stoppingToken"></param>
    /// <returns></returns>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        // test için kullandım
        //Timer timer = new Timer(DoWorkColumnColor, null, TimeSpan.Zero, TimeSpan.FromMilliseconds(1));
        //TaskRun();

        while (!stoppingToken.IsCancellationRequested)
        {
            await AlgoHubCheckInToken().ConfigureAwait(false);
            await Task.Delay(TimeSpan.FromDays(1), stoppingToken).ConfigureAwait(false);
            LoggerQueueManager.AddInfoFileName(ServiceFileName, "Servis çalışmaya devam ediyor.");
        }
    }






    private async Task AlgoHubCheckInToken()
    {
        List<KeyValuePair<string, string>> _headers = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("EngineId", MultiCryptoFin.Shared.Commons.Definitions.EngineId)
        };

        while (AlgoHubManager.IsConnected == false)
        {
            try
            {
                LoggerQueueManager.AddInfoFileName(ServiceFileName, $"{HubAlgoAddress} adresine bağlanıyor...");
                await AlgoHubManager.Disconnect().ConfigureAwait(false);
                await AlgoHubManager.Connect(Url: HubAlgoAddress,
                    Headers: _headers,
                    UseMessagePackProtocol: true).ConfigureAwait(false);
            }
            catch (Exception exc)
            {
                LoggerQueueManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());

            }
            Thread.Sleep(250);
        }
        LoggerQueueManager.AddInfoFileName(ServiceFileName, $"{HubAlgoAddress} adresine bağlandı.");
    }


}
