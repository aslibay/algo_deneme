﻿namespace MultiCryptoFin.Engine.Algo.Services.Background.SignalRHubConnectionChecker.Common;

/// <summary>
/// TokenCheckInManager
/// </summary>
public class TokenCheckInManager
{
    private readonly string Email;
    private readonly string Password;

    /// <summary>
    /// TokenCheckInManager
    /// </summary>
    /// <param name="Email"></param>
    /// <param name="Password"></param>
    public TokenCheckInManager(string Email, 
        string Password)
    {
        this.Email = Email;
        this.Password = Password;
    }

}
