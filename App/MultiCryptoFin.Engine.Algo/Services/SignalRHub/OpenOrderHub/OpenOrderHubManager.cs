﻿using MultiCryptoFin.Engine.Algo.Services.SignalRHub.OpenOrderHub.Interfaces;

namespace MultiCryptoFin.Engine.Algo.Services.SignalRHub.OpenOrderHub;
public class OpenOrderHubManager : FinTech.SignalRClientService.SignalRClientManager, IOpenOrderHubManager
{
}
