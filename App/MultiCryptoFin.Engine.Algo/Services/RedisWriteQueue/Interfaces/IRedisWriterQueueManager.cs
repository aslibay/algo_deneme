﻿using MultiCryptoFin.Engine.Algo.Services.RedisWriteQueue.Entities;

namespace MultiCryptoFin.Engine.Algo.Services.RedisWriteQueue.Interfaces;
public interface IRedisWriterQueueManager : IDisposable
{
    void AddQueue(RedisWriterQueueModel request);
    void AddQueue(Methods Method, string RedisKey, string RedisValue, TimeSpan? RedisKeyTTL = null, bool WithRoot = true);
    void AddQueueDelete(Methods Method, string RedisKey, string HashKey, bool WithRoot = true);
    void AddQueueHashMSet(string RedisKey, bool WithRoot = true, params object[] args);
    void AddQueueHashSet(string RedisKey, string HashKey, string HashValue, bool WithRoot = true);
}
