﻿namespace MultiCryptoFin.Engine.Algo.Services.RedisWriteQueue.Entities;
public struct RedisWriterQueueModel
{
    public Methods Method { get; set; }

    public string RedisKey { get; set; }
    public string RedisValue { get; set; }
    public TimeSpan? RedisKeyTTL { get; set; }

    public string HashKey { get; set; }
    public string HashValue { get; set; }

    public object[] HashMSetArgs { get; set; }

    public bool WithRoot { get; set; }

    /// <summary>
    /// Execute Redis StringSet or SetAdd Constructor
    /// </summary>
    /// <param name="Method"></param>
    /// <param name="RedisKey"></param>
    /// <param name="RedisValue"></param>
    /// <param name="RedisKeyTTL"></param>
    /// <param name="WithRoot"></param>
    public RedisWriterQueueModel(Methods Method, string RedisKey, string RedisValue, TimeSpan? RedisKeyTTL = null, bool WithRoot = true)
    {
        this.Method = Method;
        this.WithRoot = WithRoot;
        this.RedisKey = RedisKey;
        this.RedisValue = RedisValue;
        this.RedisKeyTTL = RedisKeyTTL;
        HashMSetArgs = null;
        HashKey = null;
        HashValue = null;
    }

    /// <summary>
    /// Execute Redis StringDelete, HashKeyDelete, SetRemove Constructor
    /// </summary>
    /// <param name="Method"></param>
    /// <param name="RedisKey"></param>
    /// <param name="WithRoot"></param>
    public RedisWriterQueueModel(Methods Method, string RedisKey, string HashKey, bool WithRoot = true)
    {
        this.Method = Method;
        this.WithRoot = WithRoot;
        this.RedisKey = RedisKey;
        this.HashKey = HashKey;

        RedisValue = null;
        RedisKeyTTL = null;
        HashMSetArgs = null;
        HashValue = null;
    }

    #region HashMSet
    /// <summary>
    /// Execute HashMSet with args Constructor
    /// </summary>
    /// <param name="RedisKey"></param>
    /// <param name="WithRoot"></param>
    /// <param name="args"></param>
    public RedisWriterQueueModel(string RedisKey, bool WithRoot = true, params object[] args)
    {
        Method = Methods.HashMSet;
        this.RedisKey = RedisKey;
        RedisValue = null;
        RedisKeyTTL = null;
        HashKey = null;
        HashValue = null;
        this.WithRoot = WithRoot;
        HashMSetArgs = args;
    }
    /// <summary>
    /// Execute HashMSet with single key Constructor
    /// </summary>
    /// <param name="RedisKey"></param>
    /// <param name="HashKey"></param>
    /// <param name="HashValue"></param>
    /// <param name="WithRoot"></param>
    public RedisWriterQueueModel(string RedisKey, string HashKey, string HashValue, bool WithRoot = true)
    {
        Method = Methods.HashSet;
        this.RedisKey = RedisKey;
        this.HashKey = HashKey;
        this.HashValue = HashValue;
        this.WithRoot = WithRoot;

        RedisValue = null;
        RedisKeyTTL = null;
        HashMSetArgs = null;
    }
    #endregion

    public override int GetHashCode()
    {
        return -1;
    }

    public override bool Equals(object obj)
    {
        if (!(obj is RedisWriterQueueModel))
            return false;

        return Equals((RedisWriterQueueModel)obj);
    }

    private bool Equals(RedisWriterQueueModel other)
    {
        if (Method != other.Method)
            return false;
        else if (RedisKey != other.RedisKey)
            return false;
        else if (HashValue != other.HashValue)
            return false;
        else if (HashKey != other.HashKey)
            return false;
        return true;
    }

    public static bool operator ==(RedisWriterQueueModel left, RedisWriterQueueModel right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(RedisWriterQueueModel left, RedisWriterQueueModel right)
    {
        return !(left == right);
    }
}
public enum Methods
{
    StringSet = 0,
    StringDelete = 1,
    HashSet = 2,
    HashKeyDelete = 3,
    HashMSet = 4,
    SetAdd = 5,
    SetRemove = 5
}

