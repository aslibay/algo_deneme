﻿using MultiCryptoFin.Engine.Algo.Services.RedisWriteQueue.Entities;

namespace MultiCryptoFin.Engine.Algo.Services.RedisWriteQueue;
public class RedisWriterQueueManager : IRedisWriterQueueManager
{
    private readonly IStackExchangeRedisManager RedisManager;
    private readonly IQueueManager<RedisWriterQueueModel> QueueManager;
    private readonly static int TimeElapsedMultiplier = 10;
    private static int TimeElapsed = 1;
    private static int TimeElapsedCounter = 0;

    public RedisWriterQueueManager()
    {
        QueueManager = new QueueManager<RedisWriterQueueModel>("RedisWriterQueueManager");
        QueueManager.OnEventTriggered += QueueManager_OnEventTriggered;

        RedisManager = new StackExchangeRedisManager(FinTech.StackExchangeRedisService.Commons.Definitions.Address, FinTech.StackExchangeRedisService.Commons.Definitions.Port, FinTech.StackExchangeRedisService.Commons.Definitions.Password, "RedisWriterQueue", ConnectWithConstructor: false);
        RedisManager.Connect();
    }

    public RedisWriterQueueManager(IStackExchangeRedisManager RedisManager)
    {
        QueueManager = new QueueManager<RedisWriterQueueModel>("RedisWriterQueueManager");
        QueueManager.OnEventTriggered += QueueManager_OnEventTriggered;
        this.RedisManager = RedisManager;
    }
    private void QueueManager_OnEventTriggered(object sender, FinTech.Definitions.Commons.MyEventArgs<RedisWriterQueueModel> e)
    {
        if (RedisManager.IsConnected())
        {
            ExecuteItem(e.Item);
            e.Dispose();
        }
        else
        {
            QueueManager.Add(e.Item);
        }
    }
    private void ExecuteItem(RedisWriterQueueModel item)
    {
        try
        {
            if (item.Method == Methods.HashSet)
            {
                RedisManager.HashSetAsync(item.RedisKey, item.HashKey, item.HashValue, item.WithRoot, Flags: StackExchange.Redis.CommandFlags.FireAndForget).GetAwaiter().GetResult();
            }
            else if (item.Method == Methods.StringSet)
            {
                if (item.RedisKeyTTL == null)
                {
                    RedisManager.StringSetAsync(item.RedisKey, item.RedisValue, item.WithRoot, Flags: StackExchange.Redis.CommandFlags.FireAndForget).GetAwaiter().GetResult();
                }
                else
                {
                    RedisManager.StringSetAsync(item.RedisKey, item.RedisValue, item.WithRoot, item.RedisKeyTTL, Flags: StackExchange.Redis.CommandFlags.FireAndForget).GetAwaiter().GetResult();
                }
            }
            else if (item.Method == Methods.HashKeyDelete)
            {
                RedisManager.HashDeleteAsync(item.RedisKey, item.HashKey, item.WithRoot, Flags: StackExchange.Redis.CommandFlags.FireAndForget).GetAwaiter().GetResult();
            }
            else if (item.Method == Methods.HashMSet)
            {
                RedisManager.HashMSetAsync(item.RedisKey, item.WithRoot, item.HashMSetArgs).GetAwaiter().GetResult();
            }
            else if (item.Method == Methods.StringDelete)
            {
                RedisManager.KeyDeleteAsync(item.RedisKey, item.WithRoot, Flags: StackExchange.Redis.CommandFlags.FireAndForget).GetAwaiter().GetResult();
            }
            else if (item.Method == Methods.SetAdd)
            {
                RedisManager.SetAddAsync(item.RedisKey, item.RedisValue, item.WithRoot, Flags: StackExchange.Redis.CommandFlags.FireAndForget).GetAwaiter().GetResult();
            }
            else if (item.Method == Methods.SetRemove)
            {
                RedisManager.SetAddAsync(item.RedisKey, item.RedisValue, item.WithRoot, Flags: StackExchange.Redis.CommandFlags.FireAndForget).GetAwaiter().GetResult();
            }
        }
        catch (Exception exc)
        {
            if (exc.Message.Contains("timeout"))
            {
                Thread.Sleep(TimeElapsed);
                TimeElapsedCounter++;
                TimeElapsed *= TimeElapsedMultiplier;
                if (TimeElapsedCounter <= 3)
                {
                    ExecuteItem(item);
                }
            }
        }
        finally
        {
            GC.SuppressFinalize(item);
        }
    }
    public void AddQueue(RedisWriterQueueModel request)
    {
        QueueManager.Add(request);
    }
    public void AddQueue(Methods Method, string RedisKey, string RedisValue, TimeSpan? RedisKeyTTL = null, bool WithRoot = true)
    {
        QueueManager.Add(new RedisWriterQueueModel(Method, RedisKey, RedisValue, RedisKeyTTL, WithRoot));
    }
    public void AddQueueDelete(Methods Method, string RedisKey, string HashKey, bool WithRoot = true)
    {
        QueueManager.Add(new RedisWriterQueueModel(Method, RedisKey, HashKey, WithRoot));
    }
    public void AddQueueHashMSet(string RedisKey, bool WithRoot = true, params object[] args)
    {
        QueueManager.Add(new RedisWriterQueueModel(RedisKey, WithRoot, args));
    }
    public void AddQueueHashSet(string RedisKey, string HashKey, string HashValue, bool WithRoot = true)
    {
        QueueManager.Add(new RedisWriterQueueModel(RedisKey, HashKey, HashValue, WithRoot));
    }

    #region DISPOSE PATTERN

    private bool disposedValue;
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~RedisWriteQueueManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
