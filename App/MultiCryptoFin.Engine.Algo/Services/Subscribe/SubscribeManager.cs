﻿using System.Collections.Concurrent;
using MultiCryptoFin.Engine.Algo.Services.Subscribe.Interfaces;
using MultiCryptoFin.Models.Subscribe;

namespace MultiCryptoFin.Engine.Algo.Services.Subscribe;
public class SubscribeManager : ISubscribeManager
{
    /*
        {
          "BTC/USDT": {
            "AlgoId{1}": true,
            "AlgoId{2}": true,
            "AlgoId{n}": true,
            "ConnectionId{1}": true,
            "ConnectionId{2}": true,
            "ConnectionId{n}": true
          },
          "ETH/USDT": {
            "AlgoId{1}": true,
            "AlgoId{2}": true,
            "AlgoId{n}": true,
            "ConnectionId{1}": true,
            "ConnectionId{2}": true,
            "ConnectionId{n}": true
          },
          "...": {
            "AlgoId{1}": true,
            "AlgoId{2}": true,
            "AlgoId{n}": true,
            "ConnectionId{1}": true,
            "ConnectionId{2}": true,
            "ConnectionId{n}": true
          }
        }
     */
    private readonly object _lockSubscriber = new object();
    private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, bool>> subscribers = new ConcurrentDictionary<string, ConcurrentDictionary<string, bool>>();
    private readonly IQueueManager<SubscriberEngineModel> _queue;
    private readonly ILoggerManager _loggerManager;
    private readonly IStackExchangeRedisManager _redisManager;
    public SubscribeManager(IStackExchangeRedisManager redisManager, ILoggerManager loggerManager)
    {
        _loggerManager = loggerManager;
        _redisManager = redisManager;

        _queue = new QueueManager<SubscriberEngineModel>("QueueSubscriber");
        _queue.OnEventTriggered += _queue_OnEventTriggered;
        _redisManager = redisManager;
    }
    private void _queue_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<SubscriberEngineModel> e)
    {
        Console.WriteLine(JsonConvert.SerializeObject(e));
        bool isSubscribed = false;
        lock (_lockSubscriber)
        {
            if (string.IsNullOrWhiteSpace(e.Item.Symbol) == false)
            {
                if (string.IsNullOrWhiteSpace(e.Item.Subscriber) == false)
                {
                    subscribers.AddOrUpdate(e.Item.Symbol,
                        (k) =>
                        {
                            isSubscribed = true;
                            ConcurrentDictionary<string, bool> newValue = new ConcurrentDictionary<string, bool>();
                            newValue.TryAdd(e.Item.Subscriber, true);
                            return newValue;
                        },
                        (k, v) =>
                        {
                            if (v.ContainsKey(e.Item.Subscriber) == false)
                            {
                                isSubscribed = true;
                                v.TryAdd(e.Item.Subscriber, true);
                            }
                            return v;
                        });
                    if (isSubscribed) _redisManager.PublishAsync($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:{ConstantRedisChannels.Subscribe}", e.Item.Symbol, StackExchange.Redis.CommandFlags.FireAndForget);
                }
                else if (string.IsNullOrWhiteSpace(e.Item.Unsubscriber) == false)
                {
                    if (subscribers.TryGetValue(e.Item.Symbol, out ConcurrentDictionary<string, bool> outSubscribers))
                    {
                        outSubscribers.TryRemove(e.Item.Unsubscriber, out bool outValue);
                    }
                    _redisManager.PublishAsync($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:{ConstantRedisChannels.Unsubscribe}", e.Item.Symbol, StackExchange.Redis.CommandFlags.FireAndForget);
                }
            }
        }
    }

    public void Add(SubscriberEngineModel request)
    {
        _queue.Add(request);
    }

    private Dictionary<string, int> GetSubscribers()
    {
        Dictionary<string, int> result = new Dictionary<string, int>();
        foreach (var iter in subscribers)
        {
            result.TryAdd(iter.Key, iter.Value.Count);
        }
        return result;
    }
    public void Set(Dictionary<string, List<string>> request)
    {
        Dictionary<string, int> result = new Dictionary<string, int>();
        lock (_lockSubscriber)
        {
            subscribers.Clear();
            foreach (var iter in request)
            {
                subscribers.TryAdd(iter.Key, new ConcurrentDictionary<string, bool>());
                result.TryAdd(iter.Key, iter.Value.Count);
                foreach (var iterSubscriber in iter.Value)
                {
                    subscribers[iter.Key].TryAdd(iterSubscriber, true);
                }
            }
        }
        _redisManager.PublishAsync($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:{ConstantRedisChannels.SubscribeInitialize}", JsonConvert.SerializeObject(result), StackExchange.Redis.CommandFlags.FireAndForget);
    }

    public void SendSubscribers()
    {
        _redisManager.PublishAsync($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:{ConstantRedisChannels.SubscribeInitialize}", JsonConvert.SerializeObject(GetSubscribers()), StackExchange.Redis.CommandFlags.FireAndForget);
    }

    public void Clear()
    {
        lock (_lockSubscriber)
        {
            subscribers.Clear();
        }
    }

    #region DISPOSABLE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~SubscribeManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
