﻿using MultiCryptoFin.Models.Subscribe;

namespace MultiCryptoFin.Engine.Algo.Services.Subscribe.Interfaces;
public interface ISubscribeManager : IDisposable
{
    void Add(SubscriberEngineModel request);
    void SendSubscribers();
    void Set(Dictionary<string, List<string>> request);
    void Clear();
}
