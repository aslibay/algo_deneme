﻿using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.Engine.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.AlgoHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Algo.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Subscribe.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
using MultiCryptoFin.Models.Price;
using MultiCryptoFin.Models.Price.Exchange;
using MultiCryptoFin.Models.Subscribe;
using MultiCryptoFin.Shared.Entities.SignalR;
using MultiCryptoFin.Models.Algo;

namespace MultiCryptoFin.Engine.Algo.Services.Engine;
public class EngineManager : IEngineManager
{
    private readonly IAlgoHubManager _algoHubManager;
    
    private readonly ILoggerManager _loggerManager;
    private readonly IOrderManager _orderManager;
    private readonly IPriceManager _priceManager;
    private readonly IAlgoManager _algoManager;
    private readonly ITransactionManager _transactionManager;
    private readonly ISubscribeManager _subscribeManager;

    private readonly IQueueManager<StackExchange.Redis.ChannelMessage> OnMessageRedisOrderRequest;
    private readonly IQueueManager<StackExchange.Redis.ChannelMessage> OnMessageRedisOrderResponse;
    private readonly IQueueManager<StackExchange.Redis.ChannelMessage> OnMessageRedisPrice;

    private readonly IQueueManager<SignalRMessageModel<SubscriberSignalRModel>> OnMessageSignalRSubscribeUnSubscribe;
    private readonly IQueueManager<SignalRMessageModel<string>> OnMessageSignalRClientConnected;

    private readonly IStackExchangeRedisManager _redisReaderManager;
    private readonly IStackExchangeRedisManager _redisSubscriberManager;
    private readonly IStackExchangeRedisManager _redisPublisherManager;
    private readonly IRedisWriterQueueManager _redisWriterQueueManager;

    /// <summary>
    /// EngineManager
    /// </summary>
    /// <param name="LoggerQueueManager"></param>
    /// <param name="mainHubManager"></param>
    /// <param name="RedisWriterManager"></param>
    /// <param name="redisWriterQueueManager"></param>
    public EngineManager(ILoggerManager loggerManager,
        IAlgoHubManager algoHubManager,
        IRedisWriterQueueManager redisWriterQueueManager,
        IOrderManager orderManager,
        IPriceManager priceManager,
        IAlgoManager algoManager,
        ISubscribeManager subscribeManager,        
        ITransactionManager transactionManager)
    {
        _loggerManager = loggerManager;
        _orderManager = orderManager;
        _priceManager = priceManager;
        _algoManager = algoManager;
        _subscribeManager = subscribeManager;
        _transactionManager = transactionManager;

        #region SIGNALR CLIENTS
        _algoHubManager = algoHubManager;
        _algoHubManager.OnConnected += AlgoHubManager_OnConnected;
        _algoHubManager.OnDisconnected += AlgoHubManager_OnDisconnected;
        #endregion

        _redisWriterQueueManager = redisWriterQueueManager;

        #region Redis Client
        _redisReaderManager = new StackExchangeRedisManager(FinTech.StackExchangeRedisService.Commons.Definitions.Address, FinTech.StackExchangeRedisService.Commons.Definitions.Port, FinTech.StackExchangeRedisService.Commons.Definitions.Password, $"{Shared.Commons.Definitions.EngineAppSetting.RedisSettings.ApplicationName}.Get", ConnectWithConstructor: false);
        _redisReaderManager.Connect();

        _redisSubscriberManager = new StackExchangeRedisManager(FinTech.StackExchangeRedisService.Commons.Definitions.Address, FinTech.StackExchangeRedisService.Commons.Definitions.Port, FinTech.StackExchangeRedisService.Commons.Definitions.Password, $"{Shared.Commons.Definitions.EngineAppSetting.RedisSettings.ApplicationName}.Subscriber", ConnectWithConstructor: false);
        _redisSubscriberManager.Connect();

        _redisPublisherManager = new StackExchangeRedisManager(FinTech.StackExchangeRedisService.Commons.Definitions.Address, FinTech.StackExchangeRedisService.Commons.Definitions.Port, FinTech.StackExchangeRedisService.Commons.Definitions.Password, $"{Shared.Commons.Definitions.EngineAppSetting.RedisSettings.ApplicationName}.Publisher", ConnectWithConstructor: false);
        _redisPublisherManager.Connect();
        #endregion

        #region Queue Manager
        OnMessageRedisOrderRequest = new QueueManager<StackExchange.Redis.ChannelMessage>("OnMessageRedisOrderRequest");
        OnMessageRedisOrderRequest.OnEventTriggered += OnMessageRedisOrderRequest_OnEventTriggered;

        OnMessageRedisOrderResponse = new QueueManager<StackExchange.Redis.ChannelMessage>("OnMessageRedisOrderResponse");
        OnMessageRedisOrderResponse.OnEventTriggered += OnMessageRedisOrderResponse_OnEventTriggered;

        OnMessageRedisPrice = new QueueManager<StackExchange.Redis.ChannelMessage>("OnMessageRedisPrice");
        OnMessageRedisPrice.OnEventTriggered += OnMessageRedisPrice_OnEventTriggered;

        OnMessageSignalRSubscribeUnSubscribe = new QueueManager<SignalRMessageModel<SubscriberSignalRModel>>("OnMessageSignalRSubscribeUnSubscribe");
        OnMessageSignalRSubscribeUnSubscribe.OnEventTriggered += OnMessageSignalRSubscribeUnSubscribe_OnEventTriggered;

        OnMessageSignalRClientConnected = new QueueManager<SignalRMessageModel<string>>("OnMessageSignalRClientConnected");
        OnMessageSignalRClientConnected.OnEventTriggered += OnMessageSignalRClientConnected_OnEventTriggered;
        #endregion

        RedisSubscribeChannels();
    }

    #region SIGNALR EVENTS
    #region AlgoHubManager
    private void AlgoHubManager_OnConnected(object? sender, FinTech.SignalRClientService.Commons.MyEventArgs<string> e)
    {
        _loggerManager.AddInfoFileName(ConstantLoggerFileName.Engine, $"AlgoHubManager|Connected|{e.Item}");
        SignalR_AlgoHubOnChannels();
    }
    private void AlgoHubManager_OnDisconnected(object? sender, FinTech.SignalRClientService.Commons.MyEventArgs<string> e)
    {
        _subscribeManager.Clear();
        _loggerManager.AddInfoFileName(ConstantLoggerFileName.Engine, $"AlgoHubManager|Disconnected|{e.Item}");
    }
    #endregion
    #endregion




    #region QueueManager Events

    #region OnMessageRedisPrice
    private List<object> OnMessageRedisPrice_ReceivedData;
    private PriceDto OnMessageRedisPrice_Item = new PriceDto();
    private void OnMessageRedisPrice_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<StackExchange.Redis.ChannelMessage> e)
    {
        try
        {
            OnMessageRedisPrice_ReceivedData = MessagePackSerializer.Deserialize<List<object>>(e.Item.Message);
            _loggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.RedisPrice, JsonConvert.SerializeObject(new { Channel = e.Item.Channel.ToString(), Message = OnMessageRedisPrice_ReceivedData }));
            OnMessageRedisPrice_Item.Set(e.Item.Channel.ToString().Split(":")[3], MessagePackSerializer.Deserialize<PriceExchangeDto>(e.Item.Message));
            _priceManager.Add(OnMessageRedisPrice_Item);
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            OnMessageRedisPrice_Item.SetEmpty();
            e.Dispose();
        }
    }
    #endregion

    #region OnMessageRedisOrderRequest
    private List<object> OnMessageRedisOrderRequest_ReceivedData;
    private void OnMessageRedisOrderRequest_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<StackExchange.Redis.ChannelMessage> e)
    {
        try
        {
            OnMessageRedisOrderRequest_ReceivedData = MessagePackSerializer.Deserialize<List<object>>(e.Item.Message);
            _loggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.RedisRequest, JsonConvert.SerializeObject(new { Channel = e.Item.Channel.ToString(), Message = OnMessageRedisOrderRequest_ReceivedData }));
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
        }
    }
    #endregion

    #region OnMessageRedisOrderResponse
    private string OnMessageRedisOrderResponse_Exchange;
    private List<object> OnMessageRedisOrderResponse_ReceivedData;
    private OrderExchangeResponseReceiveDto OnMessageRedisOrderResponse_Receive = new OrderExchangeResponseReceiveDto();
    private void OnMessageRedisOrderResponse_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<StackExchange.Redis.ChannelMessage> e)
    {
        try
        {
            OnMessageRedisOrderResponse_ReceivedData = MessagePackSerializer.Deserialize<List<object>>(e.Item.Message);
            _loggerManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.RedisResponse, JsonConvert.SerializeObject(new { Channel = e.Item.Channel.ToString(), Message = OnMessageRedisOrderResponse_ReceivedData }));

            OnMessageRedisOrderResponse_Exchange = e.Item.Channel.ToString().Split(":")[2];
            OnMessageRedisOrderResponse_Receive.Set(OnMessageRedisOrderResponse_ReceivedData, OnMessageRedisOrderResponse_Exchange);
            _orderManager.AddResponse(OnMessageRedisOrderResponse_Receive);
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
            OnMessageRedisOrderResponse_Receive.SetEmpty();
        }
    }
    #endregion

    #region OnMessageSignalRSubscribeUnSubscribe
    private SubscriberEngineModel OnMessageSignalRSubscribeUnSubscribe_Receive = new SubscriberEngineModel();
    private void OnMessageSignalRSubscribeUnSubscribe_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<SignalRMessageModel<SubscriberSignalRModel>> e)
    {
        try
        {
            Console.WriteLine("sena => "+JsonConvert.SerializeObject(e));
            OnMessageSignalRSubscribeUnSubscribe_Receive.Set(e.Item.Message.Symbol, (e.Item.Message.IsSubscriber ? e.Item.ConnectionId : null), (e.Item.Message.IsSubscriber == false ? e.Item.ConnectionId : null));
            _subscribeManager.Add(OnMessageSignalRSubscribeUnSubscribe_Receive);
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            OnMessageSignalRSubscribeUnSubscribe_Receive.SetEmpty();
            e.Item.Dispose();
        }

    }
    #endregion

    #region OnMessageSignalRSubscribeUnSubscribe
    private void OnMessageSignalRClientConnected_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<SignalRMessageModel<string>> e)
    {
        try
        {
            if (e.Item.Message == ConstantCommon.MainHub)
            {                
            }
            else if (e.Item.Message == ConstantCommon.OpenOrderHub)
            {
            }
            else if (e.Item.Message == ConstantCommon.TransactionHub)
            {
                // _transactionHubManager.SendByQueue(ConstantHubMethods.SendGroupInitialize, _transactionManager.GetTransactions());
            }
            else if (e.Item.Message == ConstantCommon.PriceHub)
            {

            }
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Item.Dispose();
        }

    }
    #endregion

    #endregion

    #region PRIVATE METHODS
    private void SignalR_AlgoHubOnChannels()
    {
        _algoHubManager.On<AlgoSignalRModel>(ConstantSignalRChannels.AlgoCreate, (Message) =>
        {
            _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate,JsonConvert.SerializeObject(Message));
            //_loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, JsonConvert.SerializeObject(Message));

            _algoManager.CalculateAlgo(Message,_redisSubscriberManager, _orderManager);
            //redise de kaydetmem gerekiyo 
            Message.Dispose();
        });
        _algoHubManager.SendByQueue("GetAlgo", "");
            _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "senaaaa");
        _algoHubManager.On<bool>(ConstantSignalRChannels.AlgoStart, (Message) =>
        {
            _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoStart,"AlgoStart IsOn => True");
            _algoManager.StartAlgo();
        });
        _algoHubManager.On<bool>(ConstantSignalRChannels.AlgoStop, (Message) =>
        {
            _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoStop,"AlgoStop IsOn => False");
            _algoManager.StopAlgo();
        });
        _algoHubManager.On<AlgoSignalRModel>(ConstantSignalRChannels.AlgoReplace, (Message) =>
        {
            _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoReplace,"AlgoReplace  => "+Message);
            _algoManager.AlgoReplace(Message);
        });


    }
        private void RedisSubscribeChannels()
    {
        _redisSubscriberManager.SubscribeQueue($"{ConstantRedisChannels.Order}:{ConstantRedisChannels.Request}:*:*", (cmq) =>
        {
            OnMessageRedisOrderRequest.Add(cmq);
            GC.SuppressFinalize(cmq);
        });
        _redisSubscriberManager.SubscribeQueue($"{ConstantRedisChannels.Order}:{ConstantRedisChannels.Response}:*", (cmq) =>
        {
            OnMessageRedisOrderResponse.Add(cmq);
            GC.SuppressFinalize(cmq);
        });
        _redisSubscriberManager.SubscribeQueue($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:*:*", (cmq) =>
        {
            OnMessageRedisPrice.Add(cmq);
            GC.SuppressFinalize(cmq);
        });
        _redisSubscriberManager.SubscribeQueue($"{ConstantRedisChannels.System}:{ConstantRedisChannels.AdapterIsOnline}", (cmq) =>
        {
            _subscribeManager.SendSubscribers();
            GC.SuppressFinalize(cmq);
        });
    }
    #endregion

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~EngineManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
