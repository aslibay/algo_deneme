
using MultiCryptoFin.Engine.Algo.Services.Algo.Interfaces;
using MultiCryptoFin.Models.Orders.UI;
using MultiCryptoFin.Models.Algo;
using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
using MultiCryptoFin.Shared.Extensions;
using MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
using MultiCryptoFin.Models.Price;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.AlgoHub.Interfaces;

namespace MultiCryptoFin.Engine.Algo.Services.Algo;
public class AlgoManager : IAlgoManager
{
    private IPriceManager _priceManager;
    private readonly IAlgoHubManager _algoHubManager;
    private AlgoPriceCalculation _priceCalculation;
    private AlgoSignalRModel _message;
    private AlgoPriceSender _algoPriceSender;
    private readonly IQueueManager<StackExchange.Redis.ChannelMessage> OnMessageRedisOrderRequest;
    private readonly IQueueManager<AlgoSignalRModel> OnMessageSignalRModel;
    private object lock_price = new object();
    private object lock_order = new object();
    private readonly IQueueManager<StackExchange.Redis.ChannelMessage> OnMessageFirstExchangeData;
    private readonly IQueueManager<StackExchange.Redis.ChannelMessage> OnMessageSecondExchangeData;
    private readonly IQueueManager<bool> OnMessageCalculation;
    private IOrderManager _orderManager;
    private IStackExchangeRedisManager _redisSubscribeManager;

    private readonly IStackExchangeRedisManager _redisManager;
    private AlgoSignalRModel _request;
    private ILoggerManager _loggerManager;

    public AlgoManager(IAlgoHubManager algoHubManager, ILoggerManager loggerManager, IStackExchangeRedisManager redisManager, IPriceManager priceManager)
    {
        _priceManager = priceManager;
        _loggerManager = loggerManager;
        _redisManager = redisManager;
        _algoHubManager = algoHubManager;

        OnMessageRedisOrderRequest = new QueueManager<StackExchange.Redis.ChannelMessage>("OnMessageRedisOrderRequest");
        OnMessageRedisOrderRequest.OnEventTriggered += OnMessageRedisOrderRequest_OnEventTriggered;

        OnMessageSignalRModel = new QueueManager<AlgoSignalRModel>();
        OnMessageSignalRModel.OnEventTriggered += OnMessageSignalRModel_OnEventTriggered;

        OnMessageFirstExchangeData = new QueueManager<StackExchange.Redis.ChannelMessage>();
        OnMessageFirstExchangeData.OnEventTriggered += OnMessageFirstExchangeData_OnEventTriggered;

        OnMessageSecondExchangeData = new QueueManager<StackExchange.Redis.ChannelMessage>();
        OnMessageSecondExchangeData.OnEventTriggered += OnMessageSecondExchangeData_OnEventTriggered;

        OnMessageCalculation = new QueueManager<bool>();
        OnMessageCalculation.OnEventTriggered += OnMessageDiffPrice_OnEventTriggered;

    }

    private List<object> _firstExchangeData;
    private List<object> _secondExchangeData;

    public void CalculateAlgo(AlgoSignalRModel message, IStackExchangeRedisManager redisSubscribeManager, IOrderManager orderManager)
    {
        _orderManager = orderManager;
        _redisSubscribeManager = redisSubscribeManager;
        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "Calculate Algoya Girdi" + JsonConvert.SerializeObject(message));

        AddSignalRModel(message);

    }
    public void AlgoReplace(AlgoSignalRModel Request)
    {
        _message = Request;
        _priceCalculation.Set(
            Id : _message.Id,
            FirstExcApiId :_message.FirstExcApiId,
            SecondExcApiId:_message.SecondExcApiId,
            X_Open: _message.X_Open,
            Y_Open: _message.Y_Open,
            Qty: _message.Qty,
            Adj_A: _message.Adj_A,
            Adj_B: _message.Adj_B,
            Adj_C: _message.Adj_C,
            Adj_D: _message.Adj_D,
            PosLimit: _message.PosLimit,
            IsOn: _message.IsOn,
            FirstExchangeName: _message.FirstExchange,
            SecondExchangeName: _message.SecondExchange

        );
        _loggerManager.AddInfoFileName("AlgoReplace",JsonConvert.SerializeObject(_priceCalculation));

        // AddSignalRModel(Request);
        // _message = Request;

    }
    public void StartAlgo()
    {

        lock (lock_price)
        {
            _message.IsOn = true;
            _priceCalculation.IsOn = true;
            // AddSignalRModel(_message);
        }


    }
    public void StopAlgo()
    {
        lock (lock_price)
        {
            _message.IsOn = false;
            _priceCalculation.IsOn = false;
            // AddSignalRModel(_message);
        }
    }

    public void Dispose()
    {

    }
    public void AddSignalRModel(AlgoSignalRModel request)
    {
        OnMessageSignalRModel.Add(request);
    }

    private List<object> OnMessageRedisOrderRequest_ReceivedData;
    private void OnMessageRedisOrderRequest_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<StackExchange.Redis.ChannelMessage> e)
    {
        try
        {
            OnMessageRedisOrderRequest_ReceivedData = MessagePackSerializer.Deserialize<List<object>>(e.Item.Message);
            _loggerManager.AddInfoFileName(ConstantLoggerFileName.RedisRequest, JsonConvert.SerializeObject(new { Channel = e.Item.Channel.ToString(), Message = OnMessageRedisOrderRequest_ReceivedData }));
        }
        catch (Exception exc)
        {
            _loggerManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
        }
        finally
        {
            e.Dispose();
        }
    }

    private void OnMessageSignalRModel_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<AlgoSignalRModel> e)
    {
        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "OnMessageSignalRModel_OnEventTriggered Girdi" + JsonConvert.SerializeObject(e.Item));

        Console.WriteLine(JsonConvert.SerializeObject(e.Item));
        _message = e.Item;
        _priceCalculation.Set(
            _message.FirstExcApiId,
            _message.SecondExcApiId,
            X_Open: _message.X_Open,
            Y_Open: _message.Y_Open,
            Qty: _message.Qty,
            Adj_A: _message.Adj_A,
            Adj_B: _message.Adj_B,
            Adj_C: _message.Adj_C,
            Adj_D: _message.Adj_D,
            PosLimit: _message.PosLimit,
            IsOn: _message.IsOn,
            FirstExchangeName: _message.FirstExchange,
            SecondExchangeName: _message.SecondExchange

        );
        try
        {
            _redisManager.PublishAsync($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:{ConstantRedisChannels.Subscribe}", _message.Symbol, StackExchange.Redis.CommandFlags.FireAndForget);
            _redisSubscribeManager.SubscribeQueue($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:{_message.Symbol}:{_message.FirstExchange}", (cmq) =>
            {
                // _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "Redis Price First => "+JsonConvert.SerializeObject(cmq));

                OnMessageFirstExchangeData.Add(cmq);
                GC.SuppressFinalize(cmq);
            });
            _loggerManager.AddInfoFileName("second_exchange", "second exchange => " + JsonConvert.SerializeObject(_message.SecondExchange));

            // Console.WriteLine(JsonConvert.SerializeObject(_message.SecondExchange));
            _redisSubscribeManager.SubscribeQueue($"{ConstantRedisChannels.Symbol}:{ConstantRedisChannels.TOB}:{_message.Symbol}:{_message.SecondExchange}", (cmq) =>
            {
                // _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "Redis Price Second => "+JsonConvert.SerializeObject(cmq));

                OnMessageSecondExchangeData.Add(cmq);
                GC.SuppressFinalize(cmq);
            });
        }
        catch (Exception exc)
        {
            _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "Exception'a düştü => " + JsonConvert.SerializeObject(exc));

        }

    }

    private PriceDto _firstExchangePrice;
    private void OnMessageFirstExchangeData_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<StackExchange.Redis.ChannelMessage> e)
    {
        // Console.WriteLine(JsonConvert.SerializeObject(e.Item.Message));
        _firstExchangeData = MessagePackSerializer.Deserialize<List<object>>(e.Item.Message);
        _loggerManager.AddInfoFileName("AlgoStartPriceFirst", "OnMessageFirstExchangeData_OnEventTriggered   => " + JsonConvert.SerializeObject(_firstExchangeData));
        try
        {
            lock (lock_price)
            {
                _algoPriceSender.SetFirstExcPrice(_message.Id, _firstExchangeData[1].ToString().ToDecimal(), _firstExchangeData[3].ToString().ToDecimal());
                _priceCalculation.FirstBidPrice = _firstExchangeData[1].ToString().ToDecimal();
                _priceCalculation.FirstAskPrice = _firstExchangeData[3].ToString().ToDecimal();
                OnMessageCalculation.Add(true);
            }
        }
        catch (Exception exc)
        {
            _loggerManager.AddInfoFileName("Error", "OnMessageFirstExchangeData_OnEventTriggered   => " + JsonConvert.SerializeObject(exc));

        }


    }

    private void OnMessageSecondExchangeData_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<StackExchange.Redis.ChannelMessage> e)
    {
        // Console.WriteLine(JsonConvert.SerializeObject(e.Item.Message));
        _secondExchangeData = MessagePackSerializer.Deserialize<List<object>>(e.Item.Message);
        _loggerManager.AddInfoFileName("AlgoStartPriceSecond", "OnMessageSecondExchangeData_OnEventTriggered   => " + JsonConvert.SerializeObject(_secondExchangeData));
        try
        {
            lock (lock_price)
            {
                _algoPriceSender.SetSecondExcPrice(_message.Id, _secondExchangeData[1].ToString().ToDecimal(), _secondExchangeData[3].ToString().ToDecimal());

                _priceCalculation.SecondBidPrice = _secondExchangeData[1].ToString().ToDecimal();
                _priceCalculation.SecondAskPrice = _secondExchangeData[3].ToString().ToDecimal();
                OnMessageCalculation.Add(true);
            }
        }
        catch (Exception exc)
        {
            _loggerManager.AddInfoFileName("Error", "OnMessageSecondExchangeData_OnEventTriggered   => " + JsonConvert.SerializeObject(exc));

        }

    }
    NewOrderUIRequestReceiveDto newOrder = new NewOrderUIRequestReceiveDto();
    private void OnMessageDiffPrice_OnEventTriggered(object? sender, FinTech.Definitions.Commons.MyEventArgs<bool> e)
    {
        try
        {
        _loggerManager.AddInfoFileName("AlgoReplace"," pricecalcultion işlem yaptığı yer"+JsonConvert.SerializeObject(_priceCalculation));

            _algoHubManager.SendByQueue(ConstantHubMethods.SendAlgoPrice, _algoPriceSender);
            _priceCalculation.CalculateOrder();
            _loggerManager.AddInfoFileName("PosLimit", "if calculated pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.CalculatedPosLimit));
            _loggerManager.AddInfoFileName("PosLimit", "if pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit));

            if (_message.IsOn == true)
            {
                if (Math.Abs(_priceCalculation.CalculatedPosLimit) < _priceCalculation.PosLimit)
                {
                    if (_priceCalculation.SellCBuyB == true)
                    {

                        // _loggerManager.AddInfoFileName("AlgoStartOrder", "_priceCalculation.SellCBuyB == true   => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit) + " > " + JsonConvert.SerializeObject(Math.Abs(_priceCalculation.CalculatedPosLimit)));
                        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoOrder, "SellCBuyB == true   =>    " + JsonConvert.SerializeObject(_priceCalculation));
                        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoOrder, JsonConvert.SerializeObject(_priceCalculation.SecondExchangeName) + "  Sell price=========> "  + JsonConvert.SerializeObject(_priceCalculation.OrderC));
                        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoOrder, JsonConvert.SerializeObject(_priceCalculation.FirstExchangeName) + "  Buy price=========> " +   JsonConvert.SerializeObject(_priceCalculation.OrderB));
                        newOrder.Set(
                            _message.SecondExcApiId,
                            _message.SecondExchange,
                            _message.Symbol,
                            "S",
                            "L",
                            "GTC",
                            _message.Qty,
                            _priceCalculation.OrderC
                        );
                        _orderManager.AddNewOrder(newOrder);
                        newOrder.Set(
                            _message.FirstExcApiId,
                            _message.FirstExchange,
                            _message.Symbol,
                            "B",
                            "L",
                            "GTC",
                            _message.Qty,
                            _priceCalculation.OrderB
                        );
                        _orderManager.AddNewOrder(newOrder);
                        _priceCalculation.CalculatePosLimit(1);
                    }
                    else if (_priceCalculation.SellABuyD == true)
                    {
                        // _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoOrder, "_priceCalculation.SellABuyD == true   => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit) + " > " + JsonConvert.SerializeObject(Math.Abs(_priceCalculation.CalculatedPosLimit)));
                        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoOrder, "SellABuyD == true   =>    " + JsonConvert.SerializeObject(_priceCalculation));
                        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoOrder,  JsonConvert.SerializeObject(_priceCalculation.FirstExchangeName) + "  Sell price=========> " +  JsonConvert.SerializeObject(_priceCalculation.OrderA));
                        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoOrder, JsonConvert.SerializeObject(_priceCalculation.SecondExchangeName) + "  Buy price=========> " +  JsonConvert.SerializeObject(_priceCalculation.OrderD));

                        newOrder.Set(
                            _message.FirstExcApiId,
                            _message.FirstExchange,
                            _message.Symbol,
                            "S",
                            "L",
                            "GTC",
                            _message.Qty,
                            _priceCalculation.OrderA
                        );
                        _orderManager.AddNewOrder(newOrder);
                        newOrder.Set(
                            _message.SecondExcApiId,
                            _message.SecondExchange,
                            _message.Symbol,
                            "B",
                            "L",
                            "GTC",
                            _message.Qty,
                            _priceCalculation.OrderD
                        );
                        _orderManager.AddNewOrder(newOrder);
                        _priceCalculation.CalculatePosLimit(-1);
                    }
                    else
                    {
                        _loggerManager.AddInfoFileName(ConstantLoggerFileName.AlgoCreate, "Hiçbir yere girmedi ---------------------------");

                    }
                }

                else
                {
                    _loggerManager.AddInfoFileName("PosLimit", "else calculated pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.CalculatedPosLimit));
                    _loggerManager.AddInfoFileName("PosLimit", "else pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit));
                    if (_priceCalculation.CalculatedPosLimit < 0)
                    {
                        _loggerManager.AddInfoFileName("PosLimit", "else İF calculated pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.CalculatedPosLimit));
                        _loggerManager.AddInfoFileName("PosLimit", "else İF pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit));
                        if (_priceCalculation.SellCBuyB == true)
                        {
                            _loggerManager.AddInfoFileName("PosLimit", "İSTEDİĞİM YERE GELDİ   => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit) + " > " + JsonConvert.SerializeObject(Math.Abs(_priceCalculation.CalculatedPosLimit)));

                            _loggerManager.AddInfoFileName("AlgoStartOrder", "_priceCalculation.SellCBuyB == true   => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit) + " > " + JsonConvert.SerializeObject(Math.Abs(_priceCalculation.CalculatedPosLimit)));
                            _loggerManager.AddInfoFileName("AlgoStartOrder", "_priceCalculation******  => " + JsonConvert.SerializeObject(_priceCalculation));

                            newOrder.Set(
                                _message.SecondExcApiId,
                                _message.SecondExchange,
                                _message.Symbol,
                                "S",
                                "L",
                                "GTC",
                                _message.Qty,
                                _priceCalculation.OrderC
                            );
                            _orderManager.AddNewOrder(newOrder);
                            newOrder.Set(
                                _message.FirstExcApiId,
                                _message.FirstExchange,
                                _message.Symbol,
                                "B",
                                "L",
                                "GTC",
                                _message.Qty,
                                _priceCalculation.OrderB
                            );
                            _orderManager.AddNewOrder(newOrder);
                            _priceCalculation.CalculatePosLimit(1);
                        }
                    }

                    else
                    {
                        _loggerManager.AddInfoFileName("PosLimit", "else ELSE  calculated pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.CalculatedPosLimit));
                        _loggerManager.AddInfoFileName("PosLimit", "else ELSE pos limit  => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit));
                        if (_priceCalculation.SellABuyD == true)
                        {
                            _loggerManager.AddInfoFileName("AlgoStartOrder", "_priceCalculation.SellABuyD == true   => " + JsonConvert.SerializeObject(_priceCalculation.PosLimit) + " > " + JsonConvert.SerializeObject(Math.Abs(_priceCalculation.CalculatedPosLimit)));
                            _loggerManager.AddInfoFileName("AlgoStartOrder", "_priceCalculation******  => " + JsonConvert.SerializeObject(_priceCalculation));
                            _loggerManager.AddInfoFileName("AlgoStartOrder", "buyD price=========> " + JsonConvert.SerializeObject(_priceCalculation.OrderD));
                            _loggerManager.AddInfoFileName("AlgoStartOrder", "sellD price=========> " + JsonConvert.SerializeObject(_priceCalculation.OrderA));

                            newOrder.Set(
                                _message.FirstExcApiId,
                                _message.FirstExchange,
                                _message.Symbol,
                                "S",
                                "L",
                                "GTC",
                                _message.Qty,
                                _priceCalculation.OrderA
                            );
                            _orderManager.AddNewOrder(newOrder);
                            newOrder.Set(
                                _message.SecondExcApiId,
                                _message.SecondExchange,
                                _message.Symbol,
                                "B",
                                "L",
                                "GTC",
                                _message.Qty,
                                _priceCalculation.OrderD
                            );
                            _orderManager.AddNewOrder(newOrder);
                            _priceCalculation.CalculatePosLimit(-1);
                        }
                    }
                }
            }
            else
            {
                _loggerManager.AddInfoFileName("AlgoCreate", "IsOn   =>  False");

            }
        }
        catch (Exception exc)
        {
            _loggerManager.AddInfoFileName("Error", "OnMessageDiffPrice_OnEventTriggered   => " + JsonConvert.SerializeObject(exc));

        }


    }
}


// {
//     "Id":"093e3919-de7e-486e-875d-e21d5f43f27d",
//     "FirstAskPrice":42025.00,
//     "FirstBidPrice":42024.90,
//     "SecondAskPrice":42054.50,
//     "SecondBidPrice":42050.00,
//     "X":25.00,
//     "Y":-29.60,
//     "X_Open":0.01,
//     "Y_Open":0.01,
//     "Qty":0.01,
//     "Adj_A":0.5,
//     "Adj_B":-0.5,
//     "Adj_C":0.0,
//     "Adj_D":0.1,
//     "PosLimit":3.0,
//     "IsOn":true,
//     "OrderA":42025.40,
//     "OrderB":42024.50,
//     "OrderC":42050.00,
//     "OrderD":42054.60,
//     "SellCBuyB":false,
//     "SellABuyB":true,
//     "CalculatedPosLimit":0.0
// }