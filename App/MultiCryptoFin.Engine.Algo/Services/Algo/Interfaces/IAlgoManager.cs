using MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
using MultiCryptoFin.Models.Algo;//buraya dönüş struct modeli gelcek ama daha yapmadım
// using MultiCryptoFin.Models.Algo.UI; buraya da gelcek 
using StackExchange.Redis;

namespace MultiCryptoFin.Engine.Algo.Services.Algo.Interfaces;
public interface IAlgoManager : IDisposable
{
    // void SubscribePriceChannel(RedisValue message, IStackExchangeRedisManager redisSubscribeManager);
    void CalculateAlgo(AlgoSignalRModel message, IStackExchangeRedisManager redisSubscribeManager, IOrderManager orderManager);
    void AlgoReplace(AlgoSignalRModel Request);
    void StartAlgo();
    void StopAlgo();
    // void AddResponse(OrderExchangeResponseReceiveDto Request);
}
