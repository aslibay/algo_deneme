﻿using System.Collections.Concurrent;
using FinTech.Definitions.Extensions;
using FTP.AWT.Engine.Services.UIDataUpdaterService.Transaction.Entities;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.TransactionHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Transaction.Interfaces;
using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Engine.Algo.Services.UISender.Transaction;
public class TransactionSenderManager : ITransactionSenderManager
{
    private readonly ILoggerManager LoggerQueueManager;
    private readonly ITransactionHubManager TransactionHubManager;

    private readonly CancellationTokenSource TaskCancelSource;
    private IQueueManager<TransactionsDto> queue;
    private readonly ConcurrentDictionary<string, TransactionRowData> Transactions = new ConcurrentDictionary<string, TransactionRowData>();
    private Task QueueTask { get; set; }
    private int CheckInFromMilisecond { get; set; } = 750;
    public object LoggerFileNameConstants { get; private set; }

    private object LockedQueue = new object();
    public TransactionSenderManager(
      ILoggerManager LoggerQueueManager,
      ITransactionHubManager TransactionHubManager)
    {
        this.LoggerQueueManager = LoggerQueueManager;
        this.TransactionHubManager = TransactionHubManager;

        TaskCancelSource = new CancellationTokenSource();

        queue = new QueueManager<TransactionsDto>("Queue");
        queue.OnEventTriggered += Queue_OnEventTriggered;

        QueueTask = Task.Factory.StartNew(
            async () => { await ExecuteAsync().ConfigureAwait(false); },
            cancellationToken: TaskCancelSource.Token,
            creationOptions: TaskCreationOptions.LongRunning,
            scheduler: TaskScheduler.Default).Unwrap();
    }

    private void Queue_OnEventTriggered(object sender, FinTech.Definitions.Commons.MyEventArgs<TransactionsDto> e)
    {
        Transactions.AddOrUpdate(e.Item.ExecutionId,
            (k) =>
            {
                return new TransactionRowData(DateTime.Now.ToMilisecondsFromEpochPosixTime(), e.Item);
            },
            (k, v) =>
            {
                v.TimeStamp = DateTime.Now.ToMilisecondsFromEpochPosixTime();
                v.Fields = e.Item;
                return v;
            });
    }


    public void Add(TransactionsDto request)
    {
        lock (LockedQueue)
        {
            queue.Add(request);
        }
    }
    private async Task<bool> ExecuteAsync()
    {
        List<TransactionsDto> Itemlist = new List<TransactionsDto>();
        while (!TaskCancelSource.IsCancellationRequested)
        {
            Itemlist.Clear();
            try
            {
                if (Transactions.IsEmpty == false)
                {
                    lock (LockedQueue)
                    {
                        foreach (var iter in Transactions)
                        {
                            Itemlist.Add(iter.Value.Fields);
                        }
                        Transactions.Clear();
                    }
                    TransactionHubManager.SendByQueue(ConstantSignalRChannels.SendGroup, Itemlist);
                    LoggerQueueManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.TransactionUpdater, JsonConvert.SerializeObject(Itemlist));
                }
            }
            catch (Exception exc)
            {
                LoggerQueueManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            }
            await Task.Delay(TimeSpan.FromMilliseconds(CheckInFromMilisecond), TaskCancelSource.Token);
        }
        return await Task.FromResult(true);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~OpenOrderSenderManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    } 
    #endregion
}
