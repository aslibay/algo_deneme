﻿using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Engine.Algo.Services.UISender.Transaction.Interfaces;
public interface ITransactionSenderManager : IDisposable
{
    void Add(TransactionsDto request);
}
