﻿using MultiCryptoFin.Models.Orders.Base;

namespace FTP.AWT.Engine.Services.UIDataUpdaterService.Transaction.Entities;
public struct TransactionRowData
{
    public double TimeStamp { get; set; }
    public TransactionsDto Fields;

    public TransactionRowData(double TimeStamp, TransactionsDto Fields)
    {
        this.TimeStamp = TimeStamp;
        this.Fields = Fields;
    }
}
