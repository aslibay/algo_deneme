﻿using System.Collections.Concurrent;
using FinTech.Definitions.Extensions;
using FTP.AWT.Engine.Services.UIDataUpdaterService.OpenOrderUpdater.Entities;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.OpenOrderHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.OpenOrder.Interfaces;
using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Engine.Algo.Services.UISender.Price;
public class OpenOrderSenderManager : IOpenOrderSenderManager
{
    private readonly ILoggerManager LoggerQueueManager;
    private readonly IOpenOrderHubManager OpenOrderHubManager;

    private readonly CancellationTokenSource TaskCancelSource;
    private IQueueManager<OrderDto> queue;
    private readonly ConcurrentDictionary<Guid, OpenOrderRowData> Columns = new ConcurrentDictionary<Guid, OpenOrderRowData>();
    private Task QueueTask { get; set; }
    private int CheckInFromMilisecond { get; set; } = 250;
    private object LockedQueue = new object();
    public OpenOrderSenderManager(
      ILoggerManager LoggerQueueManager,
      IOpenOrderHubManager OpenOrderHubManager)
    {
        this.LoggerQueueManager = LoggerQueueManager;
        this.OpenOrderHubManager = OpenOrderHubManager;

        TaskCancelSource = new CancellationTokenSource();

        queue = new QueueManager<OrderDto>("Queue");
        queue.OnEventTriggered += Queue_OnEventTriggered;

        QueueTask = Task.Factory.StartNew(
            async () => { await ExecuteAsync().ConfigureAwait(false); },
            cancellationToken: TaskCancelSource.Token,
            creationOptions: TaskCreationOptions.LongRunning,
            scheduler: TaskScheduler.Default).Unwrap();
    }

    private void Queue_OnEventTriggered(object sender, FinTech.Definitions.Commons.MyEventArgs<OrderDto> e)
    {
        //Columns.AddOrUpdate(e.Item.CustomOrderId,
        //    (k) =>
        //    {
        //        return new OpenOrderRowData(DateTime.Now.ToMilisecondsFromEpochPosixTime(), e.Item);
        //    },
        //    (k, v) =>
        //    {
        //        v.TimeStamp = DateTime.Now.ToMilisecondsFromEpochPosixTime();
        //        v.Fields = e.Item;
        //        return v;
        //    });
    }


    public void Add(OrderDto request)
    {
        lock (LockedQueue)
        {
            queue.Add(request);
        }
    }



    private async Task<bool> ExecuteAsync()
    {
        double TimeStamp = DateTime.Now.ToMilisecondsFromEpochPosixTime();
        List<OrderDto> Itemlist = new List<OrderDto>();
        while (!TaskCancelSource.IsCancellationRequested)
        {
            Itemlist.Clear();
            //try
            //{
            //    if (Columns.IsEmpty == false)
            //    {
            //        lock (LockedQueue)
            //        {
            //            foreach (var iter in Columns)
            //            {
            //                Itemlist.Add(iter.Value.Fields);
            //                //SignalRClientOrdersManager.SendByQueue(SignalRHubMethodConstants.SendGroup, iter.Value.Fields.MessagePackSerializeToJsonObject());
            //            }
            //            Columns.Clear();
            //        }
            //        OpenOrderHubManager.SendByQueue(ConstantSignalRChannels.SendGroup, JsonConvert.SerializeObject(Itemlist));
            //        LoggerQueueManager.AddInfoFileNameDebugMode("OpenOrderUpdater", JsonConvert.SerializeObject(Itemlist));
            //    }
            //}
            //catch (Exception exc)
            //{
            //    LoggerQueueManager.AddErrorFileName(LoggerFileNameConstants.Error, exc.ExtractToString());
            //}
            await Task.Delay(TimeSpan.FromMilliseconds(CheckInFromMilisecond), TaskCancelSource.Token);
        }
        return await Task.FromResult(true);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~OpenOrderSenderManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    } 
    #endregion
}
