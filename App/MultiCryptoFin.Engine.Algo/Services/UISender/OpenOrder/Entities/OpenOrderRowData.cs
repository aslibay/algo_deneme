﻿using MultiCryptoFin.Models.Orders.Base;

namespace FTP.AWT.Engine.Services.UIDataUpdaterService.OpenOrderUpdater.Entities;
public struct OpenOrderRowData
{
    public double TimeStamp { get; set; }
    public OrderDto Fields;

    public OpenOrderRowData(double TimeStamp, OrderDto Fields)
    {
        this.TimeStamp = TimeStamp;
        this.Fields = Fields;
    }
}
