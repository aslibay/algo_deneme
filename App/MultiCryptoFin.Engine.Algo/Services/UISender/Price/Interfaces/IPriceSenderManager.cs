﻿using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.UISender.Price.Interfaces;
public interface IPriceSenderManager: IDisposable
{
     void Add(PriceDto request);
}
