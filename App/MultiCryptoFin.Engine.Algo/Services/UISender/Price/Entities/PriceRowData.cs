﻿using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.UISender.Price.Entities;
public struct PriceRowData
{
    public double TimeStamp { get; set; }
    public PriceDto Fields { get; set; }

    public PriceRowData(double TimeStamp, PriceDto Fields)
    {
        this.TimeStamp = TimeStamp;
        this.Fields = Fields;
    }
}
