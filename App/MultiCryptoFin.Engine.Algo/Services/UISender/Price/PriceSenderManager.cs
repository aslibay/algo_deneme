﻿using System.Collections.Concurrent;
using FinTech.Definitions.Extensions;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.PriceHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Price.Entities;
using MultiCryptoFin.Engine.Algo.Services.UISender.Price.Interfaces;
using MultiCryptoFin.Models.Price;

namespace MultiCryptoFin.Engine.Algo.Services.UISender.Price;
public class PriceSenderManager : IPriceSenderManager
{
    private readonly ILoggerManager LoggerQueueManager;
    private readonly IPriceHubManager PriceHubManager;

    private readonly CancellationTokenSource TaskCancelSource;
    private IQueueManager<PriceDto> queue;
    // { "Exchange" : { "Symbol": "Data" } }
    private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, PriceRowData>> Datas = new ConcurrentDictionary<string, ConcurrentDictionary<string, PriceRowData>>();
    private Task QueueTask { get; set; }
    private int CheckInFromMilisecond { get; set; } = 500;
    private object LockedQueue = new object();
    public PriceSenderManager(
      ILoggerManager LoggerQueueManager,
      IPriceHubManager PriceHubManager)
    {
        this.LoggerQueueManager = LoggerQueueManager;
        this.PriceHubManager = PriceHubManager;

        TaskCancelSource = new CancellationTokenSource();

        queue = new QueueManager<PriceDto>("Queue");
        queue.OnEventTriggered += Queue_OnEventTriggered;

        QueueTask = Task.Factory.StartNew(
            async () => { await ExecuteAsync().ConfigureAwait(false); },
            cancellationToken: TaskCancelSource.Token,
            creationOptions: TaskCreationOptions.LongRunning,
            scheduler: TaskScheduler.Default).Unwrap();
    }
    private void Queue_OnEventTriggered(object sender, FinTech.Definitions.Commons.MyEventArgs<PriceDto> e)
    {
        Datas.AddOrUpdate(e.Item.Exchange,
            (k) =>
            {
                return new ConcurrentDictionary<string, PriceRowData>(new List<KeyValuePair<string, PriceRowData>>()
                {
                    new KeyValuePair<string, PriceRowData>(e.Item.Symbol, new PriceRowData(DateTime.Now.ToMilisecondsFromEpochPosixTime(), e.Item))
                });
            },
            (k, v) =>
            {
                v.AddOrUpdate(e.Item.Symbol,
                    (k2) =>
                    {
                        return new PriceRowData(DateTime.Now.ToMilisecondsFromEpochPosixTime(), e.Item);
                    },
                    (k2, v2) =>
                    {
                        v2.TimeStamp = DateTime.Now.ToMilisecondsFromEpochPosixTime();
                        v2.Fields = e.Item;
                        return v2;
                    });
                return v;
            });
    }
    public void Add(PriceDto request)
    {
        lock (LockedQueue)
        {
            queue.Add(request);
        }
    }
    private async Task<bool> ExecuteAsync()
    {
        // { "Symbol" : [ "Data" ] }
        Dictionary<string, List<PriceDto>> ItemList = new Dictionary<string, List<PriceDto>>();
        while (!TaskCancelSource.IsCancellationRequested)
        {
            ItemList.Clear();
            try
            {
                if (Datas.IsEmpty == false)
                {
                    lock (LockedQueue)
                    {
                        foreach (KeyValuePair<string, ConcurrentDictionary<string, PriceRowData>> iterData in Datas)
                        {
                            foreach (KeyValuePair<string, PriceRowData> iterPriceRowData in iterData.Value)
                            {
                                if (ItemList.ContainsKey(iterPriceRowData.Key) == false)
                                {
                                    ItemList.TryAdd(iterPriceRowData.Key, new List<PriceDto>());
                                }
                                ItemList[iterPriceRowData.Key].Add(iterPriceRowData.Value.Fields);
                            }
                        }
                        Datas.Clear();
                    }
                    foreach (var iter in ItemList)
                    {
                        PriceHubManager.SendByQueue(ConstantHubMethods.SendGroup, iter.Key, iter.Value);
                    }
                    LoggerQueueManager.AddInfoFileNameDebugMode(ConstantLoggerFileName.PriceUpdater, JsonConvert.SerializeObject(ItemList));
                }
            }
            catch (Exception exc)
            {
                LoggerQueueManager.AddErrorFileName(ConstantLoggerFileName.Error, exc.ExtractToString());
            }
            await Task.Delay(TimeSpan.FromMilliseconds(CheckInFromMilisecond), TaskCancelSource.Token);
        }
        return await Task.FromResult(true);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~PriceSenderManager()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    } 
    #endregion
}
