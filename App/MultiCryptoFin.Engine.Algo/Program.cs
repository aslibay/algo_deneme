﻿using MultiCryptoFin.Engine.Algo;
using MultiCryptoFin.Engine.Algo.Helpers;
using MultiCryptoFin.Engine.Algo.Services.Background.SignalRHubConnectionChecker;
using MultiCryptoFin.Engine.Algo.Services.Engine;
using MultiCryptoFin.Engine.Algo.Services.Engine.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Order;
using MultiCryptoFin.Engine.Algo.Services.Order.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Price;
using MultiCryptoFin.Engine.Algo.Services.Price.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.RedisWriteQueue;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.MainHub;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.OpenOrderHub;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.OpenOrderHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.PriceHub;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.PriceHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.TransactionHub;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.TransactionHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Subscribe;
using MultiCryptoFin.Engine.Algo.Services.Subscribe.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Transaction;
using MultiCryptoFin.Engine.Algo.Services.Transaction.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Price;
using MultiCryptoFin.Engine.Algo.Services.UISender.Price.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.UISender.Transaction;
using MultiCryptoFin.Engine.Algo.Services.UISender.Transaction.Interfaces;
using MultiCryptoFin.Shared.Commons;
using MultiCryptoFin.Shared.Services;
using System;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.AlgoHub.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.SignalRHub.AlgoHub;
using MultiCryptoFin.Engine.Algo.Services.Algo.Interfaces;
using MultiCryptoFin.Engine.Algo.Services.Algo;

MultiCryptoFin.Shared.Commons.Definitions.EngineId = args[0];
FinTech.LoggerService.Commons.Definitions.InnerFolder = args[0];
FinTech.Definitions.Commons.InfoManager.Write(FinTech.Definitions.Commons.InfoManager.InfoModel.FinTech2, ConsoleColor.Blue);
FinTech.Definitions.Commons.InfoManager.Write(FinTech.Definitions.Commons.InfoManager.InfoModel.Engine2, ConsoleColor.White);

var builder = new ConfigurationBuilder()
    .AddEnvironmentVariables("MultiCryptoFin.Engine_")
    .AddJsonFile("appsettings.json");
IConfiguration configuration = builder.Build();

Definitions.EngineAppSetting = configuration.Get<MultiCryptoFin.Shared.AppSettingsConfiguration.Engine.AppSetting>();
#region ENVIRONMENT VARIABLE SETTINGS
EnvironmentVariableService.SetEnvironmentVariable(configuration.GetChildren());
EnvironmentVariableService.CheckInEnvironment(Definitions.EngineAppSetting, configuration.GetChildren());

#endregion
// Definitions.EngineAppSetting.LogSettings.Root =  $"{Definitions.EngineAppSetting.LogSettings.Root}{Path.DirectorySeparatorChar}{MultiCryptoFin.Shared.Commons.Definitions.EngineId}";
List<string> appSetting = FinTech.Definitions.Commons.Definitions.CheckInPropertyValueIsNullOrEmpty(Definitions.EngineAppSetting, "RedisSettings.Password", "LogSettings.Root");
if (appSetting.Count == 0)
{
    Console.WriteLine("Version: " + Definitions.EngineVersion);

    IHost host = Host.CreateDefaultBuilder(args)
        .ConfigureServices(services =>
        {
            services.FintechAddLoggerSettings(Definitions.EngineAppSetting.LogSettings);
            services.FintechAddRedisSettings(Definitions.EngineAppSetting.RedisSettings);

            services.AddSingleton<IRedisWriterQueueManager, RedisWriterQueueManager>();

            services.AddSingleton<IPriceManager, PriceManager>();
            services.AddSingleton<IOrderManager, OrderManager>();
            services.AddSingleton<ITransactionManager, TransactionManager>();
            services.AddSingleton<ISubscribeManager, SubscribeManager>();

            services.AddSingleton<IMainHubManager, MainHubManager>();
            services.AddSingleton<IAlgoHubManager, AlgoHubManager>();

            services.AddSingleton<IOpenOrderHubManager, OpenOrderHubManager>();
            services.AddSingleton<ITransactionHubManager, TransactionHubManager>();
            services.AddSingleton<IPriceHubManager, PriceHubManager>();

            services.AddSingleton<IEngineManager, EngineManager>();
            services.AddSingleton<IPriceSenderManager, PriceSenderManager>();
            services.AddSingleton<ITransactionSenderManager, TransactionSenderManager>();
            services.AddSingleton<IAlgoManager, AlgoManager>();

            services.AddHostedService<HubConnectionService>();
            services.AddHostedService<Worker>();
        })
        .Build();

    await host.RunAsync();
}
else
{
    Console.WriteLine($"appsettings.json dosyası eksik parametre: {JsonConvert.SerializeObject(appSetting)}");
    FinTech.LoggerService.LoggerManager.Write(FinTech.LoggerService.Commons.EnumTypes.Log.Warning, $"appsettings.json dosyası eksik parametre :{JsonConvert.SerializeObject(appSetting)}");
    return;
}
