﻿using FinTech.LoggerService;
using FinTech.LoggerService.Interfaces;
using FinTech.StackExchangeRedisService;
using FinTech.StackExchangeRedisService.Interfaces;
using MultiCryptoFin.Shared.AppSettingsConfiguration.Commons;

namespace MultiCryptoFin.Engine.Algo.Helpers;
/// <summary>
/// StartupHelper
/// </summary>
public static class StartupHelper
{
    /// <summary>
    /// AddFintechLoggerSettings
    /// </summary>
    /// <param name="services"></param>
    /// <param name="logSettings"></param>
    /// <returns></returns>
    public static IServiceCollection FintechAddLoggerSettings(this IServiceCollection services, LogSettings logSettings)
    {
        if (string.IsNullOrWhiteSpace(logSettings.Root))
        {
            FinTech.LoggerService.Commons.Definitions.LoggerPath = AppContext.BaseDirectory;
        }
        else
        {
            FinTech.LoggerService.Commons.Definitions.LoggerPath = logSettings.Root;
        }
        FinTech.LoggerService.Commons.Definitions.ProjectName = logSettings.ApplicationName;
        FinTech.LoggerService.Commons.Definitions.CompressPastFiles = logSettings.CompressPastFiles;
        FinTech.LoggerService.Commons.Definitions.IsActive = logSettings.IsActive;
        FinTech.LoggerService.Commons.Definitions.Debug = logSettings.Debug;
        FinTech.LoggerService.Commons.Definitions.SeparateQueueForEachFile = logSettings.SeparateQueueForEachFile;
        services.AddSingleton<ILoggerManager, LoggerManager>();
        return services;
    }
    /// <summary>
    /// AddFintechRedisSettings
    /// </summary>
    /// <param name="services"></param>
    /// <param name="redisSettings"></param>
    /// <returns></returns>
    public static IServiceCollection FintechAddRedisSettings(this IServiceCollection services, RedisSettings redisSettings)
    {
        FinTech.StackExchangeRedisService.Commons.Definitions.ClientName = $"{redisSettings.ApplicationName}.Writer";
        FinTech.StackExchangeRedisService.Commons.Definitions.Address = redisSettings.Address;
        FinTech.StackExchangeRedisService.Commons.Definitions.Port = redisSettings.Port;
        FinTech.StackExchangeRedisService.Commons.Definitions.Password = redisSettings.Password;
        FinTech.StackExchangeRedisService.Commons.Definitions.RootName = redisSettings.RootName;
        services.AddSingleton<IStackExchangeRedisManager, StackExchangeRedisManager>();
        return services;
    }
}
