﻿using MultiCryptoFin.Engine.Algo.Services.Engine.Interfaces;

namespace MultiCryptoFin.Engine.Algo;
public class Worker : BackgroundService
{
    private readonly string ServiceName = "Worker";
    private readonly ILoggerManager LoggerManager;
    private readonly IEngineManager EngineManager;
    private readonly IStackExchangeRedisManager RedisWriterManager;

    public Worker(ILoggerManager LoggerManager,
        IEngineManager EngineManager,
        IStackExchangeRedisManager RedisWriterManager)
    {
        this.RedisWriterManager = RedisWriterManager;
        this.RedisWriterManager.Connect();

        this.LoggerManager = LoggerManager;
        this.EngineManager = EngineManager;
    }
    public override Task StartAsync(CancellationToken cancellationToken)
    {
        LoggerManager.AddInfoFileName(ServiceName, "Servis çalışmaya başladı.");
        return base.StartAsync(cancellationToken);
    }
    public override Task StopAsync(CancellationToken cancellationToken)
    {
        LoggerManager.AddInfoFileName(ServiceName, $"Servis çalışmayı durdurdu!");
        return base.StopAsync(cancellationToken);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(TimeSpan.FromHours(1), stoppingToken);
            LoggerManager.AddInfoFileName(ServiceName, $"Servis çalışmaya devam ediyor.");
        }
    }
}
