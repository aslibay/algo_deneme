﻿using MultiCryptoFin.Data.Concrete.App;
using MultiCryptoFin.Data.Concrete.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MultiCryptoFin.Data.Context;

public class AppDbContext : IdentityDbContext<CustomIdentityUser, CustomIdentityRole, Guid, CustomIdentityUserClaim, CustomIdentityUserRole, CustomIdentityUserLogin, CustomIdentityRoleClaim, CustomIdentityUserToken>
{
    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);        

        builder.ConfigureTableIdentityAppRoleClaim();
        builder.ConfigureTableIdentityAppRole();
        builder.ConfigureTableIdentityAppUserClaim();
        builder.ConfigureTableIdentityAppUserLogin();
        builder.ConfigureTableIdentityAppUser();
        builder.ConfigureTableIdentityAppUserRole();
        builder.ConfigureTableIdentityAppUserToken();

        builder.ConfigureTableConditionType();
        builder.ConfigureTableCondition();
        builder.ConfigureTableExecuteSp();
        builder.ConfigureTableExecuteSpAuthRole();
        builder.ConfigureTableExecuteSpAuthUser();
        builder.ConfigureTableExecuteSpAuthUserException();
        builder.ConfigureTableExecuteSpLog();
        builder.ConfigureTablePerson();
        builder.ConfigureTablePersonContact();
        builder.ConfigureTableUserForgotPassword();
        builder.ConfigureTableUserInvite();
        builder.ConfigureTableUserLoginType();
        builder.ConfigureTableUserRoleAccountPolicy();
        builder.ConfigureTableUserRoleMenu();
        builder.ConfigureTableBranch();
        builder.ConfigureTableCompany();        
    }

    public DbSet<CustomIdentityUser> ApplicationUser { get; set; }
    
    public DbSet<ExecuteSp> ExecuteSp { get; set; }
    public DbSet<ExecuteSpAuthRole> ExecuteSpAuthRole { get; set; }
    public DbSet<ExecuteSpAuthUser> ExecuteSpAuthUser { get; set; }
    public DbSet<ExecuteSpAuthUserException> ExecuteSpAuthUserException { get; set; }
    public DbSet<ExecuteSpLog> ExecuteSpLog { get; set; }

    public DbSet<ConditionType> ConditionType { get; set; }
    public DbSet<Condition> Condition { get; set; }

    public DbSet<Person> Person { get; set; }
    public DbSet<PersonContact> PersonContact { get; set; }
    public DbSet<UserForgotPassword> UserForgotPassword { get; set; }
    public DbSet<UserInvite> UserInvite { get; set; }
    public DbSet<UserLoginType> UserLoginType { get; set; }
    public DbSet<UserRoleAccountPolicy> UserRoleAccountPolicy { get; set; }
    public DbSet<UserRoleMenu> UserRoleMenu { get; set; }        
    public DbSet<Company> Company { get; set; }        
    public DbSet<Branch> Branch { get; set; }        
   

}
