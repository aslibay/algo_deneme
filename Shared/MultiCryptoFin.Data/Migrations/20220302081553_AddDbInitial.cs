﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MultiCryptoFin.Data.Migrations
{
    public partial class AddDbInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "App_Company",
                columns: table => new
                {
                    CompanyId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    Name = table.Column<string>(type: "NVarChar(500)", maxLength: 500, nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_Company", x => x.CompanyId);
                });

            migrationBuilder.CreateTable(
                name: "App_ConditionType",
                columns: table => new
                {
                    ConditionTypeId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    Name = table.Column<string>(type: "NVarChar(500)", maxLength: 500, nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_ConditionType", x => x.ConditionTypeId);
                });

            migrationBuilder.CreateTable(
                name: "App_Person",
                columns: table => new
                {
                    PersonId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    FirstName = table.Column<string>(type: "NVarChar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "NVarChar(50)", maxLength: 50, nullable: false),
                    BirthDate = table.Column<DateTime>(type: "DateTime", nullable: true),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_Person", x => x.PersonId);
                });

            migrationBuilder.CreateTable(
                name: "Fty_ExecuteSp",
                columns: table => new
                {
                    ExecuteSpId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    Description = table.Column<string>(type: "NVarChar(1000)", maxLength: 1000, nullable: false),
                    Action = table.Column<string>(type: "NVarChar(128)", maxLength: 128, nullable: false),
                    Version = table.Column<string>(type: "NVarChar(10)", maxLength: 10, nullable: false),
                    SpName = table.Column<string>(type: "NVarChar(128)", maxLength: 128, nullable: false),
                    Request = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsLog = table.Column<bool>(type: "Bit", nullable: false),
                    IsAuth = table.Column<bool>(type: "Bit", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fty_ExecuteSp", x => x.ExecuteSpId);
                });

            migrationBuilder.CreateTable(
                name: "Fty_ExecuteSpAuthRole",
                columns: table => new
                {
                    ExecuteSpAuthRoleId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    AppRoleId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    Description = table.Column<string>(type: "NVarChar(500)", maxLength: 500, nullable: false),
                    ExecuteSpIds = table.Column<string>(type: "NVarChar", nullable: false),
                    IsAdmin = table.Column<bool>(type: "Bit", nullable: false),
                    IsApp = table.Column<bool>(type: "Bit", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fty_ExecuteSpAuthRole", x => x.ExecuteSpAuthRoleId);
                });

            migrationBuilder.CreateTable(
                name: "Fty_ExecuteSpAuthUser",
                columns: table => new
                {
                    ExecuteSpAuthUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fty_ExecuteSpAuthUser", x => x.ExecuteSpAuthUserId);
                });

            migrationBuilder.CreateTable(
                name: "Fty_ExecuteSpAuthUserException",
                columns: table => new
                {
                    ExecuteSpAuthUserExceptionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    AccessExecuteSpIds = table.Column<string>(type: "NVarChar", nullable: false),
                    InAccessExecuteSpIds = table.Column<string>(type: "NVarChar", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fty_ExecuteSpAuthUserException", x => x.ExecuteSpAuthUserExceptionId);
                });

            migrationBuilder.CreateTable(
                name: "Fty_ExecuteSpLog",
                columns: table => new
                {
                    ExecuteSpLogId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    Action = table.Column<string>(type: "NVarChar(128)", maxLength: 128, nullable: false),
                    Version = table.Column<string>(type: "NVarChar(10)", maxLength: 10, nullable: false),
                    Parameters = table.Column<string>(type: "NVarChar", nullable: false),
                    Response = table.Column<string>(type: "NVarChar", nullable: false),
                    RequestSize = table.Column<long>(type: "BigInt", nullable: false),
                    ResponseSize = table.Column<long>(type: "BigInt", nullable: false),
                    ExecutionTimeInMillisecond = table.Column<long>(type: "BigInt", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fty_ExecuteSpLog", x => x.ExecuteSpLogId);
                });

            migrationBuilder.CreateTable(
                name: "Iam_Role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iam_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "App_Branch",
                columns: table => new
                {
                    BranchId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    Name = table.Column<string>(type: "NVarChar(500)", maxLength: 500, nullable: false),
                    CompanyId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_Branch", x => x.BranchId);
                    table.ForeignKey(
                        name: "FK_App_Branch_App_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "App_Company",
                        principalColumn: "CompanyId");
                });

            migrationBuilder.CreateTable(
                name: "App_Condition",
                columns: table => new
                {
                    ConditionId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    ConditionTypeId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    Name = table.Column<string>(type: "NVarChar(500)", maxLength: 500, nullable: false),
                    Order = table.Column<int>(type: "Int", nullable: true),
                    Color = table.Column<string>(type: "NVarChar(50)", maxLength: 50, nullable: true),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_Condition", x => x.ConditionId);
                    table.ForeignKey(
                        name: "FK_App_Condition_App_ConditionType_ConditionTypeId",
                        column: x => x.ConditionTypeId,
                        principalTable: "App_ConditionType",
                        principalColumn: "ConditionTypeId");
                });

            migrationBuilder.CreateTable(
                name: "App_PersonContact",
                columns: table => new
                {
                    PersonContactId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    PersonId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    Address = table.Column<string>(type: "NVarChar(500)", maxLength: 500, nullable: false),
                    Phone = table.Column<string>(type: "NVarChar(15)", maxLength: 15, nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_PersonContact", x => x.PersonContactId);
                    table.ForeignKey(
                        name: "FK_App_PersonContact_App_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "App_Person",
                        principalColumn: "PersonId");
                });

            migrationBuilder.CreateTable(
                name: "Iam_User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PersonId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iam_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Iam_User_App_Person_PersonId",
                        column: x => x.PersonId,
                        principalTable: "App_Person",
                        principalColumn: "PersonId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "App_UserInvite",
                columns: table => new
                {
                    UserInviteId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    InviteTypeId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    Email = table.Column<string>(type: "NVarChar(320)", maxLength: 320, nullable: false),
                    FirstName = table.Column<string>(type: "NVarChar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "NVarChar(50)", maxLength: 50, nullable: false),
                    RoleId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    IsActive = table.Column<bool>(type: "Bit", nullable: false, defaultValueSql: "1"),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_UserInvite", x => x.UserInviteId);
                    table.ForeignKey(
                        name: "FK_App_UserInvite_Iam_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Iam_Role",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Iam_RoleClaim",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iam_RoleClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Iam_RoleClaim_Iam_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Iam_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "App_UserForgotPassword",
                columns: table => new
                {
                    UserForgotPasswordId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    UserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    RequestCode = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    IsActive = table.Column<bool>(type: "Bit", nullable: false, defaultValueSql: "1"),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_UserForgotPassword", x => x.UserForgotPasswordId);
                    table.ForeignKey(
                        name: "FK_App_UserForgotPassword_Iam_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Iam_User",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "App_UserLoginType",
                columns: table => new
                {
                    UserLoginTypeId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    UserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    LoginTypeId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_UserLoginType", x => x.UserLoginTypeId);
                    table.ForeignKey(
                        name: "FK_App_UserLoginType_Iam_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Iam_User",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Iam_UserClaim",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iam_UserClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Iam_UserClaim_Iam_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Iam_User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Iam_UserLogin",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iam_UserLogin", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_Iam_UserLogin_Iam_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Iam_User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Iam_UserRole",
                columns: table => new
                {
                    UserRoleId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iam_UserRole", x => x.UserRoleId);
                    table.UniqueConstraint("AK_Iam_UserRole_UserId_RoleId", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_Iam_UserRole_Iam_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Iam_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Iam_UserRole_Iam_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Iam_User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Iam_UserToken",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iam_UserToken", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_Iam_UserToken_Iam_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Iam_User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "App_UserRoleAccountPolicy",
                columns: table => new
                {
                    UserRoleAccountPolicyId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    UserRoleId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    AccountPolicyTypeId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_UserRoleAccountPolicy", x => x.UserRoleAccountPolicyId);
                    table.ForeignKey(
                        name: "FK_App_UserRoleAccountPolicy_Iam_UserRole_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "Iam_UserRole",
                        principalColumn: "UserRoleId");
                });

            migrationBuilder.CreateTable(
                name: "App_UserRoleMenu",
                columns: table => new
                {
                    UserRoleMenuId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false, defaultValueSql: "NEWSEQUENTIALID()"),
                    UserRoleId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    Menu = table.Column<string>(type: "NVarChar", nullable: false),
                    CreatedUserId = table.Column<Guid>(type: "UniqueIdentifier", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "DateTime", nullable: false, defaultValueSql: "GETUTCDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_UserRoleMenu", x => x.UserRoleMenuId);
                    table.ForeignKey(
                        name: "FK_App_UserRoleMenu_Iam_UserRole_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "Iam_UserRole",
                        principalColumn: "UserRoleId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_App_Branch_CompanyId",
                table: "App_Branch",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_App_Condition_ConditionTypeId",
                table: "App_Condition",
                column: "ConditionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_App_PersonContact_PersonId",
                table: "App_PersonContact",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_App_UserForgotPassword_UserId",
                table: "App_UserForgotPassword",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_App_UserInvite_RoleId",
                table: "App_UserInvite",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_App_UserLoginType_UserId",
                table: "App_UserLoginType",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_App_UserRoleAccountPolicy_UserRoleId",
                table: "App_UserRoleAccountPolicy",
                column: "UserRoleId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_App_UserRoleMenu_UserRoleId",
                table: "App_UserRoleMenu",
                column: "UserRoleId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fty_ExecuteSp_Action_Version",
                table: "Fty_ExecuteSp",
                columns: new[] { "Action", "Version" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fty_ExecuteSpAuthUser_RoleId_UserId",
                table: "Fty_ExecuteSpAuthUser",
                columns: new[] { "RoleId", "UserId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Iam_Role",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Iam_RoleClaim_RoleId",
                table: "Iam_RoleClaim",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Iam_User",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "IX_Iam_User_PersonId",
                table: "Iam_User",
                column: "PersonId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Iam_User",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Iam_UserClaim_UserId",
                table: "Iam_UserClaim",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Iam_UserLogin_UserId",
                table: "Iam_UserLogin",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Iam_UserRole_RoleId",
                table: "Iam_UserRole",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "App_Branch");

            migrationBuilder.DropTable(
                name: "App_Condition");

            migrationBuilder.DropTable(
                name: "App_PersonContact");

            migrationBuilder.DropTable(
                name: "App_UserForgotPassword");

            migrationBuilder.DropTable(
                name: "App_UserInvite");

            migrationBuilder.DropTable(
                name: "App_UserLoginType");

            migrationBuilder.DropTable(
                name: "App_UserRoleAccountPolicy");

            migrationBuilder.DropTable(
                name: "App_UserRoleMenu");

            migrationBuilder.DropTable(
                name: "Fty_ExecuteSp");

            migrationBuilder.DropTable(
                name: "Fty_ExecuteSpAuthRole");

            migrationBuilder.DropTable(
                name: "Fty_ExecuteSpAuthUser");

            migrationBuilder.DropTable(
                name: "Fty_ExecuteSpAuthUserException");

            migrationBuilder.DropTable(
                name: "Fty_ExecuteSpLog");

            migrationBuilder.DropTable(
                name: "Iam_RoleClaim");

            migrationBuilder.DropTable(
                name: "Iam_UserClaim");

            migrationBuilder.DropTable(
                name: "Iam_UserLogin");

            migrationBuilder.DropTable(
                name: "Iam_UserToken");

            migrationBuilder.DropTable(
                name: "App_Company");

            migrationBuilder.DropTable(
                name: "App_ConditionType");

            migrationBuilder.DropTable(
                name: "Iam_UserRole");

            migrationBuilder.DropTable(
                name: "Iam_Role");

            migrationBuilder.DropTable(
                name: "Iam_User");

            migrationBuilder.DropTable(
                name: "App_Person");
        }
    }
}
