﻿namespace MultiCryptoFin.Data.Concrete.Fty;
public class ExecuteSp : BaseProperties, IDisposable
{
    public Guid ExecuteSpId { get; set; }
    public string Description { get; set; }
    public string Action { get; set; }
    public string Version { get; set; }
    public string SpName { get; set; }
    public string Request { get; set; }
    public bool IsLog { get; set; }
    public bool IsAuth { get; set; }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~ExecuteSp()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
