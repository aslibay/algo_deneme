﻿namespace MultiCryptoFin.Data.Concrete.Fty;
public class ExecuteSpLog : BaseProperties, IDisposable
{
    public Guid ExecuteSpLogId { get; set; }
    public string Action { get; set; }
    public string Version { get; set; }
    public string Parameters { get; set; }
    public string Response { get; set; }
    public long RequestSize { get; set; }
    public long ResponseSize { get; set; }
    public long ExecutionTimeInMillisecond { get; set; }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~ExecuteSpLog()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
