﻿namespace MultiCryptoFin.Data.Concrete.App;
public class PersonContact : BaseProperties, IDisposable
{
    public Guid PersonContactId { get; set; }
    public Guid PersonId { get; set; }
    public string Address { get; set; }
    public string Phone { get; set; }


    public virtual Person Person { get; set; }

    #region DISPOSE PATTERN
    private bool disposedValue;
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~PersonContact()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
