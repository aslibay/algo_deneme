﻿namespace MultiCryptoFin.Data.Concrete.Identity;

public class CustomIdentityRoleClaim: IdentityRoleClaim<Guid>
{
    public virtual CustomIdentityRole Role { get; set; }
}
