﻿namespace MultiCryptoFin.Data.Concrete.Identity;
public class CustomIdentityUser : IdentityUser<Guid>
{
    public Guid PersonId { get; set; }
    
    public virtual ICollection<CustomIdentityUserRole> Roles { get; set; }    
    public virtual ICollection<CustomIdentityUserClaim> Claims { get; set; }
    public virtual ICollection<CustomIdentityUserLogin> Logins { get; set; }
    public virtual ICollection<CustomIdentityUserToken> Tokens { get; set; }



    public virtual ICollection<UserLoginType> LoginTypes { get; set; }
    public virtual ICollection<UserForgotPassword> ForgotPasswords { get; set; }    

    public virtual Person Person { get; set; }
}



