﻿namespace MultiCryptoFin.Data.Concrete.Identity;

public class CustomIdentityUserRole: IdentityUserRole<Guid>
{
    [Key]
    public Guid UserRoleId { get; set; }

    public virtual CustomIdentityUser User { get; set; }
    public virtual CustomIdentityRole Role { get; set; }

    public virtual UserRoleAccountPolicy UserRoleAccountPolicy { get; set; }
    public virtual UserRoleMenu UserRoleMenu { get; set; }
}
