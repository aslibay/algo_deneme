﻿namespace MultiCryptoFin.Data.Concrete.Identity;

public class CustomIdentityRole : IdentityRole<Guid>
{
    public CustomIdentityRole() : base()
    {

    }
    public CustomIdentityRole(string roleName) : this()
    {
        Name = roleName;
    }
    public virtual ICollection<CustomIdentityUserRole> UserRoles { get; set; }
    public virtual ICollection<CustomIdentityRoleClaim> Claims { get; set; }
    public virtual ICollection<UserInvite> UserInvites { get; set; }
}
