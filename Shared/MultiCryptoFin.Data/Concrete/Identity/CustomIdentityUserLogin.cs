﻿namespace MultiCryptoFin.Data.Concrete.Identity;

public class CustomIdentityUserLogin: IdentityUserLogin<Guid>
{
    public virtual CustomIdentityUser User { get; set; }
}
