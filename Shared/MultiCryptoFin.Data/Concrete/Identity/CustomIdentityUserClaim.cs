﻿namespace MultiCryptoFin.Data.Concrete.Identity;

public class CustomIdentityUserClaim: IdentityUserClaim<Guid>
{
    public virtual CustomIdentityUser User { get; set; }
}
