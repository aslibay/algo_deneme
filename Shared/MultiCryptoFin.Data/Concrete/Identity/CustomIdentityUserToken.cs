﻿namespace MultiCryptoFin.Data.Concrete.Identity;

public class CustomIdentityUserToken: IdentityUserToken<Guid>
{
    public virtual CustomIdentityUser User { get; set; }
}
