﻿namespace MultiCryptoFin.Data.Base
{
    public class BaseProperties
    {
        //[Required]
        public Guid CreatedUserId { get; set; }
        //[Column(TypeName = "datetime")]
        //[Required]
        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
