﻿namespace MultiCryptoFin.Data.Constants;

public static class ConstantEntity
{
    public const string DbTableLinkPrefix = "Link_";
    public const string DbTableAppPrefix = "App_";
    public const string DbTableFinPrefix = "Fty_";
    public const string DbTableIdentityPrefix = "Iam_";

    public const int MaxLength_Max = -1;
    public const int MaxLength_StoreProcedureName = 128;
    public const int MaxLength_Phone = 15;
    public const int MaxLength_FirstName = 50;
    public const int MaxLength_LastName = 50;
    public const int MaxLength_Address = 500;
    public const int MaxLength_Email = 320;
    public const int MaxLength_Name = 500;
    public const int MaxLength_Description = 1000;

    public const string DefaultValue_NEWSEQUENTIALID = "NEWSEQUENTIALID()";
    public const string DefaultValue_GETUTCDATE = "GETUTCDATE()";
    public const string DefaultValue_GETDATE = "GETDATE()";
    public const string DefaultValue_BIT_0 = "0";
    public const string DefaultValue_BIT_1 = "1";

    public const string ColumnType_NvarCharMax = "nvarchar(max)";
}
