﻿namespace MultiCryptoFin.Data.Configuration;

public static class AppDbContextModuleBuilderExtensions
{
    #region Identity
    public static void ConfigureTableIdentityAppUser(this ModelBuilder builder)
    {
        builder.Entity<CustomIdentityUser>(b =>
        {
            b.ToTable(ConstantEntity.DbTableIdentityPrefix + "User");

            // properties

            // index

            // relation
            b.HasOne(u => u.Person)
                .WithOne()
                .HasForeignKey<CustomIdentityUser>(x =>x.PersonId);
        });
    }
    public static void ConfigureTableIdentityAppRole(this ModelBuilder builder)
    {
        builder.Entity<CustomIdentityRole>(b =>
        {
            b.ToTable(ConstantEntity.DbTableIdentityPrefix + "Role");

            // properties

            // index

            // relation
        });
    }
    public static void ConfigureTableIdentityAppUserRole(this ModelBuilder builder)
    {
        builder.Entity<CustomIdentityUserRole>(b =>
        {
            b.ToTable(ConstantEntity.DbTableIdentityPrefix + "UserRole");

            // properties
            b.Property(x => x.UserRoleId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);

            // key
            b.HasKey(x => x.UserRoleId);

            // index            
            b.HasAlternateKey(x => new { x.UserId, x.RoleId });

            // relation            
            b.HasOne(ur => ur.User)
                .WithMany(u => u.Roles)
                .HasForeignKey(ur => ur.UserId);

            b.HasOne(ur => ur.Role)
                .WithMany(r => r.UserRoles)
                .HasForeignKey(ur => ur.RoleId);

            b.HasOne(ur => ur.UserRoleAccountPolicy)
                .WithOne(urap => urap.UserRole)
                .HasForeignKey<UserRoleAccountPolicy>(urap => urap.UserRoleId)
                .OnDelete(DeleteBehavior.NoAction);

            b.HasOne(ur => ur.UserRoleMenu)
                .WithOne(urm => urm.UserRole)
                .HasForeignKey<UserRoleMenu>(x => x.UserRoleId)
                .OnDelete(DeleteBehavior.NoAction);
        });
    }
    public static void ConfigureTableIdentityAppUserToken(this ModelBuilder builder)
    {
        builder.Entity<CustomIdentityUserToken>(b =>
        {
            b.ToTable(ConstantEntity.DbTableIdentityPrefix + "UserToken");

            // properties

            // index

            // relation
            b.HasOne(ut => ut.User)
                .WithMany(u => u.Tokens)
                .HasForeignKey(ut => ut.UserId);
        });
    }
    public static void ConfigureTableIdentityAppUserLogin(this ModelBuilder builder)
    {
        builder.Entity<CustomIdentityUserLogin>(b =>
        {
            b.ToTable(ConstantEntity.DbTableIdentityPrefix + "UserLogin");

            // index

            // properties

            // relation
            b.HasOne(ul => ul.User)
                .WithMany(u => u.Logins)
                .HasForeignKey(ul => ul.UserId);
        });
    }
    public static void ConfigureTableIdentityAppUserClaim(this ModelBuilder builder)
    {
        builder.Entity<CustomIdentityUserClaim>(b =>
        {
            b.ToTable(ConstantEntity.DbTableIdentityPrefix + "UserClaim");

            // properties

            // index

            // relation
            b.HasOne(uc => uc.User)
                .WithMany(u => u.Claims)
                .HasForeignKey(uc => uc.UserId);
        });
    }
    public static void ConfigureTableIdentityAppRoleClaim(this ModelBuilder builder)
    {

        builder.Entity<CustomIdentityRoleClaim>(b =>
        {
            b.ToTable(ConstantEntity.DbTableIdentityPrefix + "RoleClaim");

            // properties

            // index

            // relation
            b.HasOne(rc => rc.Role)
                .WithMany(r => r.Claims)
                .HasForeignKey(rc => rc.RoleId);
        });
    }
    #endregion
    #region Fty
    public static void ConfigureTableExecuteSp(this ModelBuilder builder)
    {
        builder.Entity<ExecuteSp>(b =>
        {
            b.ToTable(ConstantEntity.DbTableFinPrefix + "ExecuteSp");

            // properties
            b.Property(x => x.ExecuteSpId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.Description)
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_Description);
            b.Property(x => x.Action)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_StoreProcedureName);
            b.Property(x => x.Version)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(10);
            b.Property(x => x.SpName)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_StoreProcedureName);
            b.Property(x => x.Request)
                .HasColumnType(ConstantEntity.ColumnType_NvarCharMax);
            b.Property(x => x.IsLog)
                .IsRequired()
                .HasColumnType(SqlDbType.Bit.ToString());
            b.Property(x => x.IsAuth)
                .IsRequired()
                .HasColumnType(SqlDbType.Bit.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.ExecuteSpId);

            b.HasIndex(x => new { x.Action, x.Version })
                .IsUnique();

            // relation
        });
    }
    public static void ConfigureTableExecuteSpAuthRole(this ModelBuilder builder)
    {
        builder.Entity<ExecuteSpAuthRole>(b =>
        {
            b.ToTable(ConstantEntity.DbTableFinPrefix + "ExecuteSpAuthRole");

            // properties
            b.Property(x => x.ExecuteSpAuthRoleId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.AppRoleId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.Description)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(500);
            b.Property(x => x.ExecuteSpIds)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString());
            b.Property(x => x.IsAdmin)
                .IsRequired()
                .HasColumnType(SqlDbType.Bit.ToString());
            b.Property(x => x.IsApp)
                .IsRequired()
                .HasColumnType(SqlDbType.Bit.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.ExecuteSpAuthRoleId);

            // relation
        });
    }
    public static void ConfigureTableExecuteSpAuthUser(this ModelBuilder builder)
    {
        builder.Entity<ExecuteSpAuthUser>(b =>
        {
            b.ToTable(ConstantEntity.DbTableFinPrefix + "ExecuteSpAuthUser");

            // properties
            b.Property(x => x.RoleId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.UserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.ExecuteSpAuthUserId);

            b.HasIndex(x => new { x.RoleId, x.UserId })
                .IsUnique();

            // relation
        });
    }
    public static void ConfigureTableExecuteSpAuthUserException(this ModelBuilder builder)
    {
        builder.Entity<ExecuteSpAuthUserException>(b =>
        {
            b.ToTable(ConstantEntity.DbTableFinPrefix + "ExecuteSpAuthUserException");

            // properties
            b.Property(x => x.UserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.AccessExecuteSpIds)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString());
            b.Property(x => x.InAccessExecuteSpIds)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.ExecuteSpAuthUserExceptionId);

            // relation
        });
    }
    public static void ConfigureTableExecuteSpLog(this ModelBuilder builder)
    {
        builder.Entity<ExecuteSpLog>(b =>
        {
            b.ToTable(ConstantEntity.DbTableFinPrefix + "ExecuteSpLog");

            // properties
            b.Property(x => x.ExecuteSpLogId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.Action)
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_StoreProcedureName);
            b.Property(x => x.Version)
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(10);
            b.Property(x => x.Parameters)
                .HasColumnType(SqlDbType.NVarChar.ToString());
            b.Property(x => x.Response)
                .HasColumnType(SqlDbType.NVarChar.ToString());
            b.Property(x => x.RequestSize)
                .HasColumnType(SqlDbType.BigInt.ToString());
            b.Property(x => x.ResponseSize)
                .HasColumnType(SqlDbType.BigInt.ToString());
            b.Property(x => x.ExecutionTimeInMillisecond)
                .HasColumnType(SqlDbType.BigInt.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.ExecuteSpLogId);

            // relation
        });
    }
    #endregion
    #region App
    public static void ConfigureTableConditionType(this ModelBuilder builder)
    {
        builder.Entity<ConditionType>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "ConditionType");

            // properties
            b.Property(x => x.ConditionTypeId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.Name)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(500);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.ConditionTypeId);

            // relation
        });
    }
    public static void ConfigureTableCondition(this ModelBuilder builder)
    {
        builder.Entity<Condition>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "Condition");

            // properties
            b.Property(x => x.ConditionId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.ConditionTypeId)
                  .IsRequired()
                  .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.Name)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_Name);
            b.Property(x => x.Order)
                .HasColumnType(SqlDbType.Int.ToString());
            b.Property(x => x.Color)
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(50);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.ConditionId);

            // relation
            b.HasOne(ct => ct.ConditionType)
                .WithMany(c => c.Conditions)
                .HasForeignKey(ct => ct.ConditionTypeId)
                .OnDelete(DeleteBehavior.NoAction);
        });
    }   
    public static void ConfigureTablePerson(this ModelBuilder builder)
    {
        builder.Entity<Person>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "Person");

            // properties
            b.Property(x => x.PersonId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.FirstName)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_FirstName);
            b.Property(x => x.LastName)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_LastName);
            b.Property(x => x.BirthDate)
                .HasColumnType(SqlDbType.DateTime.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.PersonId);

            // relation
        });
    }
    public static void ConfigureTablePersonContact(this ModelBuilder builder)
    {
        builder.Entity<PersonContact>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "PersonContact");

            // properties
            b.Property(x => x.PersonContactId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.PersonId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.Address)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_Address);
            b.Property(x => x.Phone)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_Phone);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.PersonContactId);

            // relation
            b.HasOne(pc => pc.Person)
                .WithMany(p => p.PersonContacts)
                .HasForeignKey(pc => pc.PersonId)
                .OnDelete(DeleteBehavior.NoAction);
        });
    }
    public static void ConfigureTableUserForgotPassword(this ModelBuilder builder)
    {
        builder.Entity<UserForgotPassword>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "UserForgotPassword");

            // properties
            b.Property(x => x.UserForgotPasswordId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.UserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.RequestCode)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.IsActive)
                .IsRequired()
                .HasColumnType(SqlDbType.Bit.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_BIT_1);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.UserForgotPasswordId);

            // relation
            b.HasOne(ufp => ufp.User)
                .WithMany(u => u.ForgotPasswords)
                .HasForeignKey(ufp => ufp.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        });
    }
    public static void ConfigureTableUserInvite(this ModelBuilder builder)
    {
        builder.Entity<UserInvite>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "UserInvite");

            // properties
            b.Property(x => x.UserInviteId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.InviteTypeId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.Email)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_Email);
            b.Property(x => x.FirstName)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_FirstName);
            b.Property(x => x.LastName)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_LastName);
            b.Property(x => x.RoleId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.IsActive)
                .IsRequired()
                .HasColumnType(SqlDbType.Bit.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_BIT_1);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.UserInviteId);

            // relation
            b.HasOne(ui => ui.Role)
                .WithMany(r => r.UserInvites)
                .HasForeignKey(ui => ui.RoleId)
                .OnDelete(DeleteBehavior.NoAction);
        });
    }
    public static void ConfigureTableUserLoginType(this ModelBuilder builder)
    {
        builder.Entity<UserLoginType>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "UserLoginType");

            // properties
            b.Property(x => x.UserLoginTypeId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.UserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.LoginTypeId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.UserLoginTypeId);

            // relation
            b.HasOne(ult => ult.User)
                .WithMany(u => u.LoginTypes)
                .HasForeignKey(ult => ult.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        });
    }
    public static void ConfigureTableUserRoleAccountPolicy(this ModelBuilder builder)
    {
        builder.Entity<UserRoleAccountPolicy>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "UserRoleAccountPolicy");

            // properties
            b.Property(x => x.UserRoleAccountPolicyId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.UserRoleId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.AccountPolicyTypeId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.Value)
                .IsRequired()
                .HasColumnType(ConstantEntity.ColumnType_NvarCharMax);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.UserRoleAccountPolicyId);

            // relation

        });
    }
    public static void ConfigureTableUserRoleMenu(this ModelBuilder builder)
    {
        builder.Entity<UserRoleMenu>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "UserRoleMenu");

            // properties
            b.Property(x => x.UserRoleMenuId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.UserRoleId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.Menu)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString());
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.UserRoleMenuId);

            // relation
        });
    }
    public static void ConfigureTableCompany(this ModelBuilder builder)
    {
        builder.Entity<Company>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "Company");

            // properties
            b.Property(x => x.CompanyId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.Name)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_Name);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.CompanyId);

            // relation
        });
    }
    public static void ConfigureTableBranch(this ModelBuilder builder)
    {
        builder.Entity<Branch>(b =>
        {
            b.ToTable(ConstantEntity.DbTableAppPrefix + "Branch");

            // properties
            b.Property(x => x.BranchId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
            b.Property(x => x.CompanyId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.Name)
                .IsRequired()
                .HasColumnType(SqlDbType.NVarChar.ToString())
                .HasMaxLength(ConstantEntity.MaxLength_Name);
            b.Property(x => x.CreatedUserId)
                .IsRequired()
                .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
            b.Property(x => x.CreatedDate)
                .IsRequired()
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

            // index
            b.HasKey(t => t.BranchId);

            // relation
            b.HasOne(b => b.Company)
                .WithMany(c => c.Branchs)
                .HasForeignKey(b => b.CompanyId)
                .OnDelete(DeleteBehavior.NoAction);
        });
    }

    //public static void ConfigureTableExchange(this ModelBuilder builder)
    //{
    //    builder.Entity<ConditionType>(b =>
    //    {
    //        b.ToTable(ConstantEntity.DbTableAppPrefix + "ConditionType");

    //        // properties
    //        b.Property(x => x.ConditionTypeId)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
    //            .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
    //        b.Property(x => x.Name)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.NVarChar.ToString())
    //            .HasMaxLength(500);
    //        b.Property(x => x.CreatedUserId)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
    //        b.Property(x => x.CreatedDate)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.DateTime.ToString())
    //            .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

    //        // index
    //        b.HasKey(t => t.ConditionTypeId);

    //        // relation
    //    });
    //}
    //public static void ConfigureTableExchangeAccount(this ModelBuilder builder)
    //{
    //    builder.Entity<Condition>(b =>
    //    {
    //        b.ToTable(ConstantEntity.DbTableAppPrefix + "Condition");

    //        // properties
    //        b.Property(x => x.ConditionId)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.UniqueIdentifier.ToString())
    //            .HasDefaultValueSql(ConstantEntity.DefaultValue_NEWSEQUENTIALID);
    //        b.Property(x => x.ConditionTypeId)
    //              .IsRequired()
    //              .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
    //        b.Property(x => x.Name)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.NVarChar.ToString())
    //            .HasMaxLength(ConstantEntity.MaxLength_Name);
    //        b.Property(x => x.Order)
    //            .HasColumnType(SqlDbType.Int.ToString());
    //        b.Property(x => x.Color)
    //            .HasColumnType(SqlDbType.NVarChar.ToString())
    //            .HasMaxLength(50);
    //        b.Property(x => x.CreatedUserId)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.UniqueIdentifier.ToString());
    //        b.Property(x => x.CreatedDate)
    //            .IsRequired()
    //            .HasColumnType(SqlDbType.DateTime.ToString())
    //            .HasDefaultValueSql(ConstantEntity.DefaultValue_GETUTCDATE);

    //        // index
    //        b.HasKey(t => t.ConditionId);

    //        // relation
    //        b.HasOne(ct => ct.ConditionType)
    //            .WithMany(c => c.Conditions)
    //            .HasForeignKey(ct => ct.ConditionTypeId)
    //            .OnDelete(DeleteBehavior.NoAction);
    //    });
    //}
    #endregion
}
