﻿using MultiCryptoFin.Shared.Extensions;

namespace MultiCryptoFin.Models.Algo;

[MessagePackObject]
public struct AlgoSignalRModel : IDisposable
{
    [Key(0)]
    public Guid? Id { get; set; }

    [Key(1)]
    public string? FirstExchange { get; set; }
    [Key(2)]
    public string? SecondExchange { get; set; }
    [Key(3)]
    public string? Symbol { get; set; }

    [Key(4)]
    public Guid? FirstExcApiId { get; set; }

    [Key(5)]
    public Guid? SecondExcApiId { get; set; }
    [Key(6)]
    public decimal? X_Open { get; set; }
    [Key(7)]
    public decimal? Y_Open { get; set; }
    [Key(8)]
    public decimal? Qty { get; set; }
    [Key(9)]
    public decimal? Adj_A { get; set; }
    [Key(10)]
    public decimal? Adj_B { get; set; }
    [Key(11)]
    public decimal? Adj_C { get; set; }
    [Key(12)]
    public decimal? Adj_D { get; set; }
    [Key(13)]
    public int? PosLimit { get; set; }
    [Key(14)]
    public bool IsOn { get; set; }
    [Key(15)]
    public decimal? FirstExcBidPrice { get; set; }
    [Key(16)]
    public decimal? FirstExcAskPrice { get; set; }
    [Key(17)]
    public decimal? SecondExcBidPrice { get; set; }
    [Key(18)]
    public decimal? SecondExcAskPrice { get; set; }

    public AlgoSignalRModel()
    {
        Id = null;
        FirstExcApiId = null;
        SecondExcApiId = null;
        FirstExchange = null;
        SecondExchange = null;
        Symbol = null;
        X_Open = null;
        Y_Open = null;
        Qty = null;
        Adj_A = null;
        Adj_B = null;
        Adj_C = null;
        Adj_D = null;
        PosLimit = null;
        IsOn = true;
        FirstExcBidPrice = null;
        FirstExcAskPrice = null;
        SecondExcBidPrice = null;
        SecondExcAskPrice = null;
    }
    public AlgoSignalRModel(List<object> Request)
    {
        this.Id = new Guid(Request[0].ToString());
        this.FirstExchange = Request[1].ToString();
        this.SecondExchange = Request[2].ToString();
        this.Symbol = Request[3].ToString();
        this.FirstExcApiId = new Guid(Request[4].ToString());
        this.SecondExcApiId = new Guid(Request[5].ToString());
        this.X_Open = Request[6].ToString().ToDecimal();
        this.Y_Open = Request[7].ToString().ToDecimal();
        this.Qty = Request[8].ToString().ToDecimal();
        this.Adj_A = Request[9].ToString().ToDecimal();
        this.Adj_B = Request[10].ToString().ToDecimal();
        this.Adj_C = Request[11].ToString().ToDecimal();
        this.Adj_D = Request[12].ToString().ToDecimal();
        this.PosLimit = int.Parse(Request[13].ToString());
        this.IsOn = (bool)Request[14];
        this.FirstExcBidPrice = null;
        this.FirstExcAskPrice = null;
        this.SecondExcBidPrice = null;
        this.SecondExcAskPrice = null;
    }
    public AlgoSignalRModel(Guid Id, Guid FirstExcApiId, string FirstExcApiKey, string FirstExcSecretKey, Guid SecondExcApiId, string SecondExcApiKey, string SecondExcSecretKey, string FirstExchange, string SecondExchange, string Symbol, decimal X_Open, decimal Y_Open, decimal Qty, decimal Adj_A, decimal Adj_B, decimal Adj_C, decimal Adj_D, int PosLimit, bool IsOn = true)
    {
        this.Id = Id;
        this.FirstExcApiId = FirstExcApiId;
        this.SecondExcApiId = SecondExcApiId;
        this.FirstExchange = FirstExchange;
        this.SecondExchange = SecondExchange;
        this.Symbol = Symbol;
        this.X_Open = X_Open;
        this.Y_Open = Y_Open;
        this.Qty = Qty;
        this.Adj_A = Adj_A;
        this.Adj_B = Adj_B;
        this.Adj_C = Adj_C;
        this.Adj_D = Adj_D;
        this.PosLimit = PosLimit;
        this.IsOn = IsOn;
        this.FirstExcBidPrice = null;
        this.FirstExcAskPrice = null;
        this.SecondExcBidPrice = null;
        this.SecondExcAskPrice = null;
    }
    public void Set(List<object> Request)
    {
        this.FirstExchange = Request[0].ToString();
        this.SecondExchange = Request[1].ToString();
        this.Symbol = Request[2].ToString();
        this.FirstExcApiId = new Guid(Request[3].ToString());
        this.SecondExcApiId = new Guid(Request[4].ToString());
        this.X_Open = Request[5].ToString().ToDecimal();
        this.Y_Open = Request[6].ToString().ToDecimal();
        this.Qty = Request[7].ToString().ToDecimal();
        this.Adj_A = Request[8].ToString().ToDecimal();
        this.Adj_B = Request[9].ToString().ToDecimal();
        this.Adj_C = Request[10].ToString().ToDecimal();
        this.Adj_D = Request[11].ToString().ToDecimal();
        this.PosLimit = int.Parse(Request[12].ToString());
        this.IsOn = (bool)Request[13];

    }

    public void SetFirstExcPrice(decimal? FirstExcBidPrice, decimal? FirstExcAskPrice)
    {
        this.FirstExcBidPrice = FirstExcBidPrice;
        this.FirstExcAskPrice = FirstExcAskPrice;
    }

    public void SetSecondExcPrice(decimal? SecondExcBidPrice, decimal? SecondExcAskPrice)
    {

        this.SecondExcBidPrice = SecondExcBidPrice;
        this.SecondExcAskPrice = SecondExcAskPrice;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
