using MultiCryptoFin.Shared.Extensions;

namespace MultiCryptoFin.Models.Algo;

[MessagePackObject]
public struct AlgoPriceSender : IDisposable
{
    [Key(0)]
    public Guid? Id { get; set; }
    [Key(1)]
    public decimal? FirstExcBidPrice { get; set; }
    [Key(2)]
    public decimal? FirstExcAskPrice { get; set; }
    [Key(3)]
    public decimal? SecondExcBidPrice { get; set; }
    [Key(4)]
    public decimal? SecondExcAskPrice { get; set; }

    public AlgoPriceSender()
    {
        Id = null;
        FirstExcBidPrice = null;
        FirstExcAskPrice = null;
        SecondExcBidPrice = null;
        SecondExcAskPrice = null;
    }

    public AlgoPriceSender(Guid Id)
    {
        this.Id = Id;
        this.FirstExcBidPrice = null;
        this.FirstExcAskPrice = null;
        this.SecondExcBidPrice = null;
        this.SecondExcAskPrice = null;
    }

    public void SetFirstExcPrice(Guid? Id, decimal? FirstExcBidPrice, decimal? FirstExcAskPrice)
    {
        this.Id = Id;
        this.FirstExcBidPrice = FirstExcBidPrice;
        this.FirstExcAskPrice = FirstExcAskPrice;
    }

    public void SetSecondExcPrice(Guid? Id, decimal? SecondExcBidPrice, decimal? SecondExcAskPrice)
    {
        this.Id = Id;
        this.SecondExcBidPrice = SecondExcBidPrice;
        this.SecondExcAskPrice = SecondExcAskPrice;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
