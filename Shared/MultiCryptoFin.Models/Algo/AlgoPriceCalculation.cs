namespace MultiCryptoFin.Models.Algo;
[MessagePackObject]
public struct AlgoPriceCalculation : IDisposable
{
    [Key(0)]
    [JsonIgnore]
    public Guid? Id { get; set; }

    [Key(1)]
    public string? FirstExchangeName { get; set; }
    [Key(2)]
    public string? SecondExchangeName { get; set; }

    [IgnoreMember]
    [JsonIgnore]
    public Guid? FirstExcApiId { get; set; }

    [IgnoreMember]
    [JsonIgnore]
    public Guid? SecondExcApiId { get; set; }

    [Key(3)]
    public decimal? FirstAskPrice { get; set; }
    [Key(4)]
    public decimal? FirstBidPrice { get; set; }
    [Key(5)]
    public decimal? SecondAskPrice { get; set; }
    [Key(6)]
    public decimal? SecondBidPrice { get; set; }
    [Key(7)]
    public decimal? X
    {
        get
        {
            if (FirstBidPrice == null || SecondAskPrice == null)
            {
                return null;
            }
            else
            {
                return FirstBidPrice - SecondAskPrice;
            }
        }
    }
    [Key(8)]
    public decimal? Y
    {
        get
        {
            if (SecondBidPrice == null || FirstAskPrice == null)
            {
                return null;
            }
            else
            {
                return SecondBidPrice - FirstAskPrice;
            }
        }
    }
    [Key(9)]
    public decimal? X_Open { get; set; }
    [Key(10)]
    public decimal? Y_Open { get; set; }
    [Key(11)]
    public decimal? Qty { get; set; }
    [Key(12)]
    public decimal? Adj_A { get; set; }
    [Key(13)]
    public decimal? Adj_B { get; set; }
    [Key(14)]
    public decimal? Adj_C { get; set; }
    [Key(15)]
    public decimal? Adj_D { get; set; }
    [Key(16)]
    public decimal? PosLimit { get; set; }
    [Key(17)]
    public bool? IsOn { get; set; }
    [Key(18)]
    public decimal? OrderA
    {
        get
        {
            if (FirstBidPrice == null || Adj_A == null)
            {
                return null;
            }
            else
            {
                return FirstBidPrice + Adj_A;
            }
        }
    }
    [Key(19)]
    public decimal? OrderB
    {
        get
        {
            if (FirstAskPrice == null || Adj_B == null)
            {
                return null;
            }
            else
            {
                return FirstAskPrice + Adj_B;
            }
        }
    }
    [Key(20)]
    public decimal? OrderC
    {
        get
        {
            if (SecondBidPrice == null || Adj_C == null)
            {
                return null;
            }
            else
            {
                return SecondBidPrice + Adj_C;
            }
        }
    }
    [Key(21)]
    public decimal? OrderD
    {
        get
        {
            if (SecondAskPrice == null || Adj_D == null)
            {
                return null;
            }
            else
            {
                return SecondAskPrice + Adj_D;
            }
        }
    }
    [IgnoreMember]
    [JsonIgnore]
    public bool? SellCBuyB { get; set; }
    [IgnoreMember]
    [JsonIgnore]
    public bool? SellABuyD { get; set; }
    [IgnoreMember]
    [JsonIgnore]
    public decimal CalculatedPosLimit { get; set; }



    public AlgoPriceCalculation()
    {
        Id = null;
        FirstExcApiId = null;
        SecondExcApiId = null;
        FirstAskPrice = null;
        FirstBidPrice = null;
        SecondAskPrice = null;
        SecondBidPrice = null;
        X_Open = null;
        Y_Open = null;
        Qty = null;
        Adj_A = null;
        Adj_B = null;
        Adj_C = null;
        Adj_D = null;
        PosLimit = null;
        IsOn = null;
        SellCBuyB = false;
        SellABuyD = false;
        CalculatedPosLimit = 0;
        FirstExchangeName = null;
        SecondExchangeName = null;
    }

    public AlgoPriceCalculation(Guid Id, Guid FirstExcApiId, Guid SecondExcApiId, decimal FirstAskPrice, decimal FirstBidPrice, decimal SecondAskPrice, decimal SecondBidPrice, decimal X_Open, decimal Y_Open, decimal Qty, decimal Adj_A, decimal Adj_B, decimal Adj_C, decimal Adj_D, decimal PosLimit, bool IsOn, bool SellCBuyB, bool SellABuyD, decimal CalculatedPosLimit, string FirstExchangeName, string SecondExchangeName)
    {
        this.Id = Id;
        this.FirstExcApiId = FirstExcApiId;
        this.SecondExcApiId = SecondExcApiId;
        this.FirstAskPrice = FirstAskPrice;
        this.FirstBidPrice = FirstBidPrice;
        this.SecondAskPrice = SecondAskPrice;
        this.SecondBidPrice = SecondBidPrice;
        this.X_Open = X_Open;
        this.Y_Open = Y_Open;
        this.Qty = Qty;
        this.Adj_A = Adj_A;
        this.Adj_B = Adj_B;
        this.Adj_C = Adj_C;
        this.Adj_D = Adj_D;
        this.PosLimit = PosLimit;
        this.IsOn = IsOn;
        this.SellCBuyB = SellCBuyB;
        this.SellABuyD = SellABuyD;
        this.CalculatedPosLimit = CalculatedPosLimit;
        this.FirstExchangeName = FirstExchangeName;
        this.SecondExchangeName = SecondExchangeName;

    }

    public void SetEmpty()
    {
        Id = null;
        FirstAskPrice = null;
        FirstBidPrice = null;
        SecondAskPrice = null;
        SecondBidPrice = null;
        X_Open = null;
        Y_Open = null;
        Qty = null;
        Adj_A = null;
        Adj_B = null;
        Adj_C = null;
        Adj_D = null;
        PosLimit = null;
        IsOn = null;
        SellCBuyB = false;
        SellABuyD = false;
        CalculatedPosLimit = 0;
        FirstExchangeName = null;
        SecondExchangeName = null;
    }

    public void Set(Guid? Id = null, Guid? FirstExcApiId = null, Guid? SecondExcApiId = null, decimal? FirstAskPrice = null, decimal? FirstBidPrice = null, decimal? SecondAskPrice = null, decimal? SecondBidPrice = null, decimal? X_Open = null, decimal? Y_Open = null, decimal? Qty = null, decimal? Adj_A = null, decimal? Adj_B = null, decimal? Adj_C = null, decimal? Adj_D = null, decimal? PosLimit = null, bool? IsOn = null, bool? SellCBuyB = null, bool? SellABuyD = null, decimal CalculatedPosLimit = 0, string? FirstExchangeName = null, string? SecondExchangeName = null)
    {
        this.Id = Id != null ? Id : this.Id;
        this.FirstExcApiId = FirstExcApiId != null ? FirstExcApiId : this.FirstExcApiId;
        this.SecondExcApiId = SecondExcApiId != null ? SecondExcApiId : this.SecondExcApiId;
        this.FirstAskPrice = FirstAskPrice != null ? FirstAskPrice : this.FirstAskPrice;
        this.FirstBidPrice = FirstBidPrice != null ? FirstBidPrice : this.FirstBidPrice;
        this.SecondAskPrice = SecondAskPrice != null ? SecondAskPrice : this.SecondAskPrice;
        this.SecondBidPrice = SecondBidPrice != null ? SecondBidPrice : this.SecondBidPrice;
        this.X_Open = X_Open != null ? X_Open : this.X_Open;
        this.Y_Open = Y_Open != null ? Y_Open : this.Y_Open;
        this.Qty = Qty != null ? Qty : this.Qty;
        this.Adj_A = Adj_A != null ? Adj_A : this.Adj_A;
        this.Adj_B = Adj_B != null ? Adj_B : this.Adj_B;
        this.Adj_C = Adj_C != null ? Adj_C : this.Adj_C;
        this.Adj_D = Adj_D != null ? Adj_D : this.Adj_D;
        this.PosLimit = PosLimit != null ? PosLimit : this.PosLimit;
        this.IsOn = IsOn != null ? IsOn : this.IsOn;
        this.SellCBuyB = SellCBuyB != null ? SellCBuyB : this.SellCBuyB;
        this.SellABuyD = SellABuyD != null ? SellABuyD : this.SellABuyD;
        this.CalculatedPosLimit = CalculatedPosLimit != 0 ? CalculatedPosLimit : this.CalculatedPosLimit;
        this.FirstExchangeName = FirstExchangeName  != null ? FirstExchangeName : this.FirstExchangeName ;
        this.SecondExchangeName = SecondExchangeName  != null ? SecondExchangeName : this.SecondExchangeName;

    }

    public void CalculatePosLimit(decimal value)
    {
        CalculatedPosLimit += value;
    }
    public void CalculateOrder()
    {
        if (X_Open <= X)
        {
            SellABuyD = true;

        }
        else if (Y_Open <= Y)
        {
            SellCBuyB = true;
        }
        else
        {
            SellCBuyB = false;
            SellABuyD = false;
        }
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
