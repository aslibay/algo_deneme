﻿using MultiCryptoFin.Models.Price.Exchange;

namespace MultiCryptoFin.Models.Price;
[MessagePackObject]
public struct PriceDto: IDisposable
{
    [Key(0)]
    public string? Exchange { get; set; }
    [Key(1)]
    public string? Symbol { get; set; }
    [Key(2)]
    public decimal? BidPrice { get; set; }
    [Key(3)]
    public decimal? BidQuantity { get; set; }
    [Key(4)]
    public decimal? AskPrice { get; set; }
    [Key(5)]
    public decimal? AskQuantity { get; set; }
    public PriceDto()
    {
        Exchange = null;
        Symbol = null;
        BidPrice = null;
        BidQuantity = null; 
        AskPrice = null;
        AskQuantity = null;
    }
    public PriceDto(string Exchange, PriceExchangeDto Request)
    {
        this.Exchange = Exchange;
        Symbol = Request.Symbol;
        BidPrice = Request.BidPrice;
        BidQuantity = Request.BidQuantity;
        AskPrice = Request.AskPrice;
        AskQuantity = Request.AskQuantity;
    }
    public void Set(string Exchange, PriceExchangeDto Request)
    {
        this.Exchange = Exchange;
        Symbol = Request.Symbol;
        BidPrice = Request.BidPrice;
        BidQuantity = Request.BidQuantity;
        AskPrice = Request.AskPrice;
        AskQuantity = Request.AskQuantity;
    }

    public void SetEmpty()
    {
        Exchange = null;
        Symbol = null;
        BidPrice = null;
        BidQuantity = null;
        AskPrice = null;
        AskQuantity = null;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
