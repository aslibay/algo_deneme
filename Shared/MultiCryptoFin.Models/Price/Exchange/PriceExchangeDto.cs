﻿namespace MultiCryptoFin.Models.Price.Exchange;
[MessagePackObject]
public struct PriceExchangeDto : IDisposable
{
    [Key(0)]
    public string? Symbol { get; set; }
    [Key(1)]
    public decimal? BidPrice { get; set; }
    [Key(2)]
    public decimal? BidQuantity { get; set; }
    [Key(3)]
    public decimal? AskPrice { get; set; }
    [Key(4)]
    public decimal? AskQuantity { get; set; }


    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
