﻿using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
using MultiCryptoFin.Shared.Constants;

namespace MultiCryptoFin.Models.Orders.Base;

[MessagePackObject]
public class OrderDto : IDisposable
{
    [Key(0)]
    public string? Id { get; set; }
    [Key(1)]
    public string? Exchange { get; set; }
    [Key(2)]
    public string? MsgType { get; set; }
    [Key(3)]
    public Guid? CustomOrderId { get; set; }
    [Key(4)]
    public string? OrderId { get; set; }
    [Key(5)]
    public string? Symbol { get; set; }
    [Key(6)]
    public string? Side { get; set; }
    [Key(7)]
    public string? Type { get; set; }
    [Key(8)]
    public string? TimeInForce { get; set; }
    [Key(9)]
    public decimal? Quantity { get; set; }
    [Key(10)]
    public decimal? Price { get; set; }
    [Key(11)]
    public decimal? ExecutedQuantity { get; set; } = 0;
    [Key(12)]
    public decimal? LeaveQuantity => Quantity - ExecutedQuantity;
    [Key(13)]
    public string? OrderStatus => (LeaveQuantity == null ? null: (LeaveQuantity == Quantity ? ContantOrderStatus.NEW : (LeaveQuantity == 0 ? ContantOrderStatus.FILLED : ContantOrderStatus.PARTIAL_FILLED)));
    [Key(14)]
    public int? ResponseCode { get; set; }
    [Key(15)]
    public string? ResponseMessage { get; set; }
    

    [IgnoreMember]
    public string? PreviousOrderId { get; set; }
    [IgnoreMember]
    public Guid? PreviousCustomOrderId { get; set; }
    [IgnoreMember]
    public Guid? ApiId { get; set; }

    public OrderDto()
    {
        Id = Guid.NewGuid().ToString().ToUpper();
    }

    public OrderDto(RequestOrderDto Request)
    {
        Id = Guid.NewGuid().ToString().ToUpper();
        ApiId = Request.ApiId;
        Exchange = Request.Exchange;
        CustomOrderId = Request.CustomOrderId;
        Symbol = Request.Symbol;
        Side = Request.Side;
        Type = Request.Type;
        TimeInForce = Request.TimeInForce;
        Quantity = Request.Quantity;
        Price = Request.Price;
    }
    public OrderDto(OrderDto OrderInfo, RequestOrderDto Request)
    {
        Id = OrderInfo.Id;
        Exchange = Request.Exchange;
        CustomOrderId = Request.CustomOrderId;
        Quantity = Request.Quantity;
        Price = Request.Price;
        PreviousCustomOrderId = OrderInfo.CustomOrderId;
        PreviousOrderId = OrderInfo.OrderId;

        Symbol = OrderInfo.Symbol;
        Side = OrderInfo.Side;
        Type = OrderInfo.Type;
        TimeInForce = OrderInfo.TimeInForce;
    }

    public OrderDto(OrderExchangeResponseReceiveDto Request)
    {
        Id = Guid.NewGuid().ToString().ToUpper();
        MsgType = Request.MsgType;
        Exchange = Request.Exchange;
        CustomOrderId = Request.CustomOrderId;
        OrderId = Request.OrderId;
        Symbol = Request.Symbol;
        Side = Request.Side;
        Type = Request.Type;
        TimeInForce = Request.TimeInForce;
        Quantity = Request.Quantity;
        Price = Request.Price;
    }
    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~OrderDto()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
