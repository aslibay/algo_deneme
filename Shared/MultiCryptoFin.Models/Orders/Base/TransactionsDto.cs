﻿using MultiCryptoFin.Shared.Constants;
using FinTech.Definitions.Extensions;
using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;

namespace MultiCryptoFin.Models.Orders.Base;
[MessagePackObject]
public struct TransactionsDto : IDisposable
{
    [Key(0)]
    public string? ExecutionId { get; set; }
    [Key(1)]
    public string? Exchange { get; set; }
    [Key(2)]
    public string? OrderId { get; set; }
    [Key(3)]
    public string? Symbol { get; set; }
    [Key(4)]
    public string? Side { get; set; }
    [Key(5)]
    public decimal? Quantity { get; set; }
    [Key(6)]
    public decimal? Price { get; set; }
    [Key(7)]
    public decimal? ExecutionPrice { get; set; }
    [Key(8)]
    public decimal? ExecutedQuantity { get; set; }
    [Key(9)]
    public decimal? ExecutionFee { get; set; }
    [Key(10)]
    public string? TradeTime { get; set; }

    public TransactionsDto()
    {
        ExecutionId = null;
        Exchange = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Quantity = null;
        Price = null;
        ExecutionPrice = null;
        ExecutedQuantity = null;
        ExecutionFee = null;
        TradeTime = null;
    }
    public TransactionsDto(OrderExchangeResponseReceiveDto Request)
    {
        ExecutionId = Request.ExecutionId;
        Exchange = Request.Exchange;
        OrderId = Request.OrderId;
        Symbol = Request.Symbol;
        Side = Request.Side;
        Quantity = Request.Quantity;
        ExecutionPrice = Request.ExecutionPrice;
        Price = Request.Price;
        ExecutedQuantity = Request.ExecutedQuantity;
        ExecutionFee = Request.ExecutionFee;
        if (Request.Exchange == ConstantExchanges.BYBIT)
        {
            // BYBIT Date Value from UTC 2022-02-26T07:29:35.568006Z
            TradeTime = Request.TradeTime.ToTimeStampMilisecond("yyyy-MM-ddTHH:mm:ss.ffffffZ").ToString();
        }
        else if (Request.Exchange == ConstantExchanges.BYBIT)
        {
            // FTX Date Value from UTC 2022-03-10T07:10:12.332934+00:00
            TradeTime = Request.TradeTime.ToTimeStampMilisecond("yyyy-MM-ddTHH:mm:ss.ffffff+00:00").ToString();
        }
        else
        {
            TradeTime = Request.TradeTime;
        }
    }
    public void Set(OrderExchangeResponseReceiveDto Request)
    {
        ExecutionId = Request.ExecutionId;
        Exchange = Request.Exchange;
        OrderId = Request.OrderId;
        Symbol = Request.Symbol;
        Side = Request.Side;
        Quantity = Request.Quantity;
        Price = Request.Price;
        ExecutionPrice = Request.ExecutionPrice;
        ExecutedQuantity = Request.ExecutedQuantity;
        ExecutionFee = Request.ExecutionFee;
        if (Request.Exchange == ConstantExchanges.BYBIT)
        {
            // BYBIT Date Value from UTC 2022-02-26T07:29:35.568006Z
            TradeTime = Request.TradeTime.ToTimeStampMilisecond("yyyy-MM-ddTHH:mm:ss.ffffffZ").ToString();
        }
        else if (Request.Exchange == ConstantExchanges.FTX)
        {
            // FTX Date Value from UTC 2022-03-10T07:10:12.332934+00:00
            TradeTime = Request.TradeTime.ToTimeStampMilisecond("yyyy-MM-ddTHH:mm:ss.ffffff+00:00").ToString();
        }
        else
        {
            TradeTime = Request.TradeTime;
        }
    }

    public void Set(OrderExchangeResponseReceiveDto request, OrderDto order)
    {        
        Exchange = order.Exchange;
        OrderId = order.OrderId;
        Symbol = order.Symbol;
        Side = order.Side;
        Quantity = order.Quantity;
        Price = order.Price;
        ExecutionId = request.ExecutionId;
        ExecutionPrice = request.ExecutionPrice;
        ExecutedQuantity = request.ExecutedQuantity;
        ExecutionFee = request.ExecutionFee;
        if (request.Exchange == ConstantExchanges.BYBIT)
        {
            // BYBIT Date Value from UTC 2022-02-26T07:29:35.568006Z
            TradeTime = request.TradeTime.ToTimeStampMilisecond("yyyy-MM-ddTHH:mm:ss.ffffffZ").ToString();
        }
        else if (request.Exchange == ConstantExchanges.FTX)
        {
            // FTX Date Value from UTC 2022-03-10T07:10:12.332934+00:00
            TradeTime = request.TradeTime.ToTimeStampMilisecond("yyyy-MM-ddTHH:mm:ss.ffffff+00:00").ToString();
        }
        else
        {
            TradeTime = request.TradeTime;
        }
    }

    public void SetEmpty()
    {
        ExecutionId = null;
        Exchange = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Quantity = null;
        Price = null;
        ExecutionPrice = null;
        ExecutedQuantity = null;
        ExecutionFee = null;
        TradeTime = null;
    }
    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }

    #region DISPOSE PATTERN
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
    #endregion
}
