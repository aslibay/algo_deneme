﻿namespace MultiCryptoFin.Models.Orders.Base;
public struct RequestOrderDto : IDisposable
{
    public Guid? ApiId { get; set; }
    public string? Exchange { get; set; }
    public Guid? CustomOrderId { get; set; }
    public string? OrderId { get; set; }
    public string? Symbol { get; set; }
    public string? Side { get; set; }
    public string? Type { get; set; }
    public string? TimeInForce { get; set; }
    public decimal? Quantity { get; set; }
    public decimal? Price { get; set; }
    public EnumsRequestType RequestType { get; set; }

    public RequestOrderDto(NewOrderUIRequestReceiveDto Request)
    {
        Exchange = Request.Exchange;
        ApiId = Request.ApiId;
        CustomOrderId = Guid.NewGuid();
        OrderId = null;
        Symbol = Request.Symbol;
        Side = Request.Side;
        Type = Request.Type;
        TimeInForce = Request.TimeInForce;
        Quantity = Request.Quantity;
        Price = Request.Price;
        RequestType = EnumsRequestType.NewOrder;
    }
    public RequestOrderDto(ReplaceOrderUIRequestReceiveDto Request)
    {
        Exchange = Request.Exchange;
        ApiId = null;
        CustomOrderId = Guid.NewGuid();
        OrderId = Request.OrderId;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = Request.Quantity;
        Price = Request.Price;
        RequestType = EnumsRequestType.ReplaceOrder;
    }
    public RequestOrderDto(CancelOrderUIRequestReceiveDto Request)
    {
        Exchange = Request.Exchange;
        ApiId = null;
        CustomOrderId = null;
        OrderId = Request.OrderId;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = null;
        Price = null;
        RequestType = EnumsRequestType.CancelOrder;
    }

    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
