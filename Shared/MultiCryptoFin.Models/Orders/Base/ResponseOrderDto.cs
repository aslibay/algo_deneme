﻿using MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
using MultiCryptoFin.Shared.Constants;

namespace MultiCryptoFin.Models.Orders.Base;
public struct ResponseOrderDto : IDisposable
{
    public string? Exchange { get; set; }
    public string? MsgType { get; set; }
    public Guid? CustomOrderId { get; set; }
    public string? OrderId { get; set; }
    public decimal? ExecutedQuantity { get; set; }
    public int? ResponseCode { get; set; }
    public string? ResponseMessage { get; set; }

    public ResponseOrderDto()
    {
        Exchange = null;
        MsgType = null;
        CustomOrderId = null;
        OrderId = null;
        ExecutedQuantity = null;
        ResponseCode = null;
        ResponseMessage = null;
    }

    public ResponseOrderDto(OrderExchangeResponseReceiveDto Request, string Exchange)
    {
        this.Exchange = Exchange;
        MsgType = Request.MsgType;
        CustomOrderId = Request.CustomOrderId;
        OrderId = Request.OrderId;
        ExecutedQuantity = null;
        ResponseCode = Request.ResponseCode;
        ResponseMessage = Request.ResponseMessage;
    }

    public ResponseOrderDto(TransactionsDto Request)
    {
        Exchange = Request.Exchange;
        MsgType = ConstantMsgTypes.MessageType_Execution;
        CustomOrderId = null;
        OrderId = Request.OrderId;
        ExecutedQuantity = Request.ExecutedQuantity;
        ResponseCode = 0;
        ResponseMessage = "OK";
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
