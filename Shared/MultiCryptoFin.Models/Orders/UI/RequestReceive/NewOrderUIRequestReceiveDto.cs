﻿namespace MultiCryptoFin.Models.Orders.UI;

[MessagePackObject]
public struct NewOrderUIRequestReceiveDto: IDisposable
{
    [Key(0)]
    public string? Exchange { get; set; }
    [Key(1)]
    public string? Symbol { get; set; }
    [Key(2)]
    public string? Side { get; set; }
    [Key(3)]
    public string? Type { get; set; }
    [Key(4)]
    public string? TimeInForce { get; set; }
    [Key(5)]
    public decimal? Quantity { get; set; }
    [Key(6)]
    public decimal? Price { get; set; }
    [Key(7)]
    public Guid? ApiId { get; set; }


    
    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public void Set(Guid? ApiId, string? Exchange,string? Symbol,string? Side,string? Type,string? TimeInForce,decimal? Quantity,decimal? Price){
        this.ApiId = ApiId;
        this.Exchange = Exchange;
        this.Symbol = Symbol;
        this.Side =Side;
        this.Type =Type;
        this.TimeInForce = TimeInForce;
        this.Quantity = Quantity;
        this.Price = Price;
    }
}
