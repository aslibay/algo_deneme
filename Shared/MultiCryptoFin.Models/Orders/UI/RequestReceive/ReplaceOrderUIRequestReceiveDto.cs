﻿namespace MultiCryptoFin.Models.Orders.UI;

[MessagePackObject]
public struct ReplaceOrderUIRequestReceiveDto:IDisposable
{
    [Key(0)]
    public string? Exchange { get; set; }    
    [Key(1)]
    public string? OrderId { get; set; }
    [Key(2)]
    public decimal? Quantity { get; set; }
    [Key(3)]
    public decimal? Price { get; set; }

    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
