﻿using MultiCryptoFin.Shared.Constants;
using MultiCryptoFin.Shared.Extensions;

namespace MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
[MessagePackObject]
public struct OrderExchangeResponseReceiveDto : IDisposable
{
    [Key(0)]
    public string? MsgType { get; set; }
    [Key(1)]
    public Guid? CustomOrderId { get; set; }
    [Key(2)]
    public string? OrderId { get; set; }
    [Key(3)]
    public string? Symbol { get; set; }
    [Key(4)]
    public string? Side { get; set; }
    [Key(5)]
    public string? Type { get; set; }
    [Key(6)]
    public string? TimeInForce { get; set; }
    [Key(7)]
    public decimal? Quantity { get; set; }
    [Key(8)]
    public decimal? Price { get; set; }
    [Key(9)]
    public string? ExecutionId { get; set; }
    [Key(10)]
    public decimal? ExecutionPrice { get; set; }
    [Key(11)]
    public decimal? ExecutedQuantity { get; set; }
    [Key(12)]
    public decimal? LeaveQuantity { get; set; }
    [Key(13)]
    public string? TradeTime { get; set; }
    [Key(14)]
    public decimal? ExecutionFee { get; set; }
    [Key(15)]
    public object? ReplacedOrderId { get; set; }
    [Key(16)]
    public int? ResponseCode { get; set; }
    [Key(17)]
    public string? ResponseMessage { get; set; }

    [IgnoreMember]
    public string Exchange { get; set; }

    public void Set(List<object> Request, string Exchange)
    {
        this.Exchange = Exchange;
        MsgType = Request[0]?.ToString();
        CustomOrderId = (string.IsNullOrWhiteSpace(Request[1]?.ToString()) == false ? new Guid(Request[1].ToString()) : null);
        OrderId = Request[2]?.ToString();
        Symbol = Request[3]?.ToString();
        Side = Request[4]?.ToString();
        Type = Request[5]?.ToString();
        TimeInForce = Request[6]?.ToString();
        Quantity = Request[7]?.ToString().ToDecimal();
        Price = Request[8]?.ToString().ToDecimal();
        if (MsgType == ConstantMsgTypes.MessageType_Replaced)
        {
            ReplacedOrderId = Request[15];
        }
        else if (MsgType == ConstantMsgTypes.MessageType_Execution)
        {
            ExecutionId = Request[9].ToString();
            if (ExecutionId == OrderId)
            {
                ExecutionId = string.Concat(ExecutionId, "/", Guid.NewGuid().ToString());
            }
            ExecutionPrice = Request[10]?.ToString().ToDecimal();
            ExecutedQuantity = Request[11]?.ToString().ToDecimal();
            LeaveQuantity = Request[12]?.ToString().ToDecimal();
            TradeTime = Request[13]?.ToString();
            ExecutionFee = Request[14]?.ToString().ToDecimal();
        }
        else if (MsgType == ConstantMsgTypes.MessageType_Rejected)
        {
            ResponseCode = Convert.ToInt32(Request[9]);
            ResponseMessage = Request[10]?.ToString();
        }
    }   

    public void SetEmpty()
    {
        MsgType = null;
        CustomOrderId = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = null;
        Price = null;
        ExecutionId = null;
        ExecutionPrice = null;
        ExecutedQuantity = null;
        LeaveQuantity = null;
        TradeTime = null;
        ExecutionFee = null;
        ReplacedOrderId = null;
        ResponseCode = null;
        ResponseMessage = null;
    }

    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
