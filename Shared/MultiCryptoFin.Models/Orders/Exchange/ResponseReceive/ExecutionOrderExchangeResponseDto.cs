﻿namespace MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
[MessagePackObject]
public class ExecutionOrderExchangeResponseDto : IDisposable
{
    [Key(0)]
    public string? MsgType { get; set; }
    [Key(1)]
    public Guid? CustomOrderId { get; set; }
    [Key(2)]
    public object? OrderId { get; set; }
    [Key(3)]
    public string? Symbol { get; set; }
    [Key(4)]
    public string? Side { get; set; }
    [Key(5)]
    public string? Type { get; set; }
    [Key(6)]
    public string? TimeInForce { get; set; }
    [Key(7)]
    public decimal? Quantity { get; set; }
    [Key(8)]
    public decimal? Price { get; set; }
    [Key(9)]
    public string? ExecutionId { get; set; }
    [Key(10)]
    public decimal? ExecutionPrice { get; set; }
    [Key(11)]
    public decimal? ExecutedQuantity { get; set; }
    [Key(12)]
    public decimal? LeaveQuantity { get; set; }
    [Key(13)]
    public string? TradeTime { get; set; }
    [Key(14)]
    public decimal? ExecutionFee { get; set; }

    public void SetEmpty()
    {
        MsgType = null;
        CustomOrderId = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = null;
        Price = null;
        ExecutionId = null;
        ExecutedQuantity = null;
        LeaveQuantity = null;
        TradeTime = null;
        ExecutionFee = null;
    }


    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }

    #region DISPOSE PATTERN
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
    // ~TransactionsDto()
    // {
    //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //     Dispose(disposing: false);
    // }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}
