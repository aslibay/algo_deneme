﻿namespace MultiCryptoFin.Models.Orders.Exchange.RequestSender;
[MessagePackObject]
public struct ReplaceOrderExchangeResponseDto : IDisposable
{
    [Key(0)]
    public string? MsgType { get; set; }
    [Key(1)]
    public Guid? CustomOrderId { get; set; }
    [Key(2)]
    public object? OrderId { get; set; }
    [Key(3)]
    public string? Symbol { get; set; }
    [Key(4)]
    public string? Side { get; set; }
    [Key(5)]
    public string? Type { get; set; }
    [Key(6)]
    public string? TimeInForce { get; set; }
    [Key(7)]
    public decimal? Quantity { get; set; }
    [Key(8)]
    public decimal? Price { get; set; }
    [Key(9)]
    public object? ReplacedOrderId { get; set; }

    public void SetEmpty()
    {
        MsgType = null;
        CustomOrderId = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = null;
        Price = null;
        ReplacedOrderId = null;
    }

    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
