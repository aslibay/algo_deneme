﻿namespace MultiCryptoFin.Models.Orders.Exchange.ResponseReceive;
[MessagePackObject]
public struct RejectOrderExchangeReceiveDto : IDisposable
{
    [Key(0)]
    public string? MsgType { get; set; }
    [Key(1)]
    public Guid? CustomOrderId { get; set; }
    [Key(2)]
    public object? OrderId { get; set; }
    [Key(3)]
    public string? Symbol { get; set; }
    [Key(4)]
    public string? Side { get; set; }
    [Key(5)]
    public string? Type { get; set; }
    [Key(6)]
    public string? TimeInForce { get; set; }
    [Key(7)]
    public decimal? Quantity { get; set; }
    [Key(8)]
    public decimal? Price { get; set; }
    [Key(9)]
    public int? ResponseCode { get; set; }
    [Key(10)]
    public string? ResponseMessage { get; set; }
    public void SetEmpty()
    {
        MsgType = null;
        CustomOrderId = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = null;
        Price = null;
        ResponseCode = null;
        ResponseMessage = null;
    }

    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
