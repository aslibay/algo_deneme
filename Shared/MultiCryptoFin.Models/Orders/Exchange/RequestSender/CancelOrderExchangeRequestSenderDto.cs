﻿using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Models.Orders.Exchange.RequestSender;
[MessagePackObject]
public struct CancelOrderExchangeRequestSenderDto : IDisposable
{
    [Key(0)]
    public Guid? CustomerOrderId { get; set; }
    [Key(1)]
    public string? OrderId { get; set; }
    [Key(2)]
    public string? Symbol { get; set; }

    public CancelOrderExchangeRequestSenderDto()
    {
        CustomerOrderId = null;
        OrderId = null;
        Symbol = null;
    }
    public CancelOrderExchangeRequestSenderDto(OrderDto Request)
    {
        CustomerOrderId = Request.CustomOrderId;
        OrderId = Request.OrderId;
        Symbol = Request.Symbol;
    }

    public void Set(OrderDto Request)
    {
        CustomerOrderId = Request.CustomOrderId;
        OrderId = Request.OrderId;
        Symbol = Request.Symbol;
    }

    public void SetEmpty()
    {
        CustomerOrderId = null;
        OrderId = null;
        Symbol = null;
    }

    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
