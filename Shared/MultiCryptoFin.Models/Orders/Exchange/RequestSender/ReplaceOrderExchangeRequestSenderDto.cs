﻿using MultiCryptoFin.Models.Orders.Base;

namespace MultiCryptoFin.Models.Orders.Exchange.RequestSender;
[MessagePackObject]
public struct ReplaceOrderExchangeRequestSenderDto : IDisposable
{
    [Key(0)]
    public Guid? CustomOrderId { get; set; }
    [Key(1)]
    public string? OrderId { get; set; }
    [Key(2)]
    public string? Symbol { get; set; }
    [Key(3)]
    public string? Side { get; set; }
    [Key(4)]
    public string? Type { get; set; }
    [Key(5)]
    public string? TimeInForce { get; set; }
    [Key(6)]
    public decimal? Quantity { get; set; }
    [Key(7)]
    public decimal? Price { get; set; }

    public ReplaceOrderExchangeRequestSenderDto()
    {
        CustomOrderId = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = null;
        Price = null;
    }
    public ReplaceOrderExchangeRequestSenderDto(OrderDto Request)
    {
        CustomOrderId = Request.CustomOrderId;
        OrderId = Request.PreviousOrderId; // PreviousOrderId atıyoruz çünkü OrderId null yeni OrderId gelecek ondan dolayı OrderDto nesnesinde boş.
        Symbol = Request.Symbol;
        Side = Request.Side;
        Type = Request.Type;
        TimeInForce = Request.TimeInForce;
        Quantity = Request.Quantity;
        Price = Request.Price;
    }

    public void Set(OrderDto Request)
    {
        CustomOrderId = Request.CustomOrderId;
        OrderId = Request.PreviousOrderId; // PreviousOrderId atıyoruz çünkü OrderId null yeni OrderId gelecek ondan dolayı OrderDto nesnesinde boş.
        Symbol = Request.Symbol;
        Side = Request.Side;
        Type = Request.Type;
        TimeInForce = Request.TimeInForce;
        Quantity = Request.Quantity;
        Price = Request.Price;
    }

    public void SetEmpty()
    {
        CustomOrderId = null;
        OrderId = null;
        Symbol = null;
        Side = null;
        Type = null;
        TimeInForce = null;
        Quantity = null;
        Price = null;
    }

    public byte[] ToMessagePackSerialize()
    {
        return MessagePackSerializer.Serialize(this);
    }
    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this, formatting);
    }
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
