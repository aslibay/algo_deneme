﻿namespace MultiCryptoFin.Models.Subscribe;
[MessagePackObject]
public struct SubscriberSignalRModel : IDisposable
{
    [Key(0)]
    public string? Symbol { get; set; }
    [Key(1)]
    public bool IsSubscriber { get; set; }

    public SubscriberSignalRModel()
    {
        Symbol = null;
        IsSubscriber = true;
    }

    public SubscriberSignalRModel(string Symbol, bool IsSubscriber = true)
    {
        this.Symbol = Symbol;
        this.IsSubscriber = IsSubscriber;
    }

    public void SetEmpty()
    {
        Symbol = null;
        IsSubscriber = true;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
