﻿namespace MultiCryptoFin.Models.Subscribe;
[MessagePackObject]
public struct SubscriberEngineModel : IDisposable
{
    [Key(0)]
    public string? Symbol { get; set; }
    /// <summary>
    /// AlgoId veya ConnectionId
    /// </summary>
    [Key(1)]
    public string? Subscriber { get; set; }
    /// <summary>
    /// AlgoId veya ConnectionId
    /// </summary>
    [Key(2)]
    public string? Unsubscriber { get; set; }

    public SubscriberEngineModel()
    {
        Symbol = null;
        Subscriber = null;
        Unsubscriber = null;
    }

    public SubscriberEngineModel(string Symbol, string Subscriber, string Unsubscriber)
    {
        this.Symbol = Symbol;
        this.Subscriber = Subscriber;
        this.Unsubscriber = Unsubscriber;
    }

    public void Set(string Symbol, string Subscriber, string Unsubscriber)
    {
        this.Symbol = Symbol;
        this.Subscriber = Subscriber;
        this.Unsubscriber = Unsubscriber;
    }

    public void SetEmpty()
    {
        Symbol = null;
        Subscriber = null;
        Unsubscriber = null;
    }


    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
