﻿namespace MultiCryptoFin.Shared.Commons;

public class Definitions
{
    public const string ApiName = "MultiCryptoFin.Api";
    public const string ApiVersion = "v2022-03-03 10:19";
    public const string SignalRVersion = "v2022-03-04 14:44";
    public const string EngineVersion = "v2022-03-04 14:44";
    public static AppSettingsConfiguration.Api.AppSetting ApiAppSetting { get; set; }
    public static AppSettingsConfiguration.SignalR.AppSetting SignalRAppSetting { get; set; }
    public static AppSettingsConfiguration.Engine.AppSetting EngineAppSetting { get; set; } 

    public static string EngineId { get; set; }   
}
