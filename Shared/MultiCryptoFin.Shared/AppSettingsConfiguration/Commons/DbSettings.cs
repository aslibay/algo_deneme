﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.Commons;
  public class DbSettings
{
    public string? ApplicationName { get; set; }
    public string? Address { get; set; }
    public string? Name { get; set; }
    public string? User { get; set; }
    public string? Password { get; set; }
}
