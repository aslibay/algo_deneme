﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.Commons;
public class LogSettings
{
    public string ApplicationName { get; set; }
    public string Root { get; set; }
    public bool CompressPastFiles { get; set; }
    public bool SeparateQueueForEachFile { get; set; }
    public bool Debug { get; set; }
    public bool IsActive { get; set; }
}
