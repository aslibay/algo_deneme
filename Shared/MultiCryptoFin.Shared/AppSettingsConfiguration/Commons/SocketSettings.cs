﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.Commons;
public class SocketSettings
{
    public string Address { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
}
