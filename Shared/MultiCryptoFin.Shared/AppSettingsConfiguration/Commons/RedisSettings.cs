﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.Commons;
public class RedisSettings
{
    public string ApplicationName { get; set; }
    public string Address { get; set; }
    public int Port { get; set; }
    public string Password { get; set; }
    public string RootName { get; set; }
}
