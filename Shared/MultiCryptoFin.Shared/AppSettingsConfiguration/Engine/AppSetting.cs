﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.Engine;
public class AppSetting
{
    public string ApiUrl { get; set; }
    public SocketSettings SocketSettings { get; set; }
    public RedisSettings RedisSettings { get; set; }
    public LogSettings LogSettings { get; set; }
}
