﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.SignalR;
public class AppSetting
{
    public int Port { get; set; }
    public SignalRConfiguration SignalRConfiguration { get; set; }
    public RedisSettings RedisSettings { get; set; }
    public LogSettings LogSettings { get; set; }
    public string SwarmpitUrl { get; set; }
}
