﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.SignalR;
public class SignalRConfiguration
{
    public bool CorsAllowAnyOrigin { get; set; }
    public string[] CorsAllowOrigins { get; set; }
}
