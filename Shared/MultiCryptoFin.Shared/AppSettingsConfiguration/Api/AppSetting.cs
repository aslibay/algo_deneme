﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.Api;
public class AppSetting
{
    public int Port { get; set; }
    public ApiConfiguration ApiConfiguration { get; set; }
    public DbSettings DbSettings { get; set; }
    public RedisSettings RedisSettings { get; set; }
    public LogSettings LogSettings { get; set; }
}
