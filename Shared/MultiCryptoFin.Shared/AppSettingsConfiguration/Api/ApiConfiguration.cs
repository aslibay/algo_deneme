﻿namespace MultiCryptoFin.Shared.AppSettingsConfiguration.Api;
public class ApiConfiguration
{
    public bool CorsAllowAnyOrigin { get; set; }
    public string[] CorsAllowOrigins { get; set; }
}
