﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using FinTech.Definitions.Extensions;

namespace MultiCryptoFin.Shared.Services;
public static class EnvironmentVariableService
{
    #region ENVIRONMENT VARIABLE SETTINGS METHODS
    private static Dictionary<string, object> EnvironmentVariableValues = new Dictionary<string, object>();
    public static void SetEnvironmentVariable(IEnumerable<IConfigurationSection> configurationSections)
    {
        foreach (IConfigurationSection env in configurationSections)
        {
            if (env.GetChildren().Count() > 0)
            {
                SetEnvironmentVariable(env.GetChildren());
            }
            else
            {
                EnvironmentVariableValues.TryAdd(env.Key, env.Value);
            }
        }
    }
    public static void CheckInEnvironment<T>(T AppSettingsModel, IEnumerable<IConfigurationSection> Environments)
    {
        try
        {
            if (AppSettingsModel != null)
            {
                PropertyInfo[] properties = AppSettingsModel.GetType().GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    try
                    {
                        bool ExceptContinue = false;
                        bool IsTypeClassExcept = false;

                        if (ExceptContinue == false)
                        {
                            object propertyValue = property.GetValue(AppSettingsModel);
                            if (property.PropertyType.Name.Contains("String")
                                || property.PropertyType.Name.Contains("List")
                                || property.PropertyType.Name.Contains("Array")
                                || property.PropertyType.Name.Contains("Dictionary"))
                            {
                                IsTypeClassExcept = true;
                            }
                            if (property.PropertyType.IsClass && IsTypeClassExcept == false)
                            {
                                if (propertyValue != null)
                                {
                                    CheckInEnvironment(Convert.ChangeType(propertyValue, property.PropertyType), Environments);

                                }
                            }
                            else
                            {
                                if (EnvironmentVariableValues.TryGetValue($"{property.DeclaringType.Name}_{property.Name}", out object outValue))
                                {
                                    Console.WriteLine($"'{property.DeclaringType.Name}_{property.Name}' alanındaki '{propertyValue}' değeri ortam değişkenlerindeki '{outValue}' değeri ile değiştiriliyor...");
                                    property.SetValue(AppSettingsModel, Convert.ChangeType(outValue, property.PropertyType));
                                    Console.WriteLine($"'{property.DeclaringType.Name}_{property.Name}' alanındaki '{propertyValue}' değeri ortam değişkenlerindeki '{outValue}' değeri ile değiştirildi.");
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("Exception => {0}", exc.ExtractToString());
                    }
                    finally { }
                }
            }
        }
        catch (Exception exc)
        {
            Console.WriteLine("Exception => {0}", exc.ExtractToString());
        }
        finally { }
    }
    #endregion
}
