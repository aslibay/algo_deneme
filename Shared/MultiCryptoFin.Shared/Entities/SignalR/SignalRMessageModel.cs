﻿using System;
using MessagePack;
using Newtonsoft.Json;

namespace MultiCryptoFin.Shared.Entities.SignalR;

[MessagePackObject]
public struct SignalRMessageModel<T> : IDisposable
{
    [Key(0)]
    public string Channel { get; set; }
    [Key(1)]
    public Guid UserId { get; set; }
    [Key(2)]
    public string ConnectionId { get; set; }
    [Key(3)]
    public T Message { get; set; }

    public SignalRMessageModel(string Channel, Guid UserId, string ConnectionId, T Message)
    {
        this.Channel = Channel;
        this.UserId = UserId;
        this.ConnectionId = ConnectionId;
        this.Message = Message;
    }

    public string ToJsonSerialize(Formatting formatting = Formatting.None)
    {
        return JsonConvert.SerializeObject(this);
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
