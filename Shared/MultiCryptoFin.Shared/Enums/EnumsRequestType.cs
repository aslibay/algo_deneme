﻿namespace MultiCryptoFin.Shared.Enums;
public enum EnumsRequestType
{
    NewOrder = 0,
    ReplaceOrder = 1,
    CancelOrder = 2,
    Transaction = 3
}
