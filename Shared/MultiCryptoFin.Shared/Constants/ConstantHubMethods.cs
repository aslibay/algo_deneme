﻿namespace MultiCryptoFin.Shared.Constants;
public static class ConstantHubMethods
{
    public const string NewOrder = "NewOrder";
    public const string AlgoStart = "AlgoStart";
    public const string AlgoCreate = "AlgoCreate";
    public const string AlgoStop = "AlgoStop";
    public const string ReplaceOrder = "ReplaceOrder";
    public const string CancelOrder = "CancelOrder";
    public const string Subscribe = "Subscribe";
    public const string Unsubscribe = "Unsubscribe";
    public const string GetSubscribers = "GetSubscribers";
    public const string SendEngineIsOnline = "SendEngineIsOnline";
    public const string GetAlgo = "GetAlgo";
    public const string SendAlgoPrice = "SendAlgoPrice";
    public const string AlgoReplace = "AlgoReplace";
    public const string AlgoDelete = "AlgoDelete";
    public const string AlgoList = "AlgoList";
    public const string PreAlgoList = "PreAlgoList";







    /// <summary>
    /// SendGroup(object Message);
    /// </summary>
    public const string SendGroup = "SendGroup";
    /// <summary>
    /// SendGroupInitialize(object Message);
    /// </summary>
    public const string SendGroupInitialize = "SendGroupInitialize";
    /// <summary>
    /// SendClient(string ConnectionId, object Message)
    /// </summary>
    public const string SendClient = "SendClient";
    /// <summary>
    /// SendClientInitialize(string ConnectionId, object Message)
    /// </summary>
    public const string SendClientInitialize = "SendClientInitialize";
    /// <summary>
    /// SendGroupWarning(object Message)
    /// </summary>
    public const string SendGroupWarning = "SendGroupWarning";
}
