﻿namespace MultiCryptoFin.Shared.Constants;
public static class ConstantCommon
{
    public const string AuthorizationType = "Bearer";
    public const string access_token = "access_token";    
    public const string MainHub = "MainHub";    
    public const string OpenOrderHub = "OpenOrderHub";    
    public const string TransactionHub = "TransactionHub";    
    public const string PriceHub = "PriceHub";    
}
