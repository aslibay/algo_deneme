﻿namespace MultiCryptoFin.Shared.Constants
{
    public static class ConstantRoleNames
    {
        public const string Administrator = "Administrator";
        public const string Customer = "Customer";
    }
}
