﻿namespace MultiCryptoFin.Shared.Constants;
public static class ConstantExchanges
{
    public const string BINANCE = "BINANCE";
    public const string BYBIT = "BYBIT";
    public const string CRYPTO = "CRYPTO";
    public const string FTX = "FTX";
    public const string HUOBI = "HUOBI";
}
