﻿namespace MultiCryptoFin.Shared.Constants;
public static class ConstantLoggerFileName
{
    public const string Error = "Error";
    public const string UIRequest = "UIRequest";    
    public const string Engine = "Engine";
    public const string NewOrder = "NewOrder";
    public const string ReplaceOrder = "ReplaceOrder";
    public const string CancelOrder = "CancelOrder";
    public const string Connected = "Connected";
    public const string HubOpenOrders = "HubOpenOrders";
    public const string HubTransactions = "HubTransactions";
    public const string HubMain = "HubMain";
    public const string HubPrices = "HubPrices";
    public const string Request = "Request";

    public const string RedisRequest = "RedisRequest";
    public const string RedisResponse = "RedisResponse";
    public const string RedisTransaction = "RedisTransaction";
    public const string RedisPrice = "RedisPrice";
    public const string TransactionUpdater = "TransactionUpdater";
    public const string PriceUpdater = "PriceUpdater";
    public const string Subscribed = "Subscribed";
    public const string Unsubscribed = "Unsubscribed";
    public const string ClientConnected = "ClientConnected";
    public const string Subscribers = "Subscribers";
    public const string AlgoStart = "AlgoStart";
    public const string AlgoCreate = "AlgoCreate";
    public const string AlgoStop = "AlgoStop";
    public const string AlgoReplace = "AlgoReplace";
    public const string AlgoDelete = "AlgoDelete";
    public const string AlgoOrder = "AlgoOrder";




    

}
