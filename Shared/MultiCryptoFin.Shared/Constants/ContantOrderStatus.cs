﻿namespace MultiCryptoFin.Shared.Constants;
public static class ContantOrderStatus
{
    public const string NEW = "NEW";
    public const string FILLED = "FILLED";
    public const string PARTIAL_FILLED = "PARTIAL FILLED";
    public const string CANCELED = "CANCELED";
    public const string REJECTED = "REJECTED";
}
