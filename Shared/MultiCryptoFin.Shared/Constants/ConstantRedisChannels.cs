﻿namespace MultiCryptoFin.Shared.Constants;
public static class ConstantRedisChannels
{
    public const string Order = "Order";
    public const string Request = "Request";
    public const string Response = "Response";
    public const string New = "New";
    public const string Replace = "Replace";
    public const string Cancel = "Cancel";
    public const string Transaction = "Transaction";
    public const string Symbol = "Symbol";
    public const string TOB = "TOB";
    public const string Subscribe = "Subscribe";    
    public const string Unsubscribe = "Unsubscribe";
    public const string SubscribeInitialize = "SubscribeInitialize";
    public const string AdapterIsOnline = "AdapterIsOnline";
    public const string System = "System";
}
