﻿namespace MultiCryptoFin.Shared.Constants;
public static class ConstantMsgTypes
{
    public const string MessageType_Accepted = "A";
    public const string MessageType_Replaced = "U";
    public const string MessageType_Execution = "E";
    public const string MessageType_Canceled = "C";
    public const string MessageType_Rejected = "J";
}
