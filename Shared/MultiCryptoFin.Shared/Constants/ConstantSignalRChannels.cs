﻿namespace MultiCryptoFin.Shared.Constants;
public static class ConstantSignalRChannels
{
    public const string Warning = "Warning";
    public const string NewOrder = "NewOrder";    
    public const string ReplaceOrder = "ReplaceOrder";    
    public const string CancelOrder = "CancelOrder";    
    public const string Connected = "Connected";    
    public const string OpenOrders = "OpenOrders";    
    public const string OpenOrdersInitialize = "OpenOrdersInitialize";
    public const string Positions = "Positions";
    public const string PositionsInitialize = "PositionsInitialize";
    public const string Transactions = "Transactions";
    public const string TransactionsInitialize = "TransactionsInitialize";  
    public const string Prices = "Prices";
    public const string PricesInitialize = "PricesInitialize";
    public const string AddOpenOrder = "AddOpenOrder";    
    public const string Subscribed = "Subscribed";    
    public const string Subscribers = "Subscribers";    
    public const string Unsubscribed = "Unsubscribed";    
    public const string SendGroup = "SendGroup";        
    public const string EngineIsOnline = "EngineIsOnline";    
    public const string ClientConnected = "ClientConnected";    
    public const string  AlgoStart = "AlgoStart";
    public const string  AlgoReplaceOrder = "AlgoReplaceOrder";
    public const string  AlgoCancelOrder = "AlgoCancelOrder";
    public const string AlgoPrice = "AlgoPrice";
    public const string  AlgoCreate = "AlgoCreate";
    public const string  AlgoStop = "AlgoStop";
    public const string  AlgoReplace = "AlgoReplace";
    public const string  AlgoList = "AlgoList";
    public const string  PreAlgoList = "PreAlgoList";

    public const string  AlgoDeleteResponse = "AlgoDeleteResponse";


    
    


}
