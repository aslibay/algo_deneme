﻿using System;
using System.Globalization;

namespace MultiCryptoFin.Shared.Extensions;
public static class StringExtensions
{
    public static decimal? ToDecimal(this string Request)
    {
        if (string.IsNullOrWhiteSpace(Request)) return null;

        Request = Request.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
        return Convert.ToDecimal(Request);
    }
}
